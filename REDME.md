if you are an external reader: 
This is the continuation of the project started by Nicolino Lo Gullo and Walter Talarico last year https://gitlab.utu.fi/nblogu/negfs-ness https://gitlab.utu.fi/nblogu/NEGFs
The project will probably move here. 

The main use of the libraries is for computation of transport quantities in stationary quantum systems, trough numerical evaluation of the green funtions in the NEGF formalism at stationarity. Our main focus is on interacting 1D anomalous systems.

For the time being only recent modifications are commented, sorry. In the near future we'll try to add comments on old part of the code as well.  



a brief note about what this code can do:
THe code is obviously still far from being optimal.  
Nevertheless with 20 noeds on Puhti and Mahti supercomputers we could perform mean-field simulations with a resolution of 10^5/10^6 points on the frequency axis, and sizes up to about 100 sites. We are currently aptempting to obtain similar performances for second order self-consistent approximations.