! ============================================================================
! Name        : wifhm_leads_tgrid.f90
! Author      : N. Lo Gullo
! Version     : 1.0
! Copyright   : GPL
! Description : Evolution for weakly interacting fermions in 1D and 2D
!		by means of a self-iterative method based on the NEGFs.
!		Both spatial and time variable are split onto a grid.
! Date        : 08/12/2017
! ============================================================================

Program wifhm
	use parametersfw
	use intchar
	!use minpack
	use libness
	implicit none
	include 'mpif.h'
!Free Hamiltonian, eigenvalues, Change basis matrix
	double precision, dimension(:), allocatable :: onsite
	double precision, dimension(:), allocatable :: nnhop
	double precision, dimension(:,:), allocatable :: ham
	double precision, dimension(:), allocatable :: w
	double complex, dimension(:), allocatable :: wc
	double precision, dimension(:,:), allocatable :: s
	double precision, dimension(:,:), allocatable :: hamhf
	double precision, dimension(:), allocatable :: wh
	double precision, dimension(:,:), allocatable :: shf
	integer, dimension(:), allocatable :: vw
	double precision, dimension(:), allocatable :: pnum0, pnum
	double precision, dimension(:), allocatable :: hnum0, hnum
	double precision, dimension(:), allocatable :: omega
	double precision, dimension(:), allocatable :: dos
	double precision, dimension(:,:), allocatable :: specfun
	double precision, dimension(:,:), allocatable :: rho


!Interaction Matrix
	double precision :: U
	double precision, dimension(:,:), allocatable :: v

!Free Green's functions matrices
	double precision :: mu
	double complex, dimension(:,:), allocatable :: lg0l,lg0g,lg0r
	double complex, dimension(:,:), allocatable :: lgal,lgag,lgar
	double complex, dimension(:,:), allocatable :: lgbl,lgbg,lgbr

!Embedding self-energy matrices for coupling with leads
	integer :: np
	integer, dimension(:), allocatable :: ixL,ixR, ixEta
	double precision :: eps, deps, epsmin, epsmax, Vl, Vr, Veta
	double complex, dimension(:,:), allocatable :: lsembl,lsembg,lsembr
	double complex, dimension(:,:), allocatable :: lsetal,lsetag,lsetar
	double complex, dimension(:,:), allocatable :: lsembl1,lsembr1, lsembl2,lsembr2

!Total self-energy matrices
	double complex, dimension(:,:), allocatable :: lsl,lsg,lsr
	double complex, dimension(:,:), allocatable :: lsrchk, inta, inta2

!NEGFs matrices
	double complex, dimension(:,:), allocatable :: lnegg,lnegl,lnegr

!Pulay coefficents
	double precision, dimension(:), allocatable :: c
	double complex, dimension(:,:,:), allocatable :: Ginr, Ginl, Ging
	double complex, dimension(:,:,:), allocatable :: Goutr, Goutl, Goutg


!Descrittore
	integer, dimension(9) :: desc,descl

!Characters
	character(len=120) :: folder, Mainfolder

!Variables for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
	integer :: lldr,lldc,lldwc,pc,pr

!Grid spatial dimension
	integer :: ns

!Global dimensions of a matrix (ns x nt)
	integer :: n

!Running indices
integer :: i,j,ix,iy,ip,jp,iw,liw,aa,aai,nrun,vvlds,numroc

!Temporary variables
	double precision :: dalpha,dalpha2,numpart,x,y, tempr, om
	double precision, dimension(:), allocatable :: ggl,ggr, ggeta, tempdSLR
	double precision, dimension(:,:), allocatable :: gtemp
  double complex :: zalpha
	double precision :: start, finish, STARTALL, FINISHALL
	double precision :: pzlange
	double precision, dimension(1:10) :: work
	double precision, dimension(2,1) :: DeltaSLR
	integer, dimension (1:9) :: desctmp
	double complex, dimension(:,:), allocatable :: intg, Id


	!temporary parameters
	integer :: ilambda, imix, imu, muctrl, irunmu, inx,ieta, in, ib, igamma
	integer ::  nlambda, nmix, nmu, nnx,nneta, ngamma, itemp
	integer, dimension(:), allocatable :: muctrd
	double precision, dimension(:), allocatable :: dmucd

	integer ::  nix, nib, mib
	double precision :: neta,rnmix, rnA1, mul,mur, muDiss
	double precision, dimension(:), allocatable :: muEta, betaEta,betaEta0, muEta0
	double precision :: dmucl, dmcurd,mcnorm, mcnorm0
	double precision, dimension(:), allocatable :: mixvec, ggvec, betavec
	double precision, dimension(:), allocatable :: lambdavec
	double precision, dimension(:), allocatable :: mulvec, murvec
	integer, dimension(:), allocatable :: nxvec
	double precision, dimension(:), allocatable :: etavec

	double precision, dimension(:,:), allocatable :: TfunLR, Tfun_evLR
	double complex, dimension(:,:), allocatable :: heff, LU
	integer :: Nmax, Nmin
	double precision :: dfdmuL,dfdmuR, fmuL,fmuR, dw, btL, btR, ildio, elamad
	double precision, dimension(:,:), allocatable :: mcurr
	integer, dimension(:), allocatable :: ipvs

	!for complex eig
	double precision, dimension(:), allocatable :: rwork
	double complex, dimension(:), allocatable :: work0, work1
	double complex, dimension(:,:), allocatable :: wvecl, wvecr
	double complex, dimension(:), allocatable :: wc_a, wc_b

	integer :: lwork, crtlTIME, DECAdi


	neta=eta0
	!nlambda=9
	nlambda=6
	nmix=7
	nmu=12
	nnx=15
	nneta=4
	ngamma=3!0
	allocate(mixvec(nmix))
	allocate(lambdavec(nlambda))
	allocate(mulvec(nmu))
	allocate(murvec(nmu))
	allocate(nxvec(nnx))
	allocate(etavec(nneta))
	allocate(ggvec(ngamma))
	allocate(betavec(5))


	mulvec=(/-1.8d0,-1.2d0,-0.6d0,0.0d0,0.6d0, 1.2d0,1.8d0,2.4d0,0.3d0,0.d0,4.d0,0.d0/)
	murvec=(/-2.4d0,-1.8d0,-1.2d0,-0.6d0,0.d0, 0.6d0,1.2d0,1.8d0,0.d0,-0.3d0,-4.d0,0.d0/)

	! do imu=1,5
	! 	mulvec(imu+10)=-((1.d0*imu-1.d0))**2/100.d0
	! 	murvec(imu+10)=mulvec(imu+10)
	! end do
	do i=1, ngamma
		ggvec(i)=2.d0**(1.d-1*i -1.d-1*ngamma/2)
	end do

	betavec=(/1.d3,1.d4,1.d2,1.d5,1.d1/)


mixvec=(/0.d0,1.d-2,5.d-2,2.d-1,5.d-1,9.d-1,1.d0/)
!lambdavec=(/0.d0,0.2d0,0.4d0, 0.6d0,0.8d0,0.9d0,1.d0,1.1d0,1.2d0/)
!lambdavec=(/1.d0/)
!lambdavec=(/0.d0,0.9d0,1.d0,1.1d0,0.8d0,0.6d0,1.2d0,0.4d0,0.2d0/)
lambdavec=(/0.d0,0.9d0,1.d0,1.2d0,2.d0,0.6d0/)
!lambdavec=(/1.d0, 1.1d0, 1.2d0/)
! mulvec=(/-2.d0,0.d0, 2.d0,2.d0,2.d0/)
! murvec=(/-2.d0,-2.d0,-2.d0,0.d0,2.d0/)
!ggvec=(/5.d-1*sqrt(5.d-1), 1.d0, sqrt(2.d0)/)

nxvec=(/10,20,30,40,50,60,70,80,90,100,110,120,130,140,150/)
etavec=(/eta0,eta0*1.d1,eta0*1.d2,eta0*1.d3, eta0*1.d4/)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!		Inizializzazione griglia (Start)
!--------------------------------------------------------------------

call sl_init(context,1,nnprow*nnpcol)
call mpi_comm_size(mpi_comm_world,nprocs,info)


if (nprocs/=nnprow*nnpcol) then
print*, '     Il numero di processi non corrisponde alla dimensione della griglia'
print*, nprocs, nnprow, nnpcol
stop
end if

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)

if (myrow==0.and.mycol==0) then

print*,' '
print*,' '
print*,'             Inizio evolutione'
print*,' '
print*, 'Numero processi=', nprow,npcol,nprocs
end if


!--------------------------------------------------------------------
!		Inizializzazione griglia (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
if (myrow==0.and.mycol==0) then
	call cpu_time(STARTALL)
end if

lldwc=numroc(nw,1,mycol,0,npcol*nprow)
allocate(omega(nw))
allocate(vw(lldwc))
call wgrid(context,nw,wm,vw,1,omega)



do DECAdi=2,4

Nmax = (DECAdi)*100
Nmin = (DECAdi-1)*100+1
dw=2.d0*wm/(nw-1)

allocate(TfunLR(Nmax-Nmin+1,nw))
allocate(Tfun_evLR(Nmax-Nmin+1,Nmax))
allocate(mcurr(Nmax-Nmin+1,3))

TfunLR=0.d0
mcurr=0.d0
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!
!______________________________________________________________________________!
!do igamma=1, ngamma
igamma=3
do ilambda=1,nlambda
do ieta=1,nneta
rnA1=lambdavec(ilambda)
neta=etavec(ieta)!eta0

if (myrow==0.and.mycol==0) then
	Mainfolder='aa_'//"curr"//int2char(Nmax)&
					&//"L"//int2char(ceiling(1.d1*rnA1))&
					&//"gl"//int2char(floor(10*gr*gl))&
					&//"etn"//int2char(ceiling(abs(LOG10(neta))))//"/"
	call system('mkdir '//trim(Mainfolder))
end if

in=0
crtlTIME=0
do while (in <= Nmax-Nmin+1 .and. crtlTIME/=1)
!do in=1,Nmax-Nmin+1!1,nnx   ________________________________________________
	call   blacs_barrier(context, 'A')
	in=in+1
	nix=in+Nmin-1!nxvec(inx)

	nib=nix*ny
	mib=nix*ny

	! if (myrow==0.and.mycol==0) then
	! 	print*, " "
	! 	print*, " "
	! 	print*, " "
	! 	print*, "NX=",nix
	! end if


	ns=nix
	n=nw*ns

	!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	!			Inizializzazione (Start)
	!--------------------------------------------------------------------

	lldr=numroc(ns,mib,myrow,0,1)
	lldc=numroc(n,nib,mycol,0,npcol*nprow)

	call   blacs_barrier(context, 'A')
	call descinit(desc,ns,n,mib,nib,0,0,context,lldr,info)
	call descinit(descl,1,nw,1,1,0,0,context,1,info)


	! if (myrow==0.and.mycol==0) then
	! 	print*, " "
	! 	print*, 'Inizializzazione fatta'
	! end if

	omega=0.d0

	allocate( Id(ns,ns), ham(ns,ns), heff(ns,ns), s(ns,ns), LU(ns,ns) )
	allocate( onsite(ns), nnhop(ns-1), w(ns), wc(ns))
	allocate( lsetar(ns,ns), lsembr(ns,ns) , lg0r(ns,ns))

	allocate(ixL(sitelds),ixR(sitelds), ixeta(ns))
	allocate(muEta(ns), betaEta(ns) )
	allocate(ggl(ns),ggr(ns),ggeta(ns), gtemp(ns,1))
	allocate(v(ns,ns))
	allocate(ipvs(ns))


	allocate(wc_a(ns), wc_b(ns), work0(2*ns), rwork(8*ns))
	allocate( wvecl(1,ns), wvecr(1,ns))


	!--------------------------------------------------------------------
	!			Inizializzazione (End)
	!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


	Id=0.d0
	do i=1,ns
			Id(i,i)=1.d0
	end do


	!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	!			Hamiltonian (Start)
	!--------------------------------------------------------------------

	!Create Hamiltonian
	onsite=0.d0
	nnhop=0.d0
	!rnA1=0.d0
	do i = 1, nix
		onsite(i)= rnA1*cos(2*pi*tau*i) !- dU*0.5d0
	end do

	do i = 1, nix-1
	!HAS TO BE NEGATIVE!
		nnhop(i)=-0.5d0
	end do

	call tbham(nix,bcx,onsite,nnhop,ham)

	! if (myrow==0.and.mycol==0) then
	! print*, "ham"
	! end if
	call blacs_barrier(context,'A')

	!Diagonalize Hamiltonian

	call diagsy(ns,ham,w,s,0,'0')
	call blacs_barrier(context,'A')

	! if (myrow.eq.0 .and. mycol.eq.0) then
	! 	print*, 'Fine diagonalizzazione'
	! end if



	!--------------------------------------------------------------------
	!		Hamiltonian (End)
	!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	!		T function (Start)
	!--------------------------------------------------------------------
	ggl=0.d0
	ggr=0.d0
	ggl(1)=1.d0!ggvec(igamma)
	ggr(1)=1.d0!ggvec(igamma)

	ildio=ggl(1)
	elamad=ggr(1)

	lsetar=zero
	do i=1,ns
		lsetar(i,i)=ci*neta
	end do
	lsembr=zero
	lsembr(1,1)=-0.5d0*ci*ggl(1)**2
	lsembr(ns,ns)=-0.5d0*ci*ggr(1)**2


	heff(1:ns,1:ns)=ham(1:ns,1:ns) +lsetar(1:ns,1:ns) +lsembr(1:ns,1:ns)

	! ! gen eigenvsl heff
	! call zggev(	"N","N",ns,heff,ns,Id,ns,wc_a,wc_b,wvecl,1,wvecr,1,work0,-1,rwork,info)
	! lwork=IDNINT(aimag(ci*work0(1)))
	! allocate(work1(lwork))
	! call zggev(	"N","N",ns,heff,ns,Id,ns,wc_a,wc_b,wvecl,1,wvecr,1,work1,lwork,rwork,info)

	! if (myrow.eq.0 .and. mycol.eq.0) then
	! 	print*, "decomposizione fatta", info, lwork
	! end if

	lg0r=zero
	mcurr=0.d0
	do iw=1, lldwc
		liw = (iw-1)*ns+1
		om=-wm+2.d0*wm*(vw(iw)-1)/(nw-1)
		lg0r(1:ns,1:ns)= om*Id(1:ns,1:ns) - heff(1:ns,1:ns)
		call locinv(ns,lg0r(1:ns,1:ns))

		!TfunLR(in,iw) = ggl(1)*ggr(1)*product(abs(wc_b)**2)/product(abs(wc)**2)
		TfunLR(in,iw) = (ggl(1)**2)*(ggr(1)**2)*abs(lg0r(1,ns))**2
	end do



	! if (myrow.eq.0 .and. mycol.eq.0) then
	! 	print*, " "
	! 	print*, " "
	! 	print*, "Tfun fatta", ns
	! 	print*, " "
	! end if


	!------------------------
! 	Tfun_evLR=0.d0
! 	LU=heff
! 	call zgetrf	(	ns, ns, LU, ns, ipvs, info)
! 	do iw=1,ns
! 		om=LU(iw,iw)
! 		lg0r(1:ns,1:ns)= om*Id(1:ns,1:ns) - heff(1:ns,1:ns)
! 		call locinv(ns,lg0r(1:ns,1:ns))
! 		Tfun_evLR(in,iw) = ggl(1)*ggr(1)*abs(lg0r(1,ns))**2
! 		if (myrow.eq.0 .and. mycol.eq.0) then
! print*, "ev", LU(iw,iw)
! print*, "pvs", ipvs
! end if
!  	end do
! 	if (myrow.eq.0 .and. mycol.eq.0) then
! 		print*, " "
! 		print*, "Tfun_ev fatta", ns
! 		print*, " "
! 	end if

	!--------------------------------------------------------------------
	!			T function (End)
	!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



	deallocate( Id, ham, heff, s , LU )
	deallocate( onsite, nnhop, w , wc )
	deallocate( lsetar, lsembr , lg0r)
	deallocate(ixL,ixR, ixeta)
	deallocate(muEta, betaEta )
	deallocate(ggl,ggr,ggeta, gtemp)
	deallocate(v)
	deallocate(ipvs)

	deallocate(wc_a, wc_b, work0, rwork)
	deallocate( wvecl, wvecr)
	!deallocate(work1)


	if (myrow==0.and.mycol==0) then
		call cpu_time(FINISHALL)
		if (FINISHALL-STARTALL > 60*5*60) then
			print*, " non gle la fai... chiudiamola qui e portiamo a casa un risultato..= ", FINISHALL-STARTALL
			crtlTIME=1
		end if
		print*, ns, " time ", FINISHALL-STARTALL

	end if

end do



!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Currents (Start)
!--------------------------------------------------------------------
if (myrow.eq.0 .and. mycol.eq.0) then
call cpu_time(start)
end if



do ib=1,2!5
btL=betavec(ib)
btR=betavec(ib)
if (ib>1) then
	itemp=nmu-1
else
	itemp=1
end if

do imu=itemp,nmu
mul=mulvec(imu)
mur=murvec(imu)

! if (myrow==0.and.mycol==0) then
! print*, "mul mul", mul, mur
! ! print*, mulvec
! ! print*, murvec
! end if



!-------- Folder (Start) --------------------------------------------

if (myrow==0.and.mycol==0) then
	folder=trim(Mainfolder)//'aa_'//"curr"//int2char(Nmax)&
					&//"L"//int2char(ceiling(1.d1*rnA1))&
					&//"gl"//int2char(floor(10*ildio*elamad))&
					&//"mu"//int2char(ceiling(100*mul))//"-"//int2char(ceiling(100*mur))&
					&//"nw"//int2char(nw)&
					&//"wm"//int2char(floor(wm))&
					&//"etn"//int2char(ceiling(LOG10(neta)))&
					&//"bt"//int2char(ib)//'/'
	call system('mkdir '//trim(folder))
end if
call   blacs_barrier(context, 'A')

!-------- Folder (End) ------------------------------------------------
!----------------------------------------------------------------------
!-------- Currents (Start) --------------------------------------------


do in=1, Nmax-Nmin+1
	mcurr(in,:)=0.d0
	do iw=1, lldwc
		liw = (iw-1)*ns+1
		om=-wm+2.d0*wm*(vw(iw)-1)/(nw-1)
		fmuL = (1.0d0/(exp(btL*(om-muL))+1.0d0))
		fmuR = (1.0d0/(exp(btR*(om-muR))+1.0d0))
		dfdmuL = ( 1/(1+1.d0*exp(-btL*(om-muL))) ) * btL * fmuL
		dfdmuR = ( 1/(1+1.d0*exp(-btL*(om-muR))) ) * btR * fmuR
		! dfdmuL = btL/((exp(btL*(om-muL))+1.d0)*(1+1.d0*exp(-btL*(om-muL))))
		! dfdmuR = btR/((exp(btR*(om-muR))+1.d0)*(1+1.d0*exp(-btR*(om-muR))))
		mcurr(in,1) = mcurr(in,1) + TfunLR(in,iw)*dw*(fmuL-fmuR)/(2.d0*pi)
		mcurr(in,2) = mcurr(in,2) + TfunLR(in,iw)*dw*dfdmuL/(2.d0*pi)
		mcurr(in,3) = mcurr(in,3) + TfunLR(in,iw)*dw*dfdmuR/(2.d0*pi)
		!print*, vw(iw), "f_d", dfdmuL, fmuL,btL*(om-muL), om, "condl", mcurr(in,2)

	end do

end do


call dgsum2d(context, 'A', 'i-ring', size(mcurr,1), 3, mcurr, size(mcurr,1), 0, 0)
!call   blacs_barrier(context, 'A')


if (myrow.eq.0 .and. mycol.eq.0) then
	open(unit=21,file=trim(folder)//'currLR'//trim("E")//'.dat',status='unknown',access='append')
	open(unit=22,file=trim(folder)//'condL'//trim("E")//'.dat',status='unknown',access='append')
	open(unit=23,file=trim(folder)//'condR'//trim("E")//'.dat',status='unknown',access='append')

do in=1, Nmax-Nmin+1
	write(21,*) mcurr(in,1)
	write(22,*) mcurr(in,2)
	write(23,*) mcurr(in,3)
! 		if (imu== nmu.and. ilambda==1) then
! 			print*, "qui", in, ildio, elamad, ggvec(igamma)
! 	print*, mcurr(in,1)
! 	print*, mcurr(in,2)
! 	print*, mcurr(in,3)
! end if
end do


	close(21)
	close(22)
	close(23)
	! print*, "correnti stampate"

	! do in=1, Nmax-Nmin+1
	! 	open(unit=21,file=trim(folder)//'Tfun_ev'//int2char(in)//'.dat',status='unknown',access='append')
	! 	do i=1,in+Nmin-1
	! 		write(21,*) Tfun_evLR(in,i)
	! 	end do
	! 	print*, "Tf_ev stampata", Tfun_evLR(in,(in+Nmin)/2)
	! end do

end if

!-------- Currents (End) --------------------------------------------
!--------------------------------------------------------------------
!-------- Parameters (Start) ----------------------------------------


if (myrow.eq.0 .and. mycol.eq.0) then
        open(unit=21,file=trim(folder)//'parameters.dat',status='unknown')
        write(21,'(A2,T5,I3)') "nix", nix
        write(21,'(A2,T5,F12.6)') "wm", wm
        write(21,'(A2,T5,I5)') "nw", nw
        write(21,'(A2,T5,F12.6)') "dw", 2*wm/(nw-1)
				write(21,'(A3,T5,F12.6)') "eta", neta
        write(21,'(A2,T5,F12.6)') "bt", beta
        write(21,'(A2,T5,F12.6)') "mu", muDiss
				write(21,'(A3,T5,F12.6)') "dmu", dmul
        write(21,'(A2,T5,F12.6)') "gl", ildio
        write(21,'(A2,T5,F12.6)') "bl", btL
        write(21,'(A2,T5,F12.6)') "gr", elamad
        write(21,'(A2,T5,F12.6)') "br", btR
        write(21,'(A2,T5,F12.6)') "ml", mul
        write(21,'(A2,T5,F12.6)') "mr", mur
				write(21,'(A2,T5,I4)') "nprocs", nprocs
        close(21)
end if

end do
end do
if (myrow.eq.0 .and. mycol.eq.0) then
call cpu_time(finish)
print*, "time currents=", finish -start
end if
!-------- Parameters (End) --------------------------------------------


!--------------------------------------------------------------------
!			Currents (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Print Physical Quantities (Start)
!--------------------------------------------------------------------




if (myrow==0.and.mycol==0) then
       call system('mv *dat '//trim(Mainfolder))
end if

end do
end do



deallocate(TfunLR)
deallocate(Tfun_evLR)
deallocate(mcurr)
end do
!--------------------------------------------------------------------
!			Print Physical Quantities (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!______________________________________________________________________________

	if (myrow==0.and.mycol==0) then
	        call cpu_time(FINISHALL)
	        print*, " TIMEALL= ", FINISHALL-STARTALL
	end if

call blacs_gridexit(context)
call blacs_exit(0)


end program wifhm
