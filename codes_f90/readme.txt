aatransp -> a basic version of the code which compute stationary quantities for a 1D quantum system. 
Naatransp -> Same as above, just with cycles on chemical potentials, onsite potentials, coupling strength and size of the system already included.
Ntridtransp -> ONLY FOR NON INTERACTING, WBLA. much less calculations are needed.
miks_detscurr -> ONLY FOR NON INTERACTING TRIDIAGONAL. Computes currents with growing size starting from the center of the chain. 

parameters -> parameters of the simulation. 
(WARNING if you use Naatransp keep in mind that temperatures, chemical potentials, couplings and size of the system parameters are redefined inside the code.)
parametersfw -> same as above. (duplicated just for practical convenience when working with “aatransp” and “aatrasnp2D” at the same time). You can find inside some suggestion for Nw.
 







aatransp2D -> same code as “aatransp”, but MKL CDFT is implemented. 
A 2D Scalapack grid is needed.  
Each process of the grid computes quantities at a certain frequency w, to time t (the distributed variables). When the fourier transform of some F(w) or F(t) is needed every column collect F on it’s first row; ft is performed on the first row, and then F is redistributed again on all rows. 

NOTICE: here performances can vary a lot depending on the choice of the Grid (number of rows A, and columns B), and the number of frequencies (times) Nw. 

For Nw: have to be $ Nw >= B*C $; $C>B$ an integer. Performances are way better when $Nw= C^2$, and for powers of small integers. I will put a table with some values.

For the grid, if you fix the number of process AxB=const
-increasing A->increase memory requirements, faster CDFT, slower collapsing of the data on the first row.
-increasing B->decrease memory requirements, slower CDFT, faster collapsing of the data on the first row.
As a general rule I suggest to start with almost squared grids.




