! ============================================================================
! Name        : wifhm_leads_tgrid.f90
! Author      : N. Lo Gullo
! Version     : 1.0
! Copyright   : GPL
! Description : Evolution for weakly interacting fermions in 1D and 2D
!		by means of a self-iterative method based on the NEGFs.
!		Both spatial and time variable are split onto a grid.
! Date        : 08/12/2017
! ============================================================================

Program wifhm
	use parametersfw
	use intchar
	!use minpack
	use libness
	!use MKL_DFTI, forget => DFTI_DOUBLE, DFTI_DOUBLE => DFTI_DOUBLE_R
	!use MKL_DFTI
	use MKL_CDFT
	implicit none
	include 'mpif.h'
	!include 'mkl_dfti.h'
!Free Hamiltonian, eigenvalues, Change basis matrix
	double precision, dimension(:), allocatable :: onsite
	double precision, dimension(:), allocatable :: nnhop
	double precision, dimension(:,:), allocatable :: ham
	double precision, dimension(:), allocatable :: w
	double precision, dimension(:,:), allocatable :: s
	double precision, dimension(:,:), allocatable :: hamhf
	double precision, dimension(:), allocatable :: wh
	double precision, dimension(:,:), allocatable :: shf
	integer, dimension(:), allocatable :: vw_c, vt_c, DFTvw
	integer, dimension(:), allocatable :: vw_lc, vt_lc
	integer, dimension(:), allocatable :: vw_ilc, vt_ilc

	double precision, dimension(:), allocatable :: pnum0, pnum
	double precision, dimension(:), allocatable :: hnum0, hnum
	double precision, dimension(:), allocatable :: omega
	double precision, dimension(:), allocatable :: dos
	double precision, dimension(:,:), allocatable :: specfun
	double precision, dimension(:,:), allocatable :: rho


!Interaction Matrix
	double precision :: U
	double precision, dimension(:,:), allocatable :: v

!Free Green's functions matrices
	double precision :: mu
	double complex, dimension(:,:), allocatable :: lg0l,lg0g,lg0r
	double complex, dimension(:,:), allocatable :: lgal,lgag,lgar
	double complex, dimension(:,:), allocatable :: lgbl,lgbg,lgbr
	double complex, dimension(:,:), allocatable :: lgtl,lgtg,lgtr


!Embedding self-energy matrices for coupling with leads
	integer :: np
	integer, dimension(:), allocatable :: ixL,ixR, ixEta
	double precision :: eps, deps, epsmin, epsmax, Vl, Vr, Veta
	double complex, dimension(:,:), allocatable :: lsembl,lsembg,lsembr
	double complex, dimension(:,:), allocatable :: lsetal,lsetag,lsetar
	double complex, dimension(:,:), allocatable :: lsembl1,lsembr1, lsembl2,lsembr2

!Total self-energy matrices
	double complex, dimension(:,:), allocatable :: lsl,lsg,lsr
	double complex, dimension(:,:), allocatable :: lsrchk, inta, inta2

!NEGFs matrices
	double complex, dimension(:,:), allocatable :: lnegg,lnegl,lnegr


!NEGFs matrices DFT
	double complex, dimension(:,:), allocatable :: mklg0l,mklg0g,mklg0r
	double complex, dimension(:,:), allocatable :: mklnegg,mklnegl,mklnegr
!Self-energy matrices DFT
	double complex, dimension(:,:), allocatable :: mklsembl,mklsembg,mklsembr
	double complex, dimension(:,:), allocatable :: mklsetal,mklsetag,mklsetar
	double complex, dimension(:,:), allocatable :: mklsl,mklsg,mklsr

!Pulay coefficents
	double precision, dimension(:), allocatable :: c
	double complex, dimension(:,:,:), allocatable :: Ginr, Ginl, Ging
	double complex, dimension(:,:,:), allocatable :: Goutr, Goutl, Goutg


!Descrittore
	integer, dimension(9) :: desc!,descl

!Characters
	character(len=90) :: folder, hfolder

!Variables for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
	integer :: lldr,lldc,lldwc,pc,pr
	integer :: lldc_t,lldtc
	integer :: info_FW, nnprocs_FW, nprocs_FW, LC_nprocs
	integer :: lc_nw, lc_nt


!Grid spatial dimension
	integer :: ns

!Global dimensions of a matrix (ns x nt)
	integer :: n

!Running indices
integer :: i,j,ix,iy,ip,jp,iw,lw,liw,lir,lic,aa,aai,nrun,vvlds,numroc

!Temporary variables
	double precision :: dalpha,dalpha2,numpart,x,y, tempr, om, tshft, Kshft
	double precision, dimension(:), allocatable :: ggl,ggr, ggeta, tempdSLR
	double precision, dimension(:,:), allocatable :: gtemp
  double complex :: zalpha
	double precision :: start, finish, STARTALL, FINISHALL
	double precision :: maxcurr,maxcurrMB,icurr,icurrMB,fcurr,fcurrMB, mcurr, mcurl, ecurr, ecurl, el, er, mcondl, mcondr, mcurt, ecurt
	double precision, dimension(:), allocatable :: mcurd, mculd, ecurd, mflxd, eflxd
	double precision :: pzlange
	double precision, dimension(1:10) :: work
	double precision, dimension(2,1) :: DeltaSLR
	integer, dimension (1:9) :: desctmp
	double complex, dimension(:,:), allocatable :: intg, Id
	double complex, dimension(:,:), allocatable :: Ftestt,Ftestw,Ftestw1


	!temporary parameters
	integer :: ilambda, imix, imu, muctrl, irunmu
	integer ::  nlambda, nmix, nmu
	integer, dimension(:), allocatable :: muctrd
	double precision, dimension(:), allocatable :: dmucd

	double precision :: neta,rnmix, rnA1, mul,mur, muDiss
	double precision, dimension(:), allocatable :: muEta, betaEta,betaEta0, muEta0
	double precision :: dmucl, dmcurd,mcnorm, mcnorm0
	double precision, dimension(:), allocatable :: mixvec
	double precision, dimension(:), allocatable :: lambdavec
	double precision, dimension(:), allocatable :: mulvec, murvec

	!variables for inbedding
	double complex, dimension(:,:), allocatable :: lgil,lgig,lgir
	double complex, dimension(:,:), allocatable :: lnigl,lnigg,lnigr, lintmp
	double complex, dimension(:,:), allocatable :: lsinl,lsing,lsinr
	double precision, dimension(:), allocatable :: fbs,XI,fvec, fbs1,XI1,fvec1
	double precision, dimension(:), allocatable ::  pnumI,enrgI, v1tmp

!Variable for splitting MPI_FW
INTEGER :: colour_FW , DFT_comm_world , error, FW_RANK

!Variable for splitting MPI_subcluster
INTEGER :: colour_LC , LC_comm_world , error_LC, LC_RANK

!test MKL DFT
integer, parameter :: ltnw=nw, nnr=7, nnc=nx
integer :: SIZE_ldft, NX_IN, START_X, NX_OUT, START_X_OUT
double Complex :: Xmkl(ltnw)
double Complex :: Ymkl(ltnw)
double Complex :: zmkl(ltnw)

double Complex :: vXmkl(nx*ltnw)
double Complex :: vYmkl(nx*ltnw)
double Complex :: vZmkl(nx*ltnw)
double Complex :: Vworkl(ltnw*nx)

double Complex :: MXkl(nnr,nnc*ltnw)
double Complex :: MYkl(nnr,nnc*ltnw)
double Complex :: MZkl(nnr,nnc*ltnw)
double Complex :: Mworkl(nnr*nnc*ltnw)

double Complex, dimension(:), allocatable :: Xmkc
double Complex, dimension(:), allocatable :: Ymkc
double Complex, dimension(:), allocatable :: Zmkc

double Complex, dimension(:,:), allocatable :: MXkc
double Complex, dimension(:,:), allocatable :: MYkc
double Complex, dimension(:,:), allocatable :: MZkc
double Complex, dimension(:), allocatable ::  Mworkc

double Complex, dimension(1,nw) ::  areggime

double complex, dimension(:,:), allocatable :: DFT_distMX


type(DFTI_DESCRIPTOR), POINTER :: My_Desc1_Handle, My_Desc2_Handle
type(DFTI_DESCRIPTOR_DM), POINTER :: Desc_DM_WT
Integer :: Status
integer, dimension(:), allocatable :: vw_IN, vW_OUT
Integer, dimension(:), allocatable :: StridesVec



integer, dimension(5,1) :: param_FW
integer, dimension(:,:), allocatable :: isendvecw, isendvect

!___________________________________________________________________________!


allocate(StridesVec(ltnw))



	neta=eta0
	!nlambda=9
	nlambda=5
	nmix=7
	nmu=5
	allocate(mixvec(nmix))
	allocate(lambdavec(nlambda))
	allocate(mulvec(nmu))
	allocate(murvec(nmu))

mixvec=(/0.d0,1.d-2,5.d-2,2.d-1,5.d-1,9.d-1,1.d0/)
!lambdavec=(/0.d0,0.2d0,0.4d0, 0.6d0,0.8d0,0.9d0,1.d0,1.1d0,1.2d0/)
!lambdavec=(/1.d0/)
!lambdavec=(/0.d0,0.9d0,1.d0,1.1d0,0.8d0,0.6d0,1.2d0,0.4d0,0.2d0/)
lambdavec=(/0.d0,0.9d0,1.d0,1.2d0,0.6d0/)
!lambdavec=(/1.d0, 1.1d0, 1.2d0/)
mulvec=(/-2.d0,0.d0, 2.d0,2.d0,2.d0/)
murvec=(/-2.d0,-2.d0,-2.d0,0.d0,2.d0/)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!		Inizializzazione griglia (Start)
!--------------------------------------------------------------------

call sl_init(context,nnprow,nnpcol)
call mpi_comm_size(mpi_comm_world,nprocs,info)


if (nprocs/=nnprow*nnpcol) then
print*, '     Il numero di processi non corrisponde alla dimensione della griglia'
print*, nprocs, nnprow, nnpcol
stop
end if

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)


!__________________   FW MPI _____________________________________________!
! find appropriate nprocs_FW
nnprocs_FW=nprow
if (myrow==0.and.mycol==0) then
	liw=0
	do iw=nprow, floor((nw)**0.5d0)
		if ( mod(nw,iw) == 0 .and. iw<= nprocs ) then
			liw=1
		end if
	end do
	if (liw ==0 ) then
		print*, "DFT grid requirements: nw=i*j, (i,j)>nprow", nprow, nw
		stop
	end if

print*,' ', floor((nw)**0.5d0)
print*,' '
print*,'             Griglia scalapak fatta'
print*,' '
print*, 'Numero processi=', nprow,npcol,nprocs
print*, 'Numero processi FW', nnprocs_FW, nw,nw/nnprocs_FW,')'

end if


! split FW-MPI

if (mycol == 0) then
	colour_FW = 1
else
	colour_FW = MPI_UNDEFINED
end if

CALL MPI_Comm_split ( mpi_comm_world , colour_FW , 0 , DFT_comm_world , error )
call mpi_comm_size	( DFT_comm_world , nprocs_FW , error)
call mpi_comm_rank	( DFT_comm_world , FW_RANK, error)

if (  DFT_comm_world == MPI_COMM_NULL .and. colour_FW == 1) then
	print*, 'INIZIALIZIAZIONE DFT-MPI FALLITA'
	stop
else if (  nprocs_FW /= nnprocs_FW .and. colour_FW == 1) then
	print*, 'INIZIALIZIAZIONE DFT-MPI SBAGLIATA', nprocs_FW, nnprocs_FW
	stop
end if



!--------------------------------------------------------------------
!		Inizializzazione griglia (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

if (myrow==0.and.mycol==0) then
	call cpu_time(STARTALL)
end if

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!		Inizializzazione fourier (Start)
!--------------------------------------------------------------------
! Prepare DFT
if ( colour_FW == 1) then
	Status = DftiCreateDescriptorDM( DFT_comm_world, Desc_DM_WT, DFTI_DOUBLE, DFTI_COMPLEX, 1, nw )

	if (myrow==0.and.mycol==0) then
		print*, myrow, mycol, "STATUS CREAZIONE DFT", Status
	end if
	!
	Status = DftiSetValueDM( Desc_DM_WT , DFTI_FORWARD_SCALE, wm/(pi*(nw)))
	Status = DftiSetValueDM( Desc_DM_WT , DFTI_BACKWARD_SCALE, (pi/wm) )

	STATUS = DftiGetValueDM( Desc_DM_WT , CDFT_LOCAL_SIZE , SIZE_ldft)
	STATUS = DftiGetValueDM( Desc_DM_WT , CDFT_LOCAL_NX , NX_IN)
	STATUS = DftiGetValueDM( Desc_DM_WT , CDFT_LOCAL_OUT_NX , NX_OUT)
	STATUS = DftiGetValueDM( Desc_DM_WT , CDFT_LOCAL_X_START , START_X)
	STATUS = DftiGetValueDM( Desc_DM_WT , CDFT_LOCAL_OUT_X_START , START_X_OUT)
	!call sleep(5)

	! !if (myrow==0.and.mycol=00) then
		! print*, myrow, mycol, "size=", SIZE_ldft
		! print*, myrow, mycol, "NX_IN=", NX_IN
		! print*, myrow, mycol, "START_X=", START_X
		! print*, myrow, mycol, "NX_OUT=", NX_OUT
		! print*, myrow, mycol, "START_X_OUT=", START_X_OUT
	! !end if
	param_FW(1,1)=SIZE_ldft
	param_FW(2,1)=NX_IN
	param_FW(3,1)=START_X
	param_FW(4,1)=NX_OUT
	param_FW(5,1)=START_X_OUT
end if

if ( mycol == 0) then
	call igebs2d( context, "R", "i-ring", 5, 1, param_FW, 5 )
else
	call igebr2d( context, "R", "i-ring", 5, 1, param_FW, 5 , myrow, 0)

SIZE_ldft=param_FW(1,1)
NX_IN=param_FW(2,1)
START_X=param_FW(3,1)
NX_OUT=param_FW(4,1)
START_X_OUT=param_FW(5,1)
end if

!--------------------------------------------------------------------
!		Inizializzazione fourier (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Inizializzazione (Start)
!--------------------------------------------------------------------


lc_nw = NX_IN
lc_nt = NX_OUT

if (d==1) then
	ns=nx
else if (d==2) then
	ns=nx*ny
end if
n=nw*ns

! LLD for MPI main process
lldr=numroc(ns,mb,myrow,0,1)

lldwc=numroc(lc_nw,1,mycol,0,npcol)
lldc = lldr*lldwc

lldtc=numroc(lc_nt,1,mycol,0,npcol)
lldc_t = lldr*lldtc



!Matrix allocation
allocate(onsite(nx),nnhop(nx-1),ham(ns,ns),s(ns,ns),hamhf(ns,ns),shf(ns,ns))
allocate(w(ns),wh(ns),pnum0(ns),hnum0(ns),pnum(ns),hnum(ns))
allocate(intg(ns,ns))
allocate(rho(ns,ns))
allocate(dos(nw))
allocate(specfun(nw,ns))
allocate(Id(ns,ns))
allocate(Ftestw(nw,1))
allocate(Ftestw1(nw,1))
allocate(Ftestt(nw,1))


!Dimension of local matrices in the row-BCD
allocate(vw_lc(lldwc))
allocate(vt_lc(lldtc))
allocate(vw_ilc(lldwc))
allocate(vt_ilc(lldtc))

allocate(vw_c(NX_IN))
allocate(vt_c(NX_OUT))
allocate(isendvecw(NX_IN,1))
allocate(isendvect(NX_OUT,1))

allocate(v(ns,ns))
allocate(inta(ns,ns),inta2(ns,ns))
allocate(ixL(sitelds),ixR(sitelds), ixeta(ns))
allocate(muEta(ns), betaEta(ns), betaEta0(ns), mcurd(ns), ecurd(ns), muEta0(ns))
allocate(mflxd(ns),eflxd(ns))
allocate(ggl(ns),ggr(ns),ggeta(ns), gtemp(ns,1))

allocate(lg0l(lldr,lldc),lg0g(lldr,lldc),lg0r(lldr,lldc))
allocate(lgtl(lldr,lldc_t),lgtg(lldr,lldc_t),lgtr(lldr,lldc_t))
allocate(lnegl(lldr,lldc),lnegg(lldr,lldc),lnegr(lldr,lldc))

allocate(lsembl(lldr,lldc),lsembg(lldr,lldc),lsembr(lldr,lldc))
allocate(lsetal(lldr,lldc),lsetag(lldr,lldc),lsetar(lldr,lldc))
allocate(lsembl1(lldr,lldc),lsembr1(lldr,lldc),lsembl2(lldr,lldc),lsembr2(lldr,lldc))
allocate(lsl(lldr,lldc),lsg(lldr,lldc),lsr(lldr,lldc))
allocate(lsrchk(lldr,lldc))

allocate(Ginr(mem,lldr,lldc), Ginl(mem,lldr,lldc), Ging(mem,lldr,lldc))
allocate(Goutr(mem,lldr,lldc),Goutl(mem,lldr,lldc),Goutg(mem,lldr,lldc))
allocate(c(mem+1))
allocate(tempdSLR(mem+1))
allocate(mculd(ns))
allocate(muctrd(ns),dmucd(ns))

allocate(lgal(lldr,lldc),lgag(lldr,lldc),lgar(lldr,lldc))
allocate(lgbl(lldr,lldc),lgbg(lldr,lldc),lgbr(lldr,lldc))

allocate(lgil(1,lldwc),lgig(1,lldwc),lgir(1,lldwc))
allocate(lnigl(1,lldwc),lnigg(1,lldwc),lnigr(1,lldwc))
allocate(lintmp(1,lldwc))
allocate(lsinl(1,lldwc),lsing(1,lldwc),lsinr(1,lldwc))
allocate(fbs(2),XI(2),fvec(2))
allocate(fbs1(1),XI1(1),fvec1(1))

allocate(pnumI(ns),enrgI(ns))
allocate(v1tmp(1))


!test MKL DFT
Allocate(MXkc(nx,nx*lldwc),MYkc(nx,nx*lldwc),MZkc(nx,nx*lldwc))
allocate(Mworkc(nnr*nnc*lldwc))


call descinit(desc,ns,n,mb,nb,0,0,context,lldr,info)


if (myrow==0.and.mycol==0) then
	print*, 'Inizializzazione fatta'
end if


!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Time-Frequency grid (Start)
!--------------------------------------------------------------------

if ( mycol == 0) then
call wtgrids(context, Desc_DM_WT , vw_c , vt_c)
isendvecw(:,1)=vw_c
isendvect(:,1)=vt_c
end if


if ( mycol == 0) then
	call igebs2d( context, "R", "i-ring", NX_IN, 1, isendvecw, NX_IN )
else
	call igebr2d( context, "R", "i-ring", NX_IN, 1, isendvecw, NX_IN , myrow, 0)
end if

if ( mycol == 0) then
	call igebs2d( context, "R", "i-ring", NX_OUT, 1, isendvect, NX_OUT )
else
	call igebr2d( context, "R", "i-ring", NX_OUT, 1, isendvect, NX_OUT , myrow, 0)
end if

if ( mycol /= 0) then
vw_c(:)=isendvecw(:,1)
vt_c(:)=isendvect(:,1)
end if


call wgrid2d_lc(npcol, mycol, vw_c, vw_lc, vw_ilc)
call wgrid2d_lc(npcol, mycol, vt_c, vt_lc, vt_ilc)





!--------------------------------------------------------------------
!			Inizializzazione (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

do i=1,ns
		Id(i,i)=1.d0
end do
do ilambda=1, 1!nlambda
	do imix=1, 1!nmix
		do imu=1,1!nmu!1,1!nmu!51,51!1,101!1,11!4,8!
			! mul=mulvec(imu)
			! mur=murvec(imu)
			mul=dmul
			mur=dmur
			muDiss=-wm!0.d0!(mur+mul)/2.d0
			muctrl=1
			dmucl=0.5d0!(mul-mur)/2.d0


			muEta=-wm!0.d0!(mur+mul)/2.d0
			if (myrow==0 .and. mycol==0) then
				print*, "MUs", mul, mur, muDiss
			end if


rnmix=mix
!rnA1=lambdavec(ilambda)
!rnmix=mixvec(imix)
rnA1=A1
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Interaction (Start)
!--------------------------------------------------------------------
U=dU
v=0.d0
!Interaction in the self-enrgIes
do i=1,ns
	v(i,i)=U
end do
!--------------------------------------------------------------------
!			Interaction (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


!do vvlds=vvldsi,vvldsf

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Folder (Start)
!--------------------------------------------------------------------

if (myrow==0.and.mycol==0) then
	folder='testfw_'//exc//"nx"//int2char(nx)&
								&//"L"//int2char(ceiling(1.d1*rnA1))&
								&//"U"//int2char(ceiling(1.d1*dU))&
								&//"gl2"//int2char(floor(10*gr*gl))&
								&//"mu"//int2char(ceiling(100*mul))//"-"//int2char(ceiling(100*mur))&
								&//"nw"//int2char(nw)&
								&//"wm"//int2char(floor(wm))&
								&//"et"//int2char(ceiling(LOG10(neta)))&
                &//"npr"//int2char(nprocs)&
								&//"mem"//int2char(mem)&
								&//"mix"//int2char(ceiling(100*rnmix))//'/'

	hfolder=folder
	call system('mkdir '//trim(folder))
	call system('mkdir '//trim(hfolder))


end if
call   blacs_barrier(context, 'A')
!--------------------------------------------------------------------
!			Folder (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Hamiltonian (Start)
!--------------------------------------------------------------------

!Create Hamiltonian
onsite=0.d0
nnhop=0.d0

do i = 1, nx
	onsite(i)= rnA1*cos(2*pi*tau*i) !- dU*0.5d0
end do

do i = 1, nx-1
!HAS TO BE NEGATIVE!
	nnhop(i)=-0.5d0
end do

if (d==1) then
	call tbham(nx,bcx,onsite,nnhop,ham)
else if (d==2) then
	call tbham2d(nx,ny,bcx,bcy,onsite,gmt,ham)
end if

call blacs_barrier(context,'A')

!Diagonalize Hamiltonian

call diagsy(ns,ham,w,s,0,'0')
call blacs_barrier(context,'A')


if (myrow==0 .and. mycol==0) then
	print*, "diagonalizzazione fatta"
end if
!--------------------------------------------------------------------
!		Hamiltonian (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!		Embedding Self enrgIes (Start)
!--------------------------------------------------------------------
lsembr=zero
lsembl=zero
lsembg=zero

!pos left lead
ggl=0.d0
ggl(1)=gl
do iy=1,ny
	ixL(iy)=1+nx*(iy-1)
end do

!pos right lead
ggr=0.d0
ggr(1)=gr
do iy=1,ny
	ixR(iy)=nx+nx*(iy-1)
end do

!pos "eta-leads"
gtemp=0.d0
ggeta=0.d0
do i=1,ns
	gtemp(i,1)=sqrt(2.d0*neta)
	ixEta(i)=i
end do
!gtemp=matmul(s,gtemp)
ggeta(:)=gtemp(:,1)


if(myrow==0.and.mycol==0) then
	open(unit=21,file=trim(folder)//'xL.dat',status='unknown')
	open(unit=22,file=trim(folder)//'xR.dat',status='unknown')
	open(unit=23,file=trim(folder)//'xEta.dat',status='unknown')
	write(21,'(I3,5x)') ixL
	write(22,'(I3,5x)') ixR
	write(23,'(I3,5x)') ixEta
	close(21)
	close(22)
	close(23)
end if

lsr=zero
lsl=zero
lsg=zero

!Adds leads coupled to each site
do i=1, ny

	Vl=-1.d0*aa*dVg+dVl-vvlds*1.d-2
	call wembslfen(context,ns,nw,wm,vw_lc,(/iXL(i)/),ggl,betal,mul,0.d0,'culo',Vl,0.d0,-1,lsembl,lsembg,lsembr)
	lsr=lsr+lsembr
	lsl=lsl+lsembl
	lsg=lsg+lsembg


	Vr=-1.d0*aa*dVg+dVr+vvlds*1.d-2
	call wembslfen(context,ns,nw,wm,vw_lc,(/ixR(i)/),ggr,betar,mur,0.d0,'culo',Vr,0.d0,-1,lsembl,lsembg,lsembr)
	lsr=lsr+lsembr
	lsl=lsl+lsembl
	lsg=lsg+lsembg

end do

lsembr=lsr
lsembl=lsl
lsembg=lsg

if (myrow==0 .and. mycol==0) then
	print*, "Selfenergies 1 fatte"
end if

if (myrow==0.and.mycol==0) then
	call cpu_time(start)
end if
	Veta=-1.d0*aa*dVg


call wembslfenDiss(context,ns,nw,wm,vw_lc,ixEta,ggeta,betaEta,muEta,0.d0,'culo',Veta,0.d0,-1,lsetal,lsetag,lsetar)

if (myrow==0 .and. mycol==0) then
	print*, "Selfenergies fatte"
end if
!--------------------------------------------------------------------
!		Embedding Self enrgIes (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Free Retarded (Start) (with diss)
!--------------------------------------------------------------------

do iw=1, lldwc
	liw = (iw-1)*ns+1
	om=-wm+2.d0*wm*(vw_lc(iw)-1)/(nw-1)
	lg0r(1:ns,liw:liw+ns-1)= om*Id(1:ns,1:ns) - ham(1:ns,1:ns) -lsetar(1:ns,liw:liw+ns-1)
	call locinv(ns,lg0r(1:ns,liw:liw+ns-1))
end do
if (myrow==0 .and. mycol==0) then
	print*, "Free Ret fatta"
end if
!--------------------------------------------------------------------
!			Free Retarded (End) (with diss)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


muEta=-wm
betaEta=beta
muDiss=sum(muEta)/(ns)

!_______Diss-Green (Start)____________________________________________________

call wembslfenDiss(context,ns,nw,wm,vw_lc,ixEta,ggeta,betaEta,muEta,0.d0,'culo',Veta,0.d0,-1,lsetal,lsetag,lsetar)

call wfreeg3(context,ns,nw,wm,vw_lc,w,s,beta,-1,lg0r,lg0l,lg0g,lsetar,lsetal,lsetag,ham,neta,desc)
call blacs_barrier(context, 'A')

!________Include leads (Start)________________________________________________
lsr=lsembr!+lsetar
lsl=lsembl+lsetal
lsg=lsembg+lsetag

!il parametro numerico :: "0"-> rigenera nuove negl, negg usando lsl, lsg // "-1" toglie il sontributo della selfenrgIa delle lg0r  //"1" lo aggiunge
call wdyson(context,ns,nw,wm,vw_lc,muDiss,beta,0,lg0l,lg0g,lg0r,lsl,lsg,lsr,lnegl,lnegg,lnegr)

lsr=lsembr+lsetar
lsl=lsembl+lsetal
lsg=lsembg+lsetag

if (myrow==0 .and. mycol==0) then
	print*, "Free Green fatte"
end if

call CPnegf(context,nx,ny,nw,wm,vw_lc,d,lg0l,lg0r, desc, "0", folder)

call CPnegf(context,nx,ny,nw,wm,vw_lc,d,lnegl,lnegr, desc, "E", folder)
call CPcurrents3(context,ns,ny,nw,wm,vw_lc,-1, iXL,iXR,ixEta, ggL,ggR,ggeta, betaL,betaR,betaEta, muL,muR,muEta, VL,VR,Veta, lnegl,lnegr,lsr, desc, "E",folder,hfolder)

if (myrow==0.and.mycol==0) then
        call cpu_time(finish)
        print*, " time CP(s)= ", finish-start
				call cpu_time(start)
end if


!--------------------------------------------------------------------
!			Include leads (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!______________________________________________________________________________!
!******************************************************************************
!------------------- INTERACTING-----------------------------------------------!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!
if (myrow==0 .and. mycol==0) then
	print*, "suca int"
end if

nrun=1
mcurl=1.d2
mcurr=1.d3

lsrchk=zero
tempdSLR=0.d0
if (myrow==0.and.mycol==0) then
	print*, "curr MB = ", mcurl , abs(mcurl-mcurr)
	open(unit=25,file=trim(folder)//'currMB.dat',status='unknown')
	open(unit=26,file=trim(folder)//'currDIFF.dat',status='unknown')
	open(unit=27,file=trim(folder)//'times'//exc//'.dat',status='unknown')
	open(unit=28,file=trim(folder)//'DslrD'//exc//'.dat',status='unknown')
	open(unit=29,file=trim(folder)//'DslrT'//exc//'.dat',status='unknown')

end if


!DeltaSLR(2,1).ge. 1.d-2,  tempr.ge. wm*1.d-2
do while((nrun<=maxrun.and.(Abs(mcurl)>1.d-6.or.abs(mcurl-mcurr).ge.1.d-8 .or. ANY(tempdSLR > wm*1.d-3))).or.nrun<=minrun)
	if(myrow==0.and.mycol==0) then
		print*, nrun, ANY(tempdSLR > wm*1.d-2)!, tempr
		print*, tempdSLR
	end if
	!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	!			NEGFs (Start)
	!--------------------------------------------------------------------
	lsr=zero
	lsl=zero
	lsg=zero

	if (myrow==0.and.mycol==0) then
					call cpu_time(start)
	end if


	if(nrun>mem) then
		call mixing(context,ns,nw,wm,mem,Ginr,Goutr,desc,c)
	end if

	if(nrun>mem) then

	lnegr=zero
	lnegl=zero
	lnegg=zero

	do i=1,mem
		lnegr=lnegr+(1.d0-rnmix)*c(i)*Goutr(i,:,:) + rnmix*c(i)*Ginr(i,:,:)
		lnegl=lnegl+(1.d0-rnmix)*c(i)*Goutl(i,:,:) + rnmix*c(i)*Ginl(i,:,:)
		lnegg=lnegg+(1.d0-rnmix)*c(i)*Goutg(i,:,:) + rnmix*c(i)*Ging(i,:,:)
	end do
	end if


	Ginr(mod(nrun,mem)+1,:,:)=lnegr(:,:)
	Ginl(mod(nrun,mem)+1,:,:)=lnegl(:,:)
	Ging(mod(nrun,mem)+1,:,:)=lnegg(:,:)


	!HF PART
	call whf(context,ns,nw,wm,v,lnegl,desc,lsr)


	if (exc=='b') then
	!Second Born
		!call wsndb(context,ns,nw,wm,vw_lc,v,lnegl,lnegg,desc,lsl,lsg,lsr)
		call wsndbDFT(context, desc, Desc_DM_WT,ns,nw, NX_IN, NX_OUT, &
			 									& wm,vw_lc, vw_ilc, vt_ilc, v,&
												& lnegl,lnegg,lsl,lsg,lsr)
	else if (exc=='gw') then
	!GW
		call wgw(context,ns,nw,wm,vw_lc,v,lnegl,lnegg,desc,lsl,lsg,lsr)
	end if

	lsr=lsr+lsembr   !!!!!NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
	lsl=lsl+lsembl
	lsg=lsg+lsembg

	call wdyson(context,ns,nw,wm,vw_lc,mu,beta,-1,lg0l,lg0g,lg0r,lsl,lsg,lsr,lnegl,lnegg,lnegr)

	if (myrow==0.and.mycol==0) then
	        call cpu_time(finish)
	        print*, " time exc + Dyson= ", finish-start
	end if


	Goutr(mod(nrun,mem)+1,:,:)=lnegr(:,:)
	Goutl(mod(nrun,mem)+1,:,:)=lnegl(:,:)
	Goutg(mod(nrun,mem)+1,:,:)=lnegg(:,:)

	lsr=lsr-lsembr
	lsl=lsl-lsembl
	lsg=lsg-lsembg


	!!!!check self-consist self-energy
	lsrchk=lsrchk-lsr

	DeltaSLR=0.d0
	do iw=1,lldwc
		liw=(iw-1)*ns+1
		do i=1, ns
			DeltaSLR(1,1)=DeltaSLR(1,1)+lsrchk(i,liw-1 +i)*conjg(lsrchk(i,liw-1 +i))
			do j=1,ns
				DeltaSLR(2,1)=DeltaSLR(2,1)+lsrchk(i,liw-1 +j)*conjg(lsrchk(i,liw-1 +j))
			end do
		end do
	end do

	call dgsum2d(context, 'A', 'i-ring', 2, 1, DeltaSLR, 2, -1, -1)
	DeltaSLR(1,1)=DeltaSLR(1,1)*2.d0*wm/nw
	DeltaSLR(2,1)=DeltaSLR(2,1)*2.d0*wm/nw

	if (myrow==0.and.mycol==0) then
		print*,"DeltaSLR=", DeltaSLR(1,1), DeltaSLR(2,1)
	end if

	if (nrun<=mem) then
		tempdSLR(nrun+1)=DeltaSLR(2,1)
	else
		do i=1, mem
			tempdSLR(i)=tempdSLR(i+1)
		end do
		tempdSLR(mem+1)=DeltaSLR(2,1)
	end if
	!tempr=DeltaSLR(2,1)!abs(mcurl*10.d0**(16))!DeltaSLR(2,1)

	lsrchk=lsr
	!!!!!
	! check self-consist - end



	mcurr=mcurl
	mcurl=1.d2
	call wmcurr(context,ns,nw,wm,lnegl,lnegr,lsl,lsr,desc,-1,-1,mcurl)

	if (myrow==0.and.mycol==0) then
		print*, "curr MB = ", mcurl , abs(mcurl-mcurr)
		write(25,*) mcurl
		write(26,*) abs(mcurl-mcurr)
		write(27,*) finish-start
		write(28,*) DeltaSLR(1,1)
		write(29,*) DeltaSLR(2,1)

	end if

	nrun=nrun+1
	!--------------------------------------------------------------------
	!			NEGFs (End)
	!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

end do


if (myrow==0.and.mycol==0) then
	close(25)
	close(26)
	close(27)
	close(28)
	close(29)
end if


call CPnegf(context,ns,ny,nw,wm,vw_lc,d,lnegl,lnegr, desc, "M", folder)
call CPcurrents3(context,ns,ny,nw,wm,vw_lc,-1, iXL,iXR,ixEta, ggL,ggR,ggeta, betaL,betaR,betaEta, muL,muR,muEta, VL,VR,Veta, lnegl,lnegr,lsr, desc, "M",folder,hfolder)








!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!		FW test (tempi)
!--------------------------------------------------------------------

if (myrow==0.and.mycol==0) then
		print*, " nx= ", nx, "nw=", nw
    print*, " vai con le fourier "
end if
 Status = DftiCommitDescriptorDM( Desc_DM_WT )


call fourierDFwt( context , Desc_DM_WT , SIZE_ldft, NX_IN, NX_OUT , vw_ilc , vt_ilc, ns , lnegl , 1 , lgtl )
if (myrow==0.and.mycol==0) then
        call cpu_time(finish)
        print*, " time DFWT= ", finish-start
				call cpu_time(start)
end if

call fourierDFwt( context , Desc_DM_WT , SIZE_ldft, NX_IN, NX_OUT , vw_ilc , vt_ilc, ns , lnegl , -1 , lgtl )
if (myrow==0.and.mycol==0) then
        call cpu_time(finish)
        print*, " time b-DFWT= ", finish-start
				call cpu_time(start)
end if

! call fourierDFwtBIG( context , Desc_DM_WT , vw_ilc , vt_ilc, ns , lnegl , 1 , lgtl )
! if (myrow==0.and.mycol==0) then
!         call cpu_time(finish)
!         print*, " time DFWT_BIG= ", finish-start
! 				call cpu_time(start)
! end if

! call fourierwtSUM(context,ns,nw,wm,1,lnegl,desc,lg0l)
! if (myrow==0.and.mycol==0) then
!         call cpu_time(finish)
!         print*, " time fourier nuova= ", finish-start
! 				call cpu_time(start)
! end if

!
! call fourierwtSUMh(context,ns,nw,wm,1,lnegl,desc,lg0l)
! if (myrow==0.and.mycol==0) then
!         call cpu_time(finish)
!         print*, " time FWsumh= ", finish-start
! 				call cpu_time(start)
! end if


!--------------------------------------------------------------------
!			FW test (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++






!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Print Physical Quantities (Start)
!--------------------------------------------------------------------

if (myrow.eq.0 .and. mycol.eq.0) then
        open(unit=21,file=trim(folder)//'parameters.dat',status='unknown')
        write(21,'(A2,T5,I3)') "nx", nx
        write(21,'(A2,T5,F12.6)') "wm", wm
        write(21,'(A2,T5,I5)') "nw", nw
        write(21,'(A2,T5,F12.6)') "dw", 2*wm/(nw-1)
        write(21,'(A2,T5,F12.6)') "dU", dU
				write(21,'(A2,T5,I5)') "mem", mem
        write(21,'(A2,T5,F12.6)') "mx", rnmix
        write(21,'(A2,T5,F12.6)') "bt", beta
        write(21,'(A2,T5,F12.6)') "mu", muDiss
				write(21,'(A3,T5,F12.6)') "dmu", dmul
        write(21,'(A2,T5,F12.6)') "gl", gl
        write(21,'(A2,T5,F12.6)') "bl", betal
        write(21,'(A2,T5,F12.6)') "gr", gr
        write(21,'(A2,T5,F12.6)') "br", betar
        write(21,'(A2,T5,F12.6)') "ml", mul
        write(21,'(A2,T5,F12.6)') "mr", mur
        write(21,'(A2,T5,F12.6)') "Vl", Vl
        write(21,'(A2,T5,F12.6)') "Vr", Vr
				write(21,'(A2,T5,I4)') "nprocs", nprocs
        close(21)
end if



if(myrow==0.and.mycol==0) then
        open(unit=21,file=trim(folder)//'ham.dat',status='unknown')
	do i=1, ns
                write(21,'(E14.6E3,5x)') ham(i,:)
	end do
        close(21)
end if
if(myrow==0.and.mycol==0) then
        open(unit=21,file=trim(folder)//'eval.dat',status='unknown')
        write(21,'(E14.6E3,5x)') w
        close(21)
end if



if (myrow.eq.0 .and. mycol.eq.0) then

	open(unit=22,file=trim(folder)//'pops.dat',status='unknown',access='append')
	write(22,'(E14.6E3,5x)') aa*dVg, ggl(1), sum(pnum)
	close(22)

	open(unit=24,file='end.txt',status='unknown')
	if (aa==af) then
		write(24,'(I1)') 1
	else
		write(24,'(I1)') 0
	end if
	close(24)
end if


!--------------------------------------------------------------------
!			Print Physical Quantities (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


! do sul gate voltage
! end do 		!aa
!
! do sul coupling coi leads
! end do   !vvlds

if (myrow==0.and.mycol==0) then
       call system('mv *dat '//trim(folder))
			 ! call system('mkdir '//'spanN_HF34/')
			 ! call system('mkdir '//'spanN_HF34_pesante/')
			 ! call system('mv '//trim(folder)//' spanN_HF34/')
			 ! call system('mv '//trim(hfolder)//' spanN_HF34_pesante/')
end if
!Exit grid


end do
end do
end do


	if (myrow==0.and.mycol==0) then
	        call cpu_time(FINISHALL)
	        print*, " TIMEALL= ", FINISHALL-STARTALL
	end if


call MPI_Comm_free ( DFT_comm_world , error )
call blacs_gridexit(context)
call blacs_exit(0)

if (myrow==0.and.mycol==0) then
				print*, " FORA", error
end if

end program wifhm
