! ============================================================================
! Name: wifhm_leads_tgrid.f90
! Author      : N. Lo Gullo
! Version     : 1.0
! Copyright   : GPL
! Description : Evolution for weakly interacting fermions in 1D and 2D
!       by means of a self-iterative method based on the NEGFs.
!       Both spatial and time variable are split onto a grid.
! Date: 08/12/2017
! ============================================================================

Program wifhm
use parameters
use intchar
use libness
implicit none
include 'mpif.h'
!Free Hamiltonian, eigenvalues, Change basis matrix
double precision, dimension(:), allocatable :: onsite
double precision, dimension(:,:), allocatable :: onsita
double precision, dimension(:), allocatable :: nnhop
double precision, dimension(:,:), allocatable :: ham
double precision, dimension(:), allocatable :: w
double precision, dimension(:,:), allocatable :: s
double precision, dimension(:,:), allocatable :: hamhf
double precision, dimension(:), allocatable :: wh
double precision, dimension(:,:), allocatable :: shf
integer, dimension(:), allocatable :: vw
double precision, dimension(:), allocatable :: pnum0, pnum
double precision, dimension(:), allocatable :: hnum0, hnum
double precision, dimension(:), allocatable :: omega
double precision, dimension(:), allocatable :: omm
double precision, dimension(:), allocatable :: dos
double precision, dimension(:,:), allocatable :: specfun, specfunx

double precision, dimension(:,:), allocatable :: Momega
double precision :: dalphaL, dalphaR

!Interaction Matrix
double precision :: U
double precision, dimension(:,:), allocatable :: v

!Determinants
double precision, dimension(:,:), allocatable :: DetL, DetR
double precision, dimension(:,:), allocatable :: DetL1, DetR1
double precision :: Dettemp


!Free Green's functions matrices
double precision :: mu
double complex, dimension(:,:), allocatable :: lg0l,lg0g,lg0r
double complex, dimension(:,:), allocatable :: lgal,lgag,lgar
double complex, dimension(:,:), allocatable :: lgbl,lgbg,lgbr

!Embedding self-energy matrices for coupling with leads
integer :: np
integer, dimension(:), allocatable :: ixL,ixR
double precision :: eps, deps, epsmin, epsmax, Vl, Vr
double complex, dimension(:,:), allocatable :: lsembl,lsembg,lsembr
double complex, dimension(:,:), allocatable :: lsemblL,lsembgL,lsembrL

!Total self-energy matrices
double complex, dimension(:,:), allocatable :: lsl,lsg,lsr

!NEGFs matrices
double complex, dimension(:,:), allocatable :: lnegg,lnegl,lnegr

double precision, dimension(:), allocatable :: GrDetemp
double complex, dimension(:,:), allocatable :: GrDet
double complex, dimension(:,:), allocatable :: fTfun
double complex, dimension(:,:), allocatable :: CurrN
double precision, dimension(:,:), allocatable :: curB, Tcurr


!Pulay coefficents
double precision, dimension(:), allocatable :: c
double complex, dimension(:,:,:), allocatable :: Ginr, Ginl, Ging
double complex, dimension(:,:,:), allocatable :: Goutr, Goutl, Goutg


!Descrittore
integer, dimension(9) :: desc

!Characters
character(len=60) :: folder, folderIsoH
character(len=60) :: Mainfolder
Character (len=60), Allocatable :: folders(:)


!Variables for grid query
integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
integer :: lldr,lldc,lldwc,pc,pr
double complex, dimension(:,:), allocatable :: intg

!Grid spatial dimension
integer :: ns

!Subgrid
integer :: nnw
integer :: nfww

!Global dimensions of a matrix (ns x nt)
integer :: n

!Running indices
integer :: i,j,jj,ix,iy,ip,jp,iw,liw,aa,nrun,vvlds,numroc, ilds,fww, jw, in, ipp

!Temporary variables
double precision :: dalpha,numpart,ggl,ggr,x,y, om
double complex :: zalpha
double precision :: start, finish, tcicl0,  tcicl1, tDetin, tDetfin, tG2rin, tG2rfin, &
& tstart, tgrigl, tinit, texit, tfTfin, tcurrfin, tTcurrin, tTcurrfin
double precision :: maxcurr,maxcurrMB,icurr,icurrMB,fcurr,fcurrMB, mcurr, mkcurr, mcurl, mkcurl,wkcurl, ecurr, ecurl, el, er
double precision :: pzlange
double precision, dimension(1:10) :: work
integer, dimension (1:9) :: desctmp

!hamiltonian names
double precision :: randonn
double precision :: lambda, phirand, eigmean
Character (len=2) :: nomeham,nomhubbard,nomanderson,nomandre, nomandranL,nomandranT, nomandranD, nomebonacci, nomeaperiod
Character (len=65), Allocatable :: nomiham(:)
integer :: nnomham, nnlds, shiftlds

double precision, dimension(:), allocatable :: ST

double precision, dimension(:,:), allocatable :: Bandlims, fTftemp


!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!       Used Hamiltonians
!--------------------------------------------------------------------
nomhubbard = "HB"; nomanderson = "AN";nomandre = "AA"; nomandranL="Al";nomandranT="At"; nomebonacci="FB"
nomeham=nomandre
nnomham=9; nnlds=10; shiftlds=5
!nnomham=4; nnlds=1; shiftlds=1

Allocate (nomiham(nnomham))
allocate(folders(nx))

do i=1, nnomham
        nomiham(i)=Trim(nomeham)//int2char(i)
end do

nfww=500






!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!       Inizializzazione griglia (Start)
!--------------------------------------------------------------------
call cpu_time(tstart)

call sl_init(context,1,nnprow*nnpcol)            !inizializza una griglia di dimensioni [1,2*4], gli da l'etichetta context
call mpi_comm_size(mpi_comm_world,nprocs,info)   !determina la taglia del gruppo. nprocs è l'output. mpi_comm_world dovrebbe esse imput(?)

if (nprocs/=nnprow*nnpcol) then
        print*, '     Il numero di processi non corrisponde alla dimensione della griglia'
        stop
end if

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)  !determina la coordinata della griglia in cui stai. Non mi è chiaro da dove vengono nprow e npcol


if (myrow==0.and.mycol==0) then
        print*,' '
        print*,' '
        print*,'     Inizio evolutione'
        print*,' '
        print*, 'Numero processi=', nprow,npcol,nprocs
end if
if (myrow==0.and.mycol==0) then
        print*, 'Griglia fatta'
end if
if (myrow==0.and.mycol==0) then
        call cpu_time(tgrigl)
        print*, " tempo Griglia= ", tgrigl-tstart
end if
!--------------------------------------------------------------------
!       Inizializzazione griglia (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!       Inizializzazione (Start)
!--------------------------------------------------------------------

if (d==1) then
        ns=nx
        else if (d==2) then
        ns=nx*ny                   !numero di siti(fisici direi)
end if
n=nw*ns                            !nw è numero di frequenze

!Matrix allocation
allocate(onsite(nx),onsita(nx,1),nnhop(nx-1),ham(ns,ns),s(ns,ns),hamhf(ns,ns),shf(ns,ns))
        allocate(w(ns),wh(ns),pnum0(ns),hnum0(ns),pnum(ns),hnum(ns))
        allocate(intg(ns,ns))
        allocate(dos(nw))
        allocate(specfun(nw,ns),specfunx(nw,ns))

!local leadin dimensions
!(#tot righe/colonne,taglia dei blocchi in cui è distr la matrice, coordinata nella griglia, coordinata di inizializzazione, #tot di processi su cui sei distribuito)
lldr=numroc(ns,mb,myrow,0,1)                  !lldrighe= (nx*ny,nx*ny,/,dist su 1 riga/processo)        ex:seconodo i miei calcoli 1      // ci ripenso e dico ns
lldc=numroc(n,nb,mycol,0,npcol*nprow)         !lldcolonne= (nx*ny*nw,nx*ny,/,dist su nnproc)            ex:seconodo i miei calcoli 501(500)[*16?] //a rimensarci si 500*16
lldwc=numroc(nw,1,mycol,0,npcol*nprow)        !lldcolonneW= (nw,1,/,dist su nnproc)                     ex:seconodo i miei calcoli 501(500)   // e questo invece lo confermo

!Dimension of local matrices in the row-BCD
allocate(vw(lldwc))
        allocate(v(ns,ns))
        allocate(omega(nw))
        allocate(Momega(nw,1))
        allocate(ixL(sitelds),ixR(sitelds))
        allocate(lg0l(lldr,lldc),lg0g(lldr,lldc),lg0r(lldr,lldc))
        allocate(lsembl(lldr,lldc),lsembg(lldr,lldc),lsembr(lldr,lldc))
        allocate(lsemblL(lldr,lldc),lsembgL(lldr,lldc),lsembrL(lldr,lldc))
        allocate(lsl(lldr,lldc),lsg(lldr,lldc),lsr(lldr,lldc))
        allocate(lnegl(lldr,lldc),lnegg(lldr,lldc),lnegr(lldr,lldc))
        allocate(Ginr(mem,lldr,lldc), Ginl(mem,lldr,lldc), Ging(mem,lldr,lldc))
        allocate(Goutr(mem,lldr,lldc),Goutl(mem,lldr,lldc),Goutg(mem,lldr,lldc))
        allocate(c(mem+1))

        allocate(lgal(lldr,lldc),lgag(lldr,lldc),lgar(lldr,lldc))
        allocate(lgbl(lldr,lldc),lgbg(lldr,lldc),lgbr(lldr,lldc))

        allocate(GrDet(lldr,lldc))
        allocate(GrDetemp(2*nfww+1))
        allocate(fTfun(lldr,lldc))
        allocate(Tcurr(ns,nw))
        allocate(CurrN(ns,ns))
        allocate(curB(ns,nnlds))


        allocate(ST(nx))
        allocate(Bandlims(Floor(1/tau),2))
        allocate(fTftemp(nw,1))



!desc=[1,context, ns, n. mb,nb,0,0,lldr] (direi che contiene le info per distr le matrici)
!matrice ns*n, divisa in blocchi mb*nb, partendo da {0,0}, con lld=lldr, nel ontesto context
call descinit(desc,ns,n,mb,nb,0,0,context,lldr,info)  !cruciale per le scalapac

!restituisce gli w presenti su {nprow,npcol}, e tutte le freq accessibili
call wgrid(context,nw,wm,vw,1,omega)                  !{0,2}->vw={3,11,19,...}, omega={-1,-0,998,-0,996....1}*wm

if (myrow.eq.0 .and. mycol.eq.0) then
        open(unit=21,file='grid.dat',status='unknown')
        do iw=1,nw
                write(21,*) omega(iw)
        end do
        close(21)
end if



!nnw=size(vw)
if (nfww.le.0) then
        nnw=size(vw)
else
        nnw=size(vw)*(2*nfww)
end if
Allocate(DetL(nnw,nx),DetR(nnw,nx), omm(nnw))
!Allocate(DetL1(nw,nx),DetR1(nw,nx))

!!to check parallelization on Determinants (but should be ok)
!if (myrow==0.and.mycol==0) then
!        do i = 1, nw; Momega(i,1)=omega(i); end do
!        call zgebs2d(context,'A','i-ring',nw,1,Momega,nx)
!else
!        call zgebr2d(context,'A','i-ring',nw,1,Momega,nx,0,0)
!end if
!do i = 1, nw; omega(i)=Momega(i,1); end do


if (myrow==0.and.mycol==0) then
        print*, 'Inizializzazione fatta'
        call cpu_time(tinit)
        print*, " tempo inizializzazione= ", tinit-tgrigl
        print*, " tempo assoluto inizializzazione= ", tinit-tstart
end if

!--------------------------------------------------------------------
!       Inizializzazione (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!       Interaction (Start)
!--------------------------------------------------------------------
U=dU
v=0.d0
!Interaction in the self-energies
do i=1,ns
        v(i,i)=U
end do
!--------------------------------------------------------------------
!       Interaction (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++




!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!       Folder (Start)
!--------------------------------------------------------------------

if (nomeham==nomeaperiod) then
        if (myrow==0.and.mycol==0) then
                Mainfolder='miksDetsCur_t4'//nomeham//"_nx"//int2char(450)//"_lds"//int2char(shiftlds)&
                &//"to"//int2char(nnlds-shiftlds)//'/'
                !Mainfolder='Main/'
                call system('mkdir '//trim(Mainfolder))
                folder='miksDetsCur_'//nomeham//"_nx"//int2char(nx)//"_lds"//int2char(shiftlds)&
                &//"to"//int2char(nnlds-shiftlds)//'_per'//int2char(Floor(1/tau))//"_phi"//'/'
                !folder='aAA/'
                call system('mkdir '//trim(Mainfolder)//trim(folder))
                do i=1, nx
                        folders(i)= nomeham//"_nx"//int2char(i)//"_lds"//int2char(shiftlds)&
                        &//"to"//int2char(nnlds-shiftlds)//"_phi"//'/'
                        call system('mkdir '//trim(Mainfolder)//trim(folder)//trim(folders(i)))
                end do
                !folderIsoH='Bbb/'
                folderIsoH='miks_'//nomeham//"_nx"//int2char(nx)//'_per'//int2char(Floor(1/tau))//"_phi"//'/'
                call system('mkdir '//trim(Mainfolder)//trim(folder)//trim(folderIsoH))
        end if
else
        if (myrow==0.and.mycol==0) then
                Mainfolder='miksDetsCur_t4'//nomeham//"_nx"//int2char(nx)//"_lds"//int2char(shiftlds)&
                &//"to"//int2char(nnlds-shiftlds)//'/'
                !Mainfolder='Main/'
                call system('mkdir '//trim(Mainfolder))
                folder='miksDetsCur_'//nomeham//"_nx"//int2char(nx)//"_lds"//int2char(shiftlds)&
                &//"to"//int2char(nnlds-shiftlds)//"_phi"//int2char(Floor(phi1))//'/'
                !folder='aAA/'
                call system('mkdir '//trim(Mainfolder)//trim(folder))
                do i=1, nx
                        folders(i)= nomeham//"_nx"//int2char(i)//"_lds"//int2char(shiftlds)&
                        &//"to"//int2char(nnlds-shiftlds)//"_phi"//int2char(Floor(phi1))//'/'
                        call system('mkdir '//trim(Mainfolder)//trim(folder)//trim(folders(i)))
                end do
                !folderIsoH='Bbb/'
                folderIsoH='miks_'//nomeham//"_nx"//int2char(nx)//"_phi"//int2char(Floor(phi1))//'/'
                call system('mkdir '//trim(Mainfolder)//trim(folder)//trim(folderIsoH))
        end if
end if
call   blacs_barrier(context, 'A')


!--------------------------------------------------------------------
!       Folder (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!!!stampa parametri!!!!
if (myrow.eq.0 .and. mycol.eq.0) then
        open(unit=21,file=trim(Mainfolder)//trim(folder)//'parameters.dat',status='unknown')
        write(21,'(A2,T5,I3)') "nx", nx
        write(21,'(A2,T5,F12.6)') "tau", tau
        write(21,'(A2,T5,F12.6)') "wm", wm
        write(21,'(A2,T5,I4)') "nw", nw
        write(21,'(A2,T5,I4)') "nfww", nfww
        write(21,'(A2,T5,F12.6)') "dw", 2*wm/(nw-1)
        write(21,'(A2,T5,F12.6)') "dU", U
        write(21,'(A2,T5,F12.6)') "mx", mix
        write(21,'(A2,T5,F12.6)') "bt", beta
        write(21,'(A2,T5,F12.6)') "mu", mu
        write(21,'(A2,T5,F12.6)') "gl", ggl
        write(21,'(A2,T5,F12.6)') "bl", betal
        write(21,'(A2,T5,F12.6)') "gr", ggr
        write(21,'(A2,T5,F12.6)') "br", betar
        write(21,'(A2,T5,F12.6)') "ml", mul
        write(21,'(A2,T5,F12.6)') "mr", mur
				if (nomeham==nomandre) then
					write(21,'(A2,T5,F12.6)') "Lambda", 2.d0*lambdamax
				else if (nomeham==nomebonacci) then
					write(21,'(A2,T5,F12.6)') "Lambda", 4.d0*lambdamax
				end if
        close(21)

end if




!______________________________________________________________________________________!
      !qui ciclava sui DV di potenziale (tra i gate dice, ma a me nun me pare)
do j=1, nnomham!!!--------------------------------------------------------------------!!

        if (myrow==0.and.mycol==0 .and. j==1) then
                call cpu_time(tcicl0)
        end if
        !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        !       Hamiltonian (Start)
        !--------------------------------------------------------------------

        !!!!!!!!Create Hamiltonian!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        onsita=0.d0
        onsite=0.d0
        nnhop=0.d0
        lambda= lambdamax*(j-1)/(nnomham-1)!0.5*0.5d0**(1.d0*(j-1)/(nnomham-1))  !lambdamax*(j-1)/(nnomham-1)
        phirand=0.d0
        !print*, lambda
        do i=1,ns
                !ST(i)=-(1.d0*(i-1))/(1.d0*(nx-1))*lambda*0.1d0
                St(i)=0.d0
        end do
        if (myrow==0.and.mycol==0) then
                print*, lambda
        end if


        !!!!!!!!!!!onsite potential!!!!!!!!!!!!!
        do i = 1, nx
                if (nomeham==nomhubbard) then
                        onsite(i)= lambda*(aa - dU*0.5d0) +ST(i)
                else if (nomeham==nomanderson) then
                        if (myrow==0.and.mycol==0) then
                                CALL RANDOM_NUMBER(randonn)
                                onsita(i,1)= dVg*(randonn*lambdamax - dU*0.5d0)
                        end if
                else if (nomeham==nomandre) then
                        onsite(i)= 2.0d0*lambda*SIN( 2*pi*( tau*(i-(1+nx/2))+phifrac*phi1 ) )
                else if (nomeham==nomandranT) then
                        if (myrow==0.and.mycol==0) then
                                CALL RANDOM_NUMBER(randonn)
                                phirand=phi1*randonn
                                onsite(i)= 0.5d0*SIN(2*pi*tau*( i-(1+nx/2)+ phirand ))
                        end if
                else if (nomeham==nomandranL) then
                        if (myrow==0.and.mycol==0) then
                                CALL RANDOM_NUMBER(randonn)
                                phirand=phi1*randonn
                                onsite(i)= 2.0d0*SIN(2*pi*tau*( i-(1+nx/2)+ phirand ))
                        end if
                else if (nomeham==nomebonacci) then
                        onsite(i)= 4.d0*lambda*(Floor(i/tau)-Floor((i-1)/tau))
                else
                        print*, "me devi da n'hamiltoniana che conosco, cojone"
                        stop
                end if
        end do

        do i=1, nx
                eigmean= eigmean+onsite(i)
        end do
        eigmean=1.d0*eigmean/nx
        do i=1, nx
                onsite(i)=onsite(i)-eigmean
        end do


        if (nomeham==nomanderson .or. nomeham==nomandranT .or. nomeham==nomandranL) then                                          !l'hamiltoniana la devi distribuire sulla griglia nel caso anderson
                if (myrow==0.and.mycol==0) then
                        call zgebs2d(context,'A','i-ring',nx,1,onsita,nx)
                else
                        call zgebr2d(context,'A','i-ring',nx,1,onsita,nx,0,0)
                end if
                do i = 1, nx; onsite(i)=onsita(i,1); end do
        end if

        !!!!!!!!!!hopping!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        do i = 1, nx-1
        !HAS TO BE NEGATIVE!   (but why?)
        nnhop(i)=-0.5d0
        end do



        !!!!!!!!!!crei l'hamiltiniana con diagonali onsite(i) e fuoridiaga primi vicini nhop(i)
        if (d==1) then
                call tbham(nx,bcx,onsite,nnhop,ham)
        else if (d==2) then
                call tbham2d(nx,ny,bcx,bcy,onsite,'grp',ham)
        end if

        !if (myrow.eq.0 .and. mycol.eq.0) then
        !        open(unit=21,file='H1'//trim(nomiham(j))//'.dat',status='unknown')
        !        do iw=1,nx
        !        write(21,*) ham(iw, :)
        !        end do
        !        close(21)
        !end if


        call blacs_barrier(context,'A')


        !____________________________________________________________________________________!
        !!!!!!!!Diagonalize Hamiltonian Max Dimension!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        !diagonalizza ham(la trasforma nella matx degli autovettori->s) e restituisce gli autoval in w
        call diagsy(ns,ham,w,s,0,'0')      !non ho capito bene come funziona dentro..
        call blacs_barrier(context,'A')

        !____________________________________________________________________________________!
        !!BAND-LIMITS
        if (nomeham==nomeaperiod) then
                do ipp=1,Floor(1/tau)
                        Bandlims(ipp,1)=w((ipp-1)*Floor((1.d0*ns*tau))+1)
                        Bandlims(ipp,2)=w((ipp)*Floor((1.d0*ns*tau)))
                end do
        end if
        !____________________________________________________________________________________!

        !!!!!!!!!!!!!!!!!!!!yeeee printiamo robba!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if (myrow.eq.0 .and. mycol.eq.0) then
                open(unit=21,file=trim(Mainfolder)//trim(folder)//trim(folderIsoH)//'onsite'//trim(nomiham(j))//'.dat',status='unknown')
                do iw=1,nx
                write(21,*) onsite(iw)
                end do
                close(21)
        end if                                                                  !print onsite
        if (myrow.eq.0 .and. mycol.eq.0) then
                open(unit=21,file=trim(Mainfolder)//trim(folder)//trim(folderIsoH)//'eigenval'//trim(nomiham(j))//'.dat',status='unknown')
                do iw=1,nx
                write(21,*) w(iw)
                end do
                close(21)
        end if
        if (myrow.eq.0 .and. mycol.eq.0) then
                if (j==1) then
                        open(unit=11,file=trim(Mainfolder)//trim(folder)//'lambdas.dat',status='unknown')
                end if
                write(11,*) lambda
                if (j==nnomham) then
                        close(11)
                end if
        end if
        !----------------------------------------------------------------------------!

        !--------------------------------------------------------------------
        !       Hamiltonian (End)
        !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        if(nfww.le.0) then
                do iw=1, nnw
                        omm(iw)=-wm+2*wm*(vw(iw)-1)/(nw-1)
                end do
        else
                do iw=1, size(vw,1)
                       om=-wm+2*wm*(vw(iw)-1)/(nw-1)
                        do fww=1,2*nfww
                                omm( (iw-1)*(2*nfww) + fww ) = om + 1.d0*(fww-nfww-1)*wm/((nw-1)*nfww)
                        end do
                end do
        end if

        if (myrow==0.and.mycol==0.and.j==1) then; call cpu_time(tDetin);end if

        call TdetsP(nx,2.d0*onsite,2.d0*nnhop,2.d0*omm, DetL, DetR)
        !call TdetsP(nx,onsite,nnhop,omm, DetL, DetR)


        if (myrow==0.and.mycol==0.and.j==1) then; call cpu_time(tDetfin);
                print*, " tempo Det= ", tDetfin-tDetin; end if

        !call TdetsP(nx,2.d0*onsite,2.d0*nnhop,2.d0*omega, DetL1, DetR1)

        if (myrow==0.and.mycol==0) then
                open(unit=21,file=trim(Mainfolder)//trim(folder)//'DetL0'//trim(nomiham(j))//'_ld.dat',status='unknown',access='append')
                do in=1, nx
                        write(21,*) DetL((nw+1)/2, in)
                end do
                close(21)
        end if
        !print*, "Detl", DetL

        !do iw=1, size(vw,1)
        !        jj=vw(iw)
        !        fww= (iw-1)*(2*nfww)+ nfww+1
                !print*, mycol, DetL(fww,nx-1) - DetL1(jj,nx-1), DetR(fww,nx-1) - DetR1(jj,nx-1)!, DetR1(jj,nx)
                !print*, jj, omm(iw), omega(jj)
                !print*, ""
        !end do

        do ilds=1, nnlds                               !mi tengo lo 0 per il senza leads
                ggl=gl*2.**(0.5d0*(ilds-shiftlds))
                ggr=gr*2.**(0.5d0*(ilds-shiftlds))


                !print*, ggl, ggr
                if (myrow.eq.0 .and. mycol.eq.0) then
                        if (j==1) then
                                open(unit=12,file=trim(Mainfolder)//trim(folder)//'ggl.dat',status='unknown')
                                open(unit=13,file=trim(Mainfolder)//trim(folder)//'ggr.dat',status='unknown')
                        end if
                        write(12,*) ggl
                        write(13,*) ggr
                        if (j==nnomham) then
                                close(12)
                                close(13)
                        end if
                end if


                if (myrow==0.and.mycol==0.and.ilds==1.and.j==1) then; call cpu_time(tG2rin);end if

                if(nfww.le.0) then
                        do in=4,nx
                                do iw=1, size(vw,1)
                                        Dettemp=( (in+1)/2 - in/2 )*DetR(iw,in-2) + ( in/2 - (in-1)/2 )*DetL(iw,in-2)
                                        GrDet(in, (iw-1)*nx+ in)= 4.d0/(DetL(iw,in)**2 + (ggl*ggr)**4*Dettemp**2+ ggr**4*DetL(iw,in-1)**2+ ggl**4*DetR(iw,in-1)**2 + 2*(ggl*ggr)**2)
                                        !GrDet(in, (iw-1)*nx+ in)= 1.d0/(2.d0**(2*in)*(DetL(iw,in)**2 + 0.5d0**4*(ggl*ggr)**4*Dettemp**2+ 0.5d0**2*ggr**4*DetL(iw,in-1)**2+ 0.5d0**2*ggl**4*DetR(iw,in-1)**2) + 0.5d0*(ggl*ggr)**2)
                                        !!!!! (2*in)->(2*in-2)!!
                                        !print*, GrDet(in, (iw-1)*nx+ in)
                                end do
                        end do
                else
                        do in=4,nx
                                do iw=1, size(vw,1)
                                        do fww=1,2*nfww+1
                                                jw=(iw-1)*(2*nfww) + fww
                                                Dettemp=( (in+1)/2 - in/2 )*DetR(jw,in-2) + ( in/2 - (in-1)/2 )*DetL(jw,in-2)
                                                GrDetemp(fww)= 4.d0/(DetL(jw,in)**2 + (ggl*ggr)**4*Dettemp**2+ ggr**4*DetL(jw,in-1)**2+ ggl**4*DetR(jw,in-1)**2 + 2*(ggl*ggr)**2)
                                                !GrDetemp(fww)= 1.d0/(2.d0**(2*in-2)*(DetL(jw,in)**2 + 0.5d0**4*(ggl*ggr)**4*Dettemp**2+ 0.5d0**2*ggr**4*DetL(jw,in-1)**2+ 0.5d0**2*ggl**4*DetR(jw,in-1)**2) + 0.5d0*(ggl*ggr)**2)
                                        end do

                                        GrDet(in, (iw-1)*nx+ in) = (sum(GrDetemp) - 0.5d0*(GrDetemp(1) + GrDetemp(2*nfww+1)))/(2*nfww)
                                        !print*, GrDet(in, (iw-1)*nx+ in)
                                end do
                        end do
                end if



                fTfun=0.d0
                Vl=dVl
                Vr=dVr
                do iw=1, size(vw,1)
                        liw=(iw-1)*nx+1
                        om=-wm+2*wm*(vw(iw)-1)/(nw-1)
                        dalphaL=stat*(1.0d0/(exp(betal*(om-mul-Vl))-1.0d0*stat))
                        dalphaR=stat*(1.0d0/(exp(betar*(om-mur-Vr))-1.0d0*stat))
                        fTfun(1:nx,liw:liw+nx-1) = (dalphaR-dalphaL)*(ggl*ggr)**2*GrDet(1:nx,liw:liw+nx-1)
                end do



                CurrN=0.d0
                call wint(context,ns,nw,wm,fTfun,desc,CurrN)

                curB=0.d0

                if (myrow==0.and.mycol==0) then
                do in=4, nx
                        curB(in, ilds)= Real(CurrN(in,in))
                        !print*, ilds, Real(CurrN(in,in))
                        open(unit=21,file=trim(Mainfolder)//trim(folder)//trim(folders(in))//'MKcurrentB'//trim(nomiham(j))//'_ld.dat',status='unknown',access='append')
                        write(21,*) curB(in,ilds)
                        close(21)
                end do
                end if



                !!! STAMPA le TFUN

                do in=4, nx
                        fTftemp=0.d0
                        do iw=1, lldwc
                                liw=(iw-1)*nx+1
                                fTftemp(vw(iw),1)= fTfun(in, liw +in -1 )
                        end do

                        call dgsum2d(context, 'R', 'i-ring', nw, 1, fTftemp, nw, 0, 0)

                        if (myrow==0.and.mycol==0) then
                                open(unit=21,file=trim(Mainfolder)//trim(folder)//trim(folders(in))//'Itfun'//"_"//trim(nomiham(j))//'_ld'//int2char(ilds)//'.dat',status='unknown',access='append')
                                do iw=1, nw
                                write(21,*) fTftemp(iw,1)
                                end do
                                close(21)
                        end if
                end do

                ! if (j==1) then
                !         do iw=1, lldwc
                !                 liw=(iw-1)*nx+1
                !                 print*, vw(iw),  fTfun(18, liw +18 -1 )
                !         end do
                ! end if

                !----------------------------------------------------------------------------!
                !!!! STAMPA correnti Bande START-----------------
                !----------------------------------------------------------------------------!
                ! if (myrow==0.and.mycol==0) then
                ! do ipp=1, Floor(1/tau)
                !         print*, "Band lim", Bandlims(ipp,1), Bandlims(ipp,2)
                ! end do
                ! end if

                if (nomeham==nomeaperiod) then
                do ipp=1, Floor(1/tau)
                        fTfun=0.d0
                        CurrN=0.d0
                        curB=0.d0

                        do iw=1, size(vw,1)
                                liw=(iw-1)*nx+1
                                om=-wm+2*wm*(vw(iw)-1)/(nw-1)
                                dalphaL=stat*(1.0d0/(exp(betal*(om-Bandlims(ipp,2)-Vl))-1.0d0*stat))
                                dalphaR=stat*(1.0d0/(exp(betar*(om-Bandlims(ipp,1)-Vr))-1.0d0*stat))
                                fTfun(1:nx,liw:liw+nx-1) = (dalphaR-dalphaL)*(ggl*ggr)**2*GrDet(1:nx,liw:liw+nx-1)
                        end do

                        call wint(context,ns,nw,wm,fTfun,desc,CurrN)

                        if (myrow==0.and.mycol==0) then
                        do in=4, nx
                                curB(in, ilds)= Real(CurrN(in,in))
                                !print*, ilds, Real(CurrN(in,in))
                                open(unit=21,file=trim(Mainfolder)//trim(folder)//trim(folders(in))//'Tcurrent'//int2char(ipp)//"_"//trim(nomiham(j))//'_ld.dat',status='unknown',access='append')
                                write(21,*) curB(in,ilds)
                                close(21)
                        end do
                        end if
                end do
                end if
                !----------------------------------------------------------------------------!
                !!!! STAMPA correnti Bande END-----------------
                !----------------------------------------------------------------------------!







        !        if (myrow==0.and.mycol==0.and.ilds==1.and.j==1) then; call cpu_time(tTcurrin);end if
!
!                Tcurr=0.d0
!                do in=4, ns
!                do iw=1, size(vw,1)
!                        Tcurr(in, vw(iw))= Real(fTfun(in, (iw-1)*nx + in))
!                end do
!                end do
!
!                call zgsum2d(context, 'A', 'i-ring', ns, nw, Tcurr, ns, 0, 0 )
!
!                if (myrow==0.and.mycol==0) then
!                do in=4, nx
!                        open(unit=21,file=trim(folder)//trim(folders(in))//'Tcurrents'//trim(nomiham(j))//'_ld'//int2char(ilds)//'.dat',status='unknown',access='append')
!                        do iw=1,nw
!                                write(21,*) Tcurr(in,nw)
!                        end do
!                        close(21)
!                end do
!                end if

!                if (myrow==0.and.mycol==0.and.ilds==1.and.j==1) then; call cpu_time(tTcurrfin);
!                        print*, " tempo Tcurr= ", tTcurrfin-tTcurrin; end if
                !
                ! fTfun=0.d0
                ! CurrN=0.d0
                ! curB=0.d0
                !
                ! do iw=1, size(vw,1)
                !         liw=(iw-1)*nx+1
                !         om=-wm+2*wm*(vw(iw)-1)/(nw-1)
                !         dalphaL=stat*(1.0d0/(exp(betal*(om-2.d0-Vl))-1.0d0*stat))
                !         dalphaR=stat*(1.0d0/(exp(betar*(om-1.d0-Vr))-1.0d0*stat))
                !         fTfun(1:nx,liw:liw+nx-1) = (dalphaR-dalphaL)*(ggl*ggr)**2*GrDet(1:nx,liw:liw+nx-1)
                ! end do
                !
                ! call wint(context,ns,nw,wm,fTfun,desc,CurrN)
                !
                ! if (myrow==0.and.mycol==0) then
                ! do in=4, nx
                !         curB(in, ilds)= Real(CurrN(in,in))
                !         !print*, ilds, Real(CurrN(in,in))
                !         open(unit=21,file=trim(Mainfolder)//trim(folder)//trim(folders(in))//'Tcurrent1'//trim(nomiham(j))//'_ld.dat',status='unknown',access='append')
                !         write(21,*) curB(in,ilds)
                !         close(21)
                ! end do
                ! end if
                !
                ! fTfun=0.d0
                ! CurrN=0.d0
                ! curB=0.d0
                !
                ! do iw=1, size(vw,1)
                !         liw=(iw-1)*nx+1
                !         om=-wm+2*wm*(vw(iw)-1)/(nw-1)
                !         dalphaL=stat*(1.0d0/(exp(betal*(om-0.5d0-Vl))-1.0d0*stat))
                !         dalphaR=stat*(1.0d0/(exp(betar*(om+0.5d0-Vr))-1.0d0*stat))
                !         fTfun(1:nx,liw:liw+nx-1) = (dalphaR-dalphaL)*(ggl*ggr)**2*GrDet(1:nx,liw:liw+nx-1)
                ! end do
                !
                ! call wint(context,ns,nw,wm,fTfun,desc,CurrN)
                !
                ! if (myrow==0.and.mycol==0) then
                ! do in=4, nx
                !         curB(in, ilds)= Real(CurrN(in,in))
                !         !print*, ilds, Real(CurrN(in,in))
                !         open(unit=21,file=trim(Mainfolder)//trim(folder)//trim(folders(in))//'Tcurrent2'//trim(nomiham(j))//'_ld.dat',status='unknown',access='append')
                !         write(21,*) curB(in,ilds)
                !         close(21)
                ! end do
                ! end if
                !
                !
                ! fTfun=0.d0
                ! CurrN=0.d0
                ! curB=0.d0
                !
                ! do iw=1, size(vw,1)
                !         liw=(iw-1)*nx+1
                !         om=-wm+2*wm*(vw(iw)-1)/(nw-1)
                !         dalphaL=stat*(1.0d0/(exp(betal*(om+1.d0-Vl))-1.0d0*stat))
                !         dalphaR=stat*(1.0d0/(exp(betar*(om+2.d0-Vr))-1.0d0*stat))
                !         fTfun(1:nx,liw:liw+nx-1) = (dalphaR-dalphaL)*(ggl*ggr)**2*GrDet(1:nx,liw:liw+nx-1)
                ! end do
                !
                ! call wint(context,ns,nw,wm,fTfun,desc,CurrN)
                !
                ! if (myrow==0.and.mycol==0) then
                ! do in=4, nx
                !         curB(in, ilds)= Real(CurrN(in,in))
                !         !print*, ilds, Real(CurrN(in,in))
                !         open(unit=21,file=trim(Mainfolder)//trim(folder)//trim(folders(in))//'Tcurrent3'//trim(nomiham(j))//'_ld.dat',status='unknown',access='append')
                !         write(21,*) curB(in,ilds)
                !         close(21)
                ! end do
                ! end if

        end do                          !sui leads (ilds)

        !do in=4, nx
        !        if (myrow==0.and.mycol==0) then
        !                open(unit=21,file=trim(folder)//trim(folders(in))//'MKcurrentB'//trim(nomiham(j))//'_ld.dat',status='unknown',access='append')
        !                write(21,*) curB(in,:)
        !                close(21)
        !        end if
        !end do

        !!!!!!!!!!!!!!!!!!!!!!!vabbè qui è brutto, poi l'aggiusto!!!!!!!!!!!!!!!!!!!!!!!


end do !(sul potenziale onsite)-------------------------------------------!
!__________________________________________________________________________________!
!(qui finiva do sul coupling coi leads)
if (myrow==0.and.mycol==0) then
        call cpu_time(tcicl1)
        print*, " tempo tot ciclo= ", tcicl1-tcicl0
        print*, " tempo assoluto Hamiltoniane= ", tcicl1-tstart
end if



if (myrow==0.and.mycol==0) then
       call system('mv *dat '//trim(Mainfolder)//trim(folder))
end if
!Exit grid
call blacs_gridexit(context)
call blacs_exit(0)


if (myrow==0.and.mycol==0) then
        call cpu_time(texit)
        print*, " tempo uscita= ", texit-tcicl1
        print*, " tempo assoluto uscita= ", texit-tstart
end if


end program wifhm
