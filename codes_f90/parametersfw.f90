! ============================================================================
! Name        : parameters.f90
! Author      : N. Lo Gullo
! Version     : 1.0
! Copyright   : GPL
! Description : Set all numerical parameters for the wifhm
! Date        : 23/01/2017
! ============================================================================
module parametersfw
implicit none
!Integration scheme: t = trapezoidal, s=simpson, r (or anything else)= rectangules
	 character (*), parameter :: gmt='grp';
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double precision, parameter :: tau = (sqrt(5.0d0)+1)/2		! Golden ratio
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero

!Parameters for space dimensionality
integer, parameter :: d=1;
integer, parameter :: nx=80;
integer, parameter :: ny=1;
	integer, parameter :: bcx = 0					! Boundary conditions (open bc=0, periodic bc=1)
	integer, parameter :: bcy = 1					! Boundary conditions (open bc=0, periodic bc=1)

!Lattice parameters
	double precision, parameter :: tx = 1.0d0			! Tunneling rate along x
	double precision, parameter :: ty = 1.0d0			! Tunneling rate along y
	double precision, parameter :: A1=0.d0;
	double precision, parameter :: k1 = 2.0d0*pi*tau		! Wave number of the lattice
	double precision, parameter :: phi1 = 0.0d0*2.0d0*pi		! Phase shift
	double precision, parameter :: dVg = 3.d0/35
	double precision, parameter :: lambdamax=1.d0
integer, parameter :: ai=0;
integer, parameter :: af=0;

!Parameters to fix mean energy and number of particles of t1 system
	double precision, parameter :: beta =1.0d2			! Inverse temperature
        double precision, parameter :: npart = 5.0d-1*nx*ny					! Number of particles

!Parameters for w grid    nw .... !8192!16384!25600!65536!802816!810000!1048576!36864!409600!200000!102!6553600
        integer, parameter :: nw =10**2+1 !1024!16384	! Number of time steps for a single temporal slice
				double precision, parameter :: wm=4.0d0				! Initial time
        double precision, parameter :: dct=1.0d4
				double precision, parameter :: eta0=4.d-5


character(*), parameter :: exc="ni";
	integer, parameter :: maxrun = 100;
	integer, parameter :: minrun = 15;
	integer, parameter :: mem = 2;
       double precision, parameter :: mix=2.d-1!3.5d-1;
double precision, parameter :: dU=0.2d0;

!Parameters for the leads
	integer, parameter :: sitelds = ny
	double precision, parameter :: gl=1.d0!5.d-1*sqrt(5.d-1)			! Coupling to left lead
        double precision, parameter :: betal = 1.0d2!/8.0d0			! Inverse temperature of left lead
	!double precision, parameter :: mul= 0.5d0;			! Chemical potential of left lead
	double precision, parameter :: dmul = 2.d-3!4.d0!2.d-3			! sift curr
	double precision, parameter :: dVl = 0.0d0!4.d-6			! Peak of DOS of left lead
	double precision, parameter :: npartl =nx*0.5d0			! Number of particles in the left lead
!	double precision, parameter :: gml = 0.2d0			! Width of DOS of left lead

	double precision, parameter :: gr=gl!1.d0!*sqrt(5.d-1)			! Coupling to right lead
	double precision, parameter :: betar=1.0d2!/7.0d0				! Inverse temperature of right lead
!	double precision, parameter :: mur=mul;				! Chemical potential of right lead
double precision, parameter :: dmur = -2.0d-3!-dmul!-2.d-3			! sift curr
	double precision, parameter :: dVr = -dVl			! Peak of DOS of right lead
	double precision, parameter :: npartr =0.0d0			! Number of particles in the right lead
!	double precision, parameter :: gmr = 0.2d0			! Width of DOS of right lead
integer, parameter :: vvldsi=0;
integer, parameter :: vvldsf=0;

!Processes grid parameters
	integer, parameter :: nnprow=128!32!50!128				! Number of grid's rows
	integer, parameter :: nnpcol=20!8!50!20					! Number of grid's column
	integer, parameter :: mb=nx*ny					! Rows distribution factor
	integer, parameter :: nb=nx*ny					! Columns distribution factor
        integer, parameter :: mbs=1                                     ! Rows distribution factor
        integer, parameter :: nbs=1                                     ! Columns distribution factor

!Parameters for saving files
	integer, parameter :: nfls=80					! Number of printed files
!	integer, parameter :: ntsv=int(nt/nfls)				! Time steps at which it saves files
end module parametersfw
