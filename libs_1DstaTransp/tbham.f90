

!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!  Hamiltonian for tight-binding  (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine tbham(npx,bc,onsite,nnhop,ham)
	use mpi
	implicit none
!Input variables
	integer :: npx, bc
	double precision, dimension(:) :: onsite
	double precision, dimension(:) :: nnhop
!Input/output variables
	double precision, dimension(:,:) :: ham
!Dummy variables
	integer :: i

ham=0.0d0

!Diagonal elements
do i=1, npx
	ham(i,i) = onsite(i)
end do

!Nearest Neighbour
do i=1, npx-1
	ham(i,i+1) = nnhop(i)
	ham(i+1,i) = nnhop(i)
end do


!Periodic boundary conditions

if(bc==1) then
	ham(npx,1) = nnhop(1)
	ham(1,npx) = nnhop(1)
end if

end subroutine tbham

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!  Hamiltonian for tight-binding (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

