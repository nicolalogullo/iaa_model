!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		PHONON self-energy (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine phse(context,nps,npw,wm,vw,v,lnegl,lnegg,desc,lsl,lsg,lsr,descld)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw
	double precision :: wm,g
	integer, dimension (:) :: vw,desc,descld
	double precision, dimension (:,:) :: v
	double complex, dimension(:,:) :: lnegl
	double complex, dimension(:,:) :: lnegg
!Input/Output variables
	double complex, dimension(:,:) :: lsl
	double complex, dimension(:,:) :: lsg
	double complex, dimension(:,:) :: lsr

!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: n,i,j,lw,liw,mb,nb,lldc,lldr,lldwc,lldwr, numroc
	double precision :: t,dt,tm
	double complex :: zalpha
	double complex, dimension(nps,nps) :: intg, tmp, tmpa, cv
	double complex, dimension(:,:), allocatable :: ltmpr, ltmpl, ltmpg, ltmp,lpa, lpr, lpl, lpg, lwr, lwl, lwg 

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)

lldwr=numroc(1,1,myrow,0,1)
lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(ltmpr(lldr,lldc),ltmpl(lldr,lldc),ltmpg(lldr,lldc),ltmp(lldwr,lldwc))
allocate(lpr(lldwr,lldwc),lpa(lldwr,lldwc),lpl(lldwr,lldwc),lpg(lldwr,lldwc))
allocate(lwr(lldr,lldc),lwl(lldr,lldc),lwg(lldr,lldc))


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!POLARIZATION DIAGRAM (time) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

call fourierwt(context,nps,npw,wm,1,lnegl,desc,ltmpl)
call fourierwt(context,nps,npw,wm,1,lnegg,desc,ltmpg)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!LESSER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!do lw=1, lldwc
!	liw=(lw-1)*nps+1
!	do i=1,nps
!	do j=1,nps
!		lpl(1,lw)= ci*ltmpl(i,liw+j-1)!*conjg(ltmpg(i,liw+j-1))*(g**2)
!	end do
!	end do
!end do
lpl=zero
tmp=zero
do lw=1, lldwc
	liw=(lw-1)*nps+1
	tmp=matmul(matmul(v,ltmpl(1:nps,liw:liw+nps-1)), matmul(v,conjg(transpose(ltmpg(1:nps,liw:liw+nps-1)))) )
	do i=1,nps
		lpl(1,lw)= lpl(1,lw)+ci*tmp(i,i)
	end do
end do

!if(myrow==0.and.mycol==0) then
!	print*, tmp(1,1)  
!end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!GREATER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!do lw=1, lldwc
!	liw=(lw-1)*nps+1
!	do i=1,nps
!	do j=1,nps
!		lpg(1,lw)= ci*ltmpg(i,liw+j-1)!*conjg(ltmpl(i,liw+j-1))*(g**2)
!	end do
!	end do
!end do
lpg=zero
tmp=zero
do lw=1, lldwc
	liw=(lw-1)*nps+1
	tmp=matmul(matmul(v,ltmpg(1:nps,liw:liw+nps-1)), matmul(v,conjg(transpose(ltmpl(1:nps,liw:liw+nps-1)))) )
	do i=1,nps
		lpg(1,lw)= lpg(1,lw)+ci*tmp(i,i)
	end do
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!RETARDED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ltmp=zero
tm=pi*(npw-1)/(2.d0*wm)
dt=pi/wm

!do lw=1, lldwc
!	liw=(lw-1)*nps+1
!	t=-tm+(vw(lw)-1)*dt

!	if (t.ge.0.d0.and.t<dt) then
!		ltmp(:,liw:liw+nps-1)=0.5d0*(lpg(:,liw:liw+nps-1)-lpl(:,liw:liw+nps-1))
!	else if (t>0.d0) then
!		ltmp(:,liw:liw+nps-1)=(lpg(:,liw:liw+nps-1)-lpl(:,liw:liw+nps-1))
!	end if
!end do

do lw=1, lldwc
	t=-tm+(vw(lw)-1)*dt
	if (t.ge.0.d0.and.t<dt) then
		ltmp(1,lw)=0.5d0*(lpg(1,lw)-lpl(1,lw))
	else if (t>0.d0) then
		ltmp(1,lw)=(lpg(1,lw)-lpl(1,lw))
	end if
end do

lsr=zero
call fourierwt(context,1,npw,wm,-1,ltmp,descld,lsr)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ADVANCED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!ltmp=zero
!tm=pi*(npw-1)/(2.d0*wm)
!dt=pi/wm

!do lw=1, lldwc
!	liw=(lw-1)*nps+1
!	t=-tm+(vw(lw)-1)*dt

!	if (t.ge.0.d0.and.t<dt) then
!		ltmp(:,liw:liw+nps-1)=-0.5d0*(lpg(:,liw:liw+nps-1)-lpl(:,liw:liw+nps-1))
!	else if (t<0.d0) then
!		ltmp(:,liw:liw+nps-1)=-(lpg(:,liw:liw+nps-1)-lpl(:,liw:liw+nps-1))
!	end if
!end do


!lsa=zero
!call fourierwt(context,nps,npw,wm,-1,ltmp,desc,lsa)

ltmp=lpl
lsl=zero
call fourierwt(context,1,npw,wm,-1,ltmp,descld,lsl)

ltmp=lpg
lsg=zero
call fourierwt(context,1,npw,wm,-1,ltmp,descld,lsg)

end subroutine phse
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		PHONON self-energy (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

