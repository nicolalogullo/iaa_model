
!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	        Hamiltonian (Start)
!Info: Creates the tight-binding free Hamiltonian
!			2D systems
!The idea is to map the 2D lattice into a 1D one with a peculiar topology
!the point (ix,iy) of the 2D lattice is mappend into the site i=ix+nx*(iy-1)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine tbham2d(npx,npy,bcx,bcy,onsite,gmt,ham)
!Input variables
	character (*) :: gmt
	integer :: npx,npy,bcx,bcy
	double precision, dimension(:) :: onsite
!	double precision, dimension(:) :: nnhop
!Input/output variables
	double precision, dimension(:,:) :: ham
!Dummy variables
	integer :: ix,iy,i,j

ham=0.0d0

if (gmt=='sq'.or.gmt=='ex') then
!Diagonal elements
do ix=1, npx
do iy=1, npy
	i=ix+npx*(iy-1)
	ham(i,i) = onsite(i)
end do
end do

!Nearest Neighbour along x (ix,iy) <-> (ix+1,iy)
do ix=1, npx-1
do iy=1,npy
	i=ix+npx*(iy-1)
	ham(i,i+1) = -0.5d0!nnhop(i)
	ham(i+1,i) = -0.5d0!nnhop(i)
end do
end do

!Nearest Neighbour along y (ix,iy) <-> (ix,iy+1)
do ix=1, npx
do iy=1,npy-1
	i=ix+npx*(iy-1)
	ham(i,i+npx) = -0.5d0!nnhop(i)
	ham(i+npx,i) = -0.5d0!nnhop(i)
end do
end do

!Periodic boundary conditions along x (npx,iy) <-> (1,iy)
if(bcx==1) then
do iy=1, npy
	i=1+npx*(iy-1)
	j=npx+npx*(iy-1)
	ham(j,i) = -0.5d0!nnhop(1)
	ham(i,j) = -0.5d0!nnhop(1)
end do
end if


!Periodic boundary conditions along y (ix,npy) <-> (ix,1)
if(bcy==1) then
do ix=1, npx
	i=ix
	j=ix+npx*(npy-1)
	ham(j,i) = -0.5d0!nnhop(1)
	ham(i,j) = -0.5d0!nnhop(1)
end do
end if


end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TRIANGULAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!To add the exagonal structure to the square one (ix,iy) <-> (ix-1,iy+1) 
!Basically this adds the triangles (\) to a "square lattice" (- , |)
!Vista QUADRATO (aiuta per gli indici)
!iy=6	o - o - o - o - o - o
!	| \ | \ | \ | \ | \ |
!iy=5	o - o - o - o - o - o
!	| \ | \ | \ | \ | \ |
!iy=4	o - o - o - o - o - o
!	| \ | \ | \ | \ | \ |
!iy=3	o - o - o - o - o - o
!	| \ | \ | \ | \ | \ |
!iy=2	o - o - o - o - o - o
!	| \ | \ | \ | \ | \ |
!iy=1	o - o - o - o - o - o
!
!ix=    1   2   3   4   5   6
!
!Vista ESAGONI (shifti le rige alternate)
!iy=6	  o - o - o - o - o - o
!	 / \ / \ / \ / \ / \ /
!iy=5	o - o - o - o - o - o
!        \ / \ / \ / \ / \ /
!iy=4	  o - o - o - o - o - o
!	 / \ / \ / \ / \ / \ /
!iy=3	o - o - o - o - o - o
!        \ / \ / \ / \ / \ /
!iy=2	  o - o - o - o - o - o
!	 / \ / \ / \ / \ / \ /
!iy=1	o - o - o - o - o - o
!
!ix=    1   2   3   4   5   6

if (gmt=='ex') then
do ix=2, npx
do iy=1,npy-1
	i=ix+npx*(iy-1)
	j=ix-1+npx*(iy-1+1)
	ham(i,j) = -0.5d0!nnhop(i)
	ham(j,i) = -0.5d0!nnhop(i)
end do
end do

!Periodic boundary conditions along x (1,iy) <-> (npx,iy+1)
!Vista QUADRATO
!iy=6	- o - o - o - o - o - o -
!	\ | \ | \ | \ | \ | \ | \
!iy=5	- o - o - o - o - o - o -
!	\ | \ | \ | \ | \ | \ | \
!iy=4	- o - o - o - o - o - o -
!	\ | \ | \ | \ | \ | \ | \
!iy=3	- o - o - o - o - o - o -
!	\ | \ | \ | \ | \ | \ | \
!iy=2	- o - o - o - o - o - o -
!	\ | \ | \ | \ | \ | \ | \
!iy=1	- o - o - o - o - o - o -
!
!ix=      4   5   6   1   2   3
if(bcx==1) then
do iy=1, npy-1
	i=1+npx*(iy-1)
	j=npx+npx*(iy-1+1)
	ham(j,i) = -0.5d0!nnhop(1)
	ham(i,j) = -0.5d0!nnhop(1)
end do
end if

!Periodic boundary conditions along y (ix,npy) <-> (ix-1,1)
!Vista QUADRATO
!	| \ | \ | \ | \ | \ |
!iy=3	o - o - o - o - o - o
!	| \ | \ | \ | \ | \ |
!iy=2	o - o - o - o - o - o
!	| \ | \ | \ | \ | \ |
!iy=1	o - o - o - o - o - o
!	| \ | \ | \ | \ | \ |
!iy=6	o - o - o - o - o - o
!	| \ | \ | \ | \ | \ |
!iy=5	o - o - o - o - o - o
!	| \ | \ | \ | \ | \ |
!iy=4	o - o - o - o - o - o
!	| \ | \ | \ | \ | \ |
!
!ix=    1   2   3   4   5   6

if(bcy==1) then
do ix=2, npx
	i=ix+npx*(npy-1)
	j=ix-1
	ham(j,i) = -0.5d0!nnhop(1)
	ham(i,j) = -0.5d0!nnhop(1)
end do
end if

end if



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!GRAPHENE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!The graphene is slightly more complicated,
!but it's structure can be recovered by assuming a stack of two different 
!chains with n_x and Ceiling(n_x/2) sites.
!In the example below 8 and 5 sites are shown
!iy=8	o     o     o     o     o
!	 \   / \   / \   / \   / 
!iy=7	  o-o   o-o   o-o   o-o
!        /   \ /   \ /   \ /   \ 
!iy=6	o     o     o     o     o
!	 \   / \   / \   / \   / 
!iy=5	  o-o   o-o   o-o   o-o
!        /   \ /   \ /   \ /   \ 
!iy=4	o     o     o     o     o
!	 \   / \   / \   / \   / 
!iy=3	  o-o   o-o   o-o   o-o
!        /   \ /   \ /   \ /   \ 
!iy=2	o     o     o     o     o
!	 \   / \   / \   / \   / 
!iy=1	  o-o   o-o   o-o   o-o
!
!ix=      1 2   3 4   5 6   7 8

if (gmt=='grp') then
!Diagonal elements
do ix=1, npx
do iy=1, npy
	i=ix+npx*(Ceiling(iy/2.d0)-1)+Ceiling(npx/2.d0)*(Floor(iy/2.d0)-1)
	ham(i,i) = onsite(i)
end do
end do

!Nearest Neighbour along x (ix,iy) <-> (ix+1,iy) if ix=2*m-1 and iy=2*l-1
!iy=3	  o-o   o-o   o-o   o-o
!
!iy=2	o     o     o     o     o
!
!iy=1	  o-o   o-o   o-o   o-o
!
!ix=      1 2   3 4   5 6   7 8
do iy=1,npy,2
do ix=1, npx-1
!	i=local coordinate along x + number of sites on the odd iy * position of the odd iy +
!	number of sites along the even iy * position of the even iy
	i=ix+npx*(Ceiling(iy/2.d0)-1)+Ceiling(npx/2.d0)*(Floor(iy/2.d0)-1)
	ham(i,i+1) = -0.5d0*mod(ix,2)!nnhop(i)
	ham(i+1,i) = -0.5d0*mod(ix,2)!nnhop(i)
end do
end do

!Nearest Neighbour along y
!iy=3	  o-o   o-o   o-o   o-o
!
!iy=2	o     o     o     o     o
!	 \     \     \     \     
!iy=1	  o-o   o-o   o-o   o-o
!
!ix=      1 2   3 4   5 6   7 8

do iy=2,npy,2
do ix=1, Ceiling(npx/2.d0)-1
	i=ix+npx*(Ceiling(iy/2.d0)-1)+Ceiling(npx/2.d0)*(Floor(iy/2.d0)-1)
	j=2*ix-1+npx*(Ceiling((iy-1)/2.d0)-1)+Ceiling(npx/2.d0)*(Floor((iy-1)/2.d0)-1)
	ham(i,j) = -0.5d0!*mod(ix,2)*mod(iy,2)!nnhop(i)
	ham(j,i) = -0.5d0!*mod(ix,2)*mod(iy,2)!nnhop(i)
end do
end do


!iy=3	  o-o   o-o   o-o   o-o
!
!iy=2	o     o     o     o     o
!	     /     /     /     / 
!iy=1	  o-o   o-o   o-o   o-o
!
!ix=      1 2   3 4   5 6   7 8


do iy=2,npy,2
do ix=2, Ceiling(npx/2.d0)
	i=ix+npx*(Ceiling(iy/2.d0)-1)+Ceiling(npx/2.d0)*(Floor(iy/2.d0)-1)
	j=2*ix+npx*(Ceiling((iy-1)/2.d0)-1)+Ceiling(npx/2.d0)*(Floor((iy-1)/2.d0)-1)
	ham(i,j) = -0.5d0!*mod(ix,2)*mod(iy,2)!nnhop(i)
	ham(j,i) = -0.5d0!*mod(ix,2)*mod(iy,2)!nnhop(i)
end do
end do


!iy=3	  o-o   o-o   o-o   o-o
!        /     /     /     /     
!iy=2	o     o     o     o     o
!
!iy=1	  o-o   o-o   o-o   o-o
!
!ix=      1 2   3 4   5 6   7 8

do iy=2,npy-2,2
do ix=1, Ceiling(npx/2.d0)-1
	i=ix+npx*(Ceiling(iy/2.d0)-1)+Ceiling(npx/2.d0)*(Floor(iy/2.d0)-1)
	j=2*ix-1+npx*(Ceiling((iy+1)/2.d0)-1)+Ceiling(npx/2.d0)*(Floor((iy+1)/2.d0)-1)
	ham(i,j) = -0.5d0!*mod(ix,2)*mod(iy,2)!nnhop(i)
	ham(j,i) = -0.5d0!*mod(ix,2)*mod(iy,2)!nnhop(i)
end do
end do



!iy=3	  o-o   o-o   o-o   o-o
!            \     \     \     \ 
!iy=2	o     o     o     o     o
!
!iy=1	  o-o   o-o   o-o   o-o
!
!ix=      1 2   3 4   5 6   7 8

do iy=2,npy-2,2
do ix=2, Ceiling(npx/2.d0)
	i=ix+npx*(Ceiling(iy/2.d0)-1)+Ceiling(npx/2.d0)*(Floor(iy/2.d0)-1)
	j=2*ix+npx*(Ceiling((iy+1)/2.d0)-1)+Ceiling(npx/2.d0)*(Floor((iy+1)/2.d0)-1)
	ham(i,j) = -0.5d0!*mod(ix,2)*mod(iy,2)!nnhop(i)
	ham(j,i) = -0.5d0!*mod(ix,2)*mod(iy,2)!nnhop(i)
end do
end do


if (bcy==1) then 
!Boundary conditions along y
!iy=1	  o-o   o-o   o-o   o-o
!        /     /     /     /     
!iy=8	o     o     o     o     o
!
!ix=      1 2   3 4   5 6   7 8


do ix=1, Ceiling(npx/2.d0)-1
	i=ix+npx*(Ceiling(npy/2.d0)-1)+Ceiling(npx/2.d0)*(Floor(npy/2.d0)-1)
	j=2*ix-1
	ham(i,j) = -0.5d0!*mod(ix,2)*mod(iy,2)!nnhop(i)
	ham(j,i) = -0.5d0!*mod(ix,2)*mod(iy,2)!nnhop(i)
end do


!iy=1	  o-o   o-o   o-o   o-o
!            \     \     \     \ 
!iy=8	o     o     o     o     o
!
!ix=      1 2   3 4   5 6   7 8


do ix=2, Ceiling(npx/2.d0)
	i=ix+npx*(Ceiling(npy/2.d0)-1)+Ceiling(npx/2.d0)*(Floor(npy/2.d0)-1)
	j=2*ix
	ham(i,j) = -0.5d0!*mod(ix,2)*mod(iy,2)!nnhop(i)
	ham(j,i) = -0.5d0!*mod(ix,2)*mod(iy,2)!nnhop(i)
end do
end if


end if



end subroutine tbham2d

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	        Hamiltonian (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
