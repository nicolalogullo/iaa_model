
!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Dyson (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wdysonph(context,nps,npw,wm,vw,lg0l,lg0g,lg0r,lsl,lsg,lsr,lsa,lnegl,lnegg,lnegr)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npt
	integer, dimension (:) :: vw
	double precision :: wm
	double complex, dimension(:,:) :: lg0r,lg0l,lg0g
!Input/Output variables
	double complex, dimension(:,:) :: lnegr,lnegl,lnegg
	double complex, dimension(:,:), allocatable :: lrr,lra
	double complex, dimension(:,:) :: lsr,lsa,lsl,lsg
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: n,npw,iw,lw,liw,lldwc,numroc
	double precision :: dalpha,om
	double complex :: zalpha
	double complex, dimension(:,:), allocatable :: lg0a, lnega

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

allocate(lrr(nps,nps), lra(nps,nps),lg0a(nps,nps),lnega(nps,nps))

lrr=zero
lra=zero
lg0a=zero
lnega=zero

lldwc=numroc(npw,1,mycol,0,nprocs)



do lw=1, lldwc
	liw=(lw-1)*nps+1
	lg0a(1:nps,1:nps) = conjg(transpose(lg0r(1:nps,liw:liw+nps-1)))

	call wsolver(nps,lg0r(:,liw:liw+nps-1),lsr(:,liw:liw+nps-1),lrr)
	lnegr(1:nps,liw:liw+nps-1) = matmul(lrr(1:nps,1:nps),lg0r(1:nps,liw:liw+nps-1))
	
	call wsolver(nps,lg0a,lsa(:,liw:liw+nps-1),lra)	
	lnega = matmul(lra,lg0a)


	lnegl(1:nps,liw:liw+nps-1) = 1.0d0*matmul(lrr, matmul(lg0l(1:nps,liw:liw+nps-1), lra )) +&
				&matmul(lnegr(1:nps,liw:liw+nps-1), matmul(lsl(1:nps,liw:liw+nps-1),lnega))


	lnegg(1:nps,liw:liw+nps-1) = 1.0d0*matmul(lrr, matmul(lg0g(1:nps,liw:liw+nps-1), lra )) +&
				&matmul(lnegr(1:nps,liw:liw+nps-1), matmul(lsg(1:nps,liw:liw+nps-1),lnega))

end do

call blacs_barrier(context,'A')

end subroutine wdysonph

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Dyson (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv


