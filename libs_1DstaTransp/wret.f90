!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Free Retarded Green's (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wret(context,nps,npw,wm,vw,w,lg0r,eta,desc)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: zero = (0.0d0,0.0d0)			! Complex zero
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco

!Input variables
	integer :: nps,npw,lld
	double precision :: wm
	double precision, dimension(:) :: w
	integer, dimension(:) :: vw
!Input/Output variables
	integer, dimension (:) :: desc
	double complex, dimension(:,:) :: lg0r
	double precision :: eta
!Variable for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: i,j,ww,liw,nsteps
	double precision :: dw,teta,deta,norm,tnorm,om
	double precision :: dalpha, start ,finish
	double complex :: zalpha,zbeta
	double complex, dimension(nps,nps) :: intg0r

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

lg0r=zero
!eta=2.d1
!deta=51.d-1
!norm=0.d0
!tnorm=0.d0
!nsteps = 1


!do while ((Abs(norm-nps)>=1.0E-14.and.nsteps<1000))
!!print*, norm, eta, deta
!!Retarded Green in the eigenbasis
!	do ww = 1, size(vw)
!		om=-wm+2*wm*(vw(ww)-1)/(npw-1)
!		liw = (ww-1)*nps+1
!		do i=1, nps
!			lg0r(i,liw+i-1) = 1.d0/(om-w(i)+ci*eta)
!		end do
!	end do

!!if (myrow.eq.0 .and. mycol.eq.0) then
!!call cpu_time(start)
!!end if




!!Calcolo integrale A(k,w)

!	call wint(context,nps,npw,wm,lg0r,desc,intg0r)




!!if (myrow.eq.0 .and. mycol.eq.0) then
!!!call cpu_time(finish)
!!!	print*, "time_int=  ",finish-start
!!	print*, norm, eta, deta
!!end if
!	tnorm=norm
!	norm=0.0d0
!	do i=1,nps
!		norm=norm-2.d0*Aimag(intg0r(i,i))
!	end do
!	if (norm<nps.and.norm>tnorm) then
!		eta=eta-deta
!	else
!		eta=eta+deta
!		deta=deta*0.5d0
!	end if

!	nsteps=nsteps+1

!end do

!eta = 10.d0*(2.d0*wm)/(pi*(npw-1))
!eta=1.d-20

!Retarded Green in the eigenbasis
do ww = 1, size(vw)
	om=-wm+2*wm*(vw(ww)-1)/(npw-1)
	liw = (ww-1)*nps+1
	do i=1, nps
		lg0r(i,liw+i-1) = 1.d0/(om-w(i)+ci*eta)
	end do
end do

!	call wint(context,nps,npw,wm,lg0r,desc,intg0r)
!	norm=0.0d0
!	do i=1,nps
!		norm=norm-2.d0*Aimag(intg0r(i,i))
!	end do
!lg0r=lg0r*nps/norm

!print*,norm,eta,nsteps

end subroutine wret
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Free Retarded Green's (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
