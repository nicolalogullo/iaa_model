!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	Integrate a matrix over w (End)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wint(context,nps,npw,wm,la,desca,outrow,outcol,inta)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
!Input variables
	integer :: nps,npw, outrow, outcol
	double precision :: wm
	integer, dimension(:), allocatable :: vw
	integer, dimension (:) :: desca
	double complex, dimension (:,:) :: la
!Output variables
	double complex, dimension (:,:) :: inta
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: n,i,lw,iw,liw
	integer :: mb,nb,pr,pc,lldr,lldc,lldwc,numroc
	double complex, dimension (:,:), allocatable :: one
	double complex, dimension (1:nps,1:nps) :: ltmp
	integer, dimension(9) :: desctmp
	double precision :: dw
	double complex :: zalpha



	call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
	nprocs=nprow*npcol

	mb = desca(5)
	nb = desca(6)
	n=nps*npw

	lldwc=size(la,2)/nps


dw=2.d0*wm/(npw-1)

inta=zero
do lw = 1, lldwc
	liw=(lw-1)*nps+1
	inta(:,:) = inta(:,:)+ la(:,liw:liw+nps-1)*dw/(2.d0*pi)
end do


call zgsum2d(context, 'A', 'i-ring', nps, nps, inta, nps, outrow, outcol)



end subroutine wint
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Integrate a matrix over w (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
