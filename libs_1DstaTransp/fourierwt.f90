!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	Fourier transform w-t (End)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine fourierwt(context,nps,npw,wm,dir,la,desca,lat)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
!Input variables
	integer :: nps,npw,dir
	double precision :: wm
	integer, dimension (:) :: desca
	double complex, dimension (:,:) :: la
!Output variables
	double complex, dimension (:,:) :: lat
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: n,i,tt,iw,lr,lc,lir,lic,lw,lwp
	integer :: mb,nb,pr,pc,lldr,lldc,lldwr,lldwc,numroc
	integer, dimension (:), allocatable :: vr,vc
	double precision :: om,tm,dt,t,dw
	integer, dimension (9) :: descw,desctmp
	double complex, dimension(nps,nps) :: intg
	double complex, dimension (:,:), allocatable :: lattmp

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

lldwr=numroc(npw,1,myrow,0,1)
lldwc=numroc(npw,1,mycol,0,nprocs)

mb = desca(5)
nb = desca(6)
n=nps*nprocs

lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)

allocate(lattmp(nps,lldc))

call descinit(descw,1,npw,1,1,0,0,context,lldwr,info)
call descinit(desctmp,nps,n,mb,nb,0,0,context,lldr,info)

dw=2.d0*wm/(npw-1)
tm=pi*(npw-1)/(2.d0*wm)
dt=pi/wm

allocate(vr(lldwr),vc(lldwc))

lr=0
do iw=1, npw
pr=Mod(Floor(Real(iw-1)),1)
if(myrow==pr) then
lr=lr+1
vr(lr)=iw
end if
end do

lc=0
do iw=1, npw
pc=Mod(Floor(Real(iw-1)),nprocs)
if(mycol==pc) then
lc=lc+1
vc(lc)=iw
end if
end do

!SUCA
if (dir==1) then
do tt=1, npw
	call infog2l(1,tt,descw,1,nprocs,myrow,mycol,lw,lwp,rsrc,csrc)
	t = -tm+(tt-1)*dt

	!Sum over the local frequencies
	lattmp=zero
	do lc = 1, lldwc
		lic=(lc-1)*nps+1
		om = -wm+(vc(lc)-1)*dw
		lattmp(:,:) = lattmp(:,:)+exp(-ci*om*t)*la(:,lic:lic+nps-1)
	end do

	call wint(context,nps,nprocs,wm,lattmp,desctmp,-1,-1,intg)

	if (myrow==rsrc.and.mycol==csrc) then
		lic=(lwp-1)*nps+1
		lat(:,lic:lic+nps-1)=intg(:,:)*(nprocs-1)/(npw-1)
	end if

end do

else if (dir ==-1) then
do tt=1, npw
	call infog2l(1,tt,descw,1,nprocs,myrow,mycol,lw,lwp,rsrc,csrc)
	om = -wm+(tt-1)*dw

	!Sum over the local times
	lattmp=zero
	do lc = 1, lldwc
		lic=(lc-1)*nps+1
		t = -tm+(vc(lc)-1)*dt
		lattmp(:,:) = lattmp(:,:)+exp(ci*om*t)*la(:,lic:lic+nps-1)
	end do

	call wint(context,nps,nprocs,tm,lattmp,desctmp,-1,-1,intg)

	if (myrow==rsrc.and.mycol==csrc) then
		lic=(lwp-1)*nps+1
		lat(:,lic:lic+nps-1)=(2.d0*pi)*intg(:,:)*(nprocs-1)/(npw-1)
	end if

end do

end if

end subroutine fourierwt
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Fourier transform w-t (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
