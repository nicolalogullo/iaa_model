!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!  Hamiltonian for spin one-half particles (Start)
!  distributed over process grid
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine phamspin(npx,bc,lham,descham,onsite,nnhop)
	use mpi
	implicit none
!Input variables
	integer :: npx, bc
	integer, dimension (:) :: descham
	double precision, dimension(:) :: onsite
	double precision, dimension(:) :: nnhop
!Input/output variables
	double precision, dimension(:,:) :: lham
!Dummy variables
	double precision :: dalpha
	integer :: i

lham=0.0d0

!Diagonal elements
do i=1, npx
!up
	dalpha = onsite(i)
	call pdelset(lham,2*i-1,2*i-1,descham,dalpha)
!down		
	dalpha = onsite(i)
	call pdelset(lham,2*i,2*i,descham,dalpha)

end do
!Nearest Neighbour	
do i=1, npx-1
	dalpha = nnhop(i)
	call pdelset(lham,2*i-1,2*i+1,descham,dalpha)
	call pdelset(lham,2*i+1,2*i-1,descham,dalpha)
	call pdelset(lham,2*i,2*i+2,descham,dalpha)
	call pdelset(lham,2*i+2,2*i,descham,dalpha)
end do

if (bc==1) then
!Periodic boundary conditions
	dalpha = nnhop(i)
	call pdelset(lham,1,2*npx-1,descham,dalpha)
	call pdelset(lham,2*npx-1,1,descham,dalpha)
	call pdelset(lham,2,2*npx,descham,dalpha)
	call pdelset(lham,2*npx,2,descham,dalpha)
end if


end subroutine phamspin

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!  Hamiltonian for spin one-half particles (End)
!  distributed over process grid
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
