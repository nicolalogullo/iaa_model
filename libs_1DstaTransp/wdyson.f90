
!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Dyson (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wdyson(context,nps,npw,wm,vw,mu,beta,stat,lg0l,lg0g,lg0r,lsl,lsg,lsr,lnegl,lnegg,lnegr)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npt,stat
	double precision :: wm,mu,beta
	integer, dimension (:) :: vw
	double complex, dimension(:,:) :: lg0r,lg0l,lg0g
!Input/Output variables
	double complex, dimension(:,:) :: lnegr,lnegl,lnegg
	double complex, dimension(:,:), allocatable :: lgrsr
	double complex, dimension(:,:) :: lsr,lsl,lsg
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: n,npw,iw,lw,liw,lldwc,numroc
	double precision :: dalpha,om
	double complex :: zalpha
	double complex, dimension(:,:), allocatable :: lnegtmp1, lnegtmp2

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

allocate(lgrsr(nps,nps))

allocate(lnegtmp1(nps,nps),lnegtmp2(nps,nps))

lgrsr=zero
lnegtmp1=zero
lnegtmp2=zero

!lldwc=numroc(npw,1,mycol,0,nprocs)
lldwc=size(vw)

do lw=1, lldwc

	liw=(lw-1)*nps+1

	call wsolver(nps,lg0r(:,liw:liw+nps-1),lsr(:,liw:liw+nps-1),lgrsr)

	!negr = matmul(grsr,g0r)
	lnegr(1:nps,liw:liw+nps-1) = matmul(lgrsr(1:nps,1:nps),lg0r(1:nps,liw:liw+nps-1))

	!--------------------------------------------------------------------


	!!+++++++++++++++++++++++++++L/G NE Green's+++++++++++++++++++++++++++
	!lnegtmp2(1:nps,1:nps) = conjg(transpose(lnegr(1:nps,liw:liw+nps-1)))
	!om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Lesser Green's function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!dalpha=1.d0*stat*(1.0d0/(exp(beta*(om-mu))-1.0d0*stat))
	!lnegtmp1(1:nps,1:nps)=dalpha*(lnegr(1:nps,liw:liw+nps-1)-lnegtmp2(1:nps,1:nps))
	!lnegl(1:nps,liw:liw+nps-1) = lnegtmp1(1:nps,1:nps)+&
	!			&matmul(lnegr(1:nps,liw:liw+nps-1),matmul(lsl(1:nps,liw:liw+nps-1),lnegtmp2(1:nps,1:nps)))

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Greater Green's function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!dalpha=1.d0*(1.d0-1.0d0/(exp(beta*(om-mu))-1.0d0*stat))
	!lnegtmp1(1:nps,1:nps)=dalpha*(lnegr(1:nps,liw:liw+nps-1)-lnegtmp2(1:nps,1:nps))
	!lnegg(1:nps,liw:liw+nps-1) =  lnegtmp1(1:nps,1:nps)+&
	!			&matmul(lnegr(1:nps,liw:liw+nps-1),matmul(lsg(1:nps,liw:liw+nps-1),lnegtmp2(1:nps,1:nps)))
	!!--------------------------------------------------------------------

	!+++++++++++++++++++++++++++L/G NE Green's+++++++++++++++++++++++++++
	lnegtmp1(1:nps,1:nps) = conjg(transpose(lgrsr(1:nps,1:nps)))
	lnegtmp2(1:nps,1:nps) = conjg(transpose(lnegr(1:nps,liw:liw+nps-1)))
	om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!Lesser Green's function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	lnegl(1:nps,liw:liw+nps-1) = stat*matmul(lgrsr(1:nps,1:nps),matmul(lg0l(1:nps,liw:liw+nps-1),lnegtmp1(1:nps,1:nps)))+&
				&1.0d0*matmul(lnegr(1:nps,liw:liw+nps-1),matmul(lsl(1:nps,liw:liw+nps-1),lnegtmp2(1:nps,1:nps)))

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!Greater Green's function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	lnegg(1:nps,liw:liw+nps-1) = stat*matmul(lgrsr(1:nps,1:nps),matmul(lg0g(1:nps,liw:liw+nps-1),lnegtmp1(1:nps,1:nps)))+&
				&1.0d0*matmul(lnegr(1:nps,liw:liw+nps-1),matmul(lsg(1:nps,liw:liw+nps-1),lnegtmp2(1:nps,1:nps)))
	!--------------------------------------------------------------------

end do

call blacs_barrier(context,'A')

end subroutine wdyson

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Dyson (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
