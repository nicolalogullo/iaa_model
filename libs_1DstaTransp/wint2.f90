!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	Integrate a matrix over w (End)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wint2(context,nps,npw,wm,la,desca,inta)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
!Input variables
	integer :: nps,npw
	double precision :: wm
	integer, dimension(:), allocatable :: vw
	integer, dimension (:) :: desca
	double complex, dimension (:,:) :: la
!Output variables
	double complex, dimension (:,:) :: inta
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: n,i,lw,iw,liw
	integer :: mb,nb,pr,pc,lldr,lldc,lldwc,numroc
	double complex, dimension (:,:), allocatable :: one
	double complex, dimension (1:nps,1:nps) :: ltmp
	integer, dimension(9) :: desctmp
	double precision :: dw
	double complex :: zalpha

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desca(5)
nb = desca(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)

lldwc=numroc(npw,1,mycol,0,nprocs)

call descinit(desctmp,nps,nps,mb,nb,0,0,context,lldr,info)

allocate(one(lldr,lldc))
allocate(vw(lldwc))



liw=0
do iw=1, npw
pc=Mod(Floor(Real(iw-1)),nprocs)
if(mycol==pc) then
liw=liw+1
vw(liw)=iw
end if
end do



one=zero
do lw = 1, lldwc
liw=(lw-1)*nps+1

!if(vw(lw)==1.or.vw(lw)==npw) then
!	zalpha = uno/3.0d0
!	else if(mod(vw(lw),2)==0) then
!	zalpha = 4.d0*uno/3.0d0
!	else if(mod(vw(lw),2)==1) then
!	zalpha = 2.d0*uno/3.0d0
!end if

zalpha=uno
do i=1,nps
	one(i,liw+i-1) = zalpha
end do
end do

dw=2.d0*wm/(npw-1)

ltmp=zero
call pzgemm ('N','T',nps,nps,n,dw*uno/(2.0d0*pi),la,1,1,desca,one,1,1,desca,zero,ltmp,1,1,desctmp)

inta=zero
if (myrow==0.and.mycol==0) then
	inta = ltmp
	call zgebs2d(context,'A','i-ring',nps,nps,inta,nps)
else
	call zgebr2d(context,'A','i-ring',nps,nps,inta,nps,0,0)
end if

end subroutine wint2
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Integrate a matrix over w (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
