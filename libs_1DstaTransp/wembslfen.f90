!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	Embedding self-energies (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wembslfen(context,nps,npw,wm,vw,lds,g,bt,mu,ea,shp,V,T,stat,lsl,lsg,lsr)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	character(*) :: shp
	integer :: nps,npw,stat
	double precision :: wm,bt,ea,mu,V,T
	double precision, dimension(:) :: g
	integer, dimension (:) :: lds
	integer, dimension (:) :: vw
!Input/Output variables
	double complex, dimension(:,:) :: lsl
	double complex, dimension(:,:) :: lsg
	double complex, dimension(:,:) :: lsr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: i,j,iw,lw,liw,lldwc,numroc
	double precision :: dalpha,om

!create the DOS for the leads
!eps0=2.0d0
!sgm0=-1.5d0

!call edos(context,shp,tf,npt,npw,eps0,sgm0,V,dos,descdos)

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

!lldwc=numroc(npw,1,mycol,0,nprocs)
lldwc = size(vw)

lsr=zero
lsg=zero
lsl=zero

!!!!!!!!!!!!!!!!!!!!!!!!!!!!Retarded Self-energy!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do lw=1,lldwc
	liw=(lw-1)*nps+1
	do i=1,size(lds)
		do j=1,size(lds)
			lsr(lds(i),liw+lds(j)-1)=-ci*0.5d0*(g(i)*g(j))
		end do
	end do
end do

!Tight-Binding Semi-Infinite Leads
if (shp == 'tb') then

do lw=1,lldwc
	liw=(lw-1)*nps+1
	om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)
	do i=1,size(lds)

	if((om-ea+mu-V).gt.2*Abs(T)) then
		lsr(lds(i),liw+lds(i)-1)=0.5d0*(om-ea+mu-V-sqrt((om-ea+mu-V)**2.d0-4*T**2.d0))*(g(i)**2.d0)/T**2.d0

	elseif(Abs(om-ea+mu-V).le.2*Abs(T)) then
		lsr(lds(i),liw+lds(i)-1)=0.5d0*(om-ea+mu-V-ci*sqrt(4*T**2.d0-(om-ea+mu-V)**2.d0))*(g(i)**2.d0)/T**2.d0

	else
		lsr(lds(i),liw+lds(i)-1)=0.5d0*(om-ea+mu-V+sqrt((om-ea+mu-V)**2.d0-4*T**2.d0))*(g(i)**2.d0)/T**2.d0

	end if
	end do
end do

end if

!Bosonic leads
if (shp == 'bos') then
lsr=zero
do lw=1,lldwc
	liw=(lw-1)*nps+1
	om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)
	if(om>0.d0) then
        do i=1,size(lds)
                do j=1,size(lds)
		        lsr(lds(i),liw+lds(j)-1)=-ci*0.5d0*(g(i)*g(j))
                end do
	end do
        end if
end do

end if


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!Lesser Self-energy!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do lw=1, lldwc
	liw=(lw-1)*nps+1
	om=-wm+2*wm*(vw(lw)-1)/(npw-1)
        if(shp=='bos'.and.om.le.0) then
                dalpha=0.d0
        else
                dalpha=stat*(1.0d0/(exp(bt*(om-mu-V))-1.0d0*stat))
        end if
	lsl(1:nps,liw:liw+nps-1)=dalpha*(lsr(1:nps,liw:liw+nps-1)-transpose(conjg(lsr(1:nps,liw:liw+nps-1))))

end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Greater Self-energy!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do lw=1, lldwc
	liw=(lw-1)*nps+1
	om=-wm+2*wm*(vw(lw)-1)/(npw-1)
	if(shp=='bos'.and.om.le.0) then
                dalpha=0.d0
        else
                dalpha=(1.d0+stat*1.0d0/(exp(bt*(om-mu-V))-1.0d0*stat))
        end if
	lsg(1:nps,liw:liw+nps-1)=dalpha*(lsr(1:nps,liw:liw+nps-1)-transpose(conjg(lsr(1:nps,liw:liw+nps-1))))
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


end subroutine wembslfen
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Embedding self-energies (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvjvvvvvvvvvvvvvvvv
