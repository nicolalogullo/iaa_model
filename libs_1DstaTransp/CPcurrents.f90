!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Compute & Print Currents (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine CPcurrents(context,nps,ny,npw,wm,vw,stat,iXL,iXR,gg1,gg2,beta1,beta2,mu1,mu2,V1,V2,lnegl,lnegr, lsmbr, desc, confg, folder)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,ny,npw, stat
	double precision :: wm, beta1, beta2, mu1, mu2, V1, V2
	double precision, dimension(:) :: gg1,gg2
	integer, dimension (:) :: desc
	integer, dimension(:) :: vw, ixL,ixR
	double complex, dimension(:,:) :: lnegl,lnegr, lsmbr
	character(*) :: confg, folder

!Varibles for grid query
	integer :: context, nprocs, lldr, lldc, lldwc, nprow, npcol, myrow, mycol
!Dummy variables
	integer :: i, n,mb,nb,j,lw,liw,iw,numroc, median
	double complex, dimension(nps,nps) :: intg
	double precision :: om, dfdmu,dw, sum,hsum
	double precision :: mcurl, mcurr, ecurl, ecurr, el, er, mcondl, mcondr, mcondtl, mcondtr, Icurr
	double complex, dimension(:,:), allocatable :: ltmp, lsembl,lsembg,lsembr, lsr, lsl, lsg
	double precision, dimension(:,:), allocatable :: GammaR, GammaL, Gammamb, Tfun, TfuncL, TfuncR


call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol


mb = desc(5)
nb = desc(6)
n=nps*npw


lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)
lldwc=numroc(npw,1,mycol,0,nprocs)


allocate(lsembl(lldr,lldc),lsembg(lldr,lldc),lsembr(lldr,lldc))
allocate(GammaR(lldr,lldc), GammaL(lldr,lldc), Gammamb(lldr,lldc))
allocate(lsl(lldr,lldc),lsg(lldr,lldc),lsr(lldr,lldc))
allocate(ltmp(lldr,lldc))
allocate(Tfun(npw,1), TfuncL(npw,1), TfuncR(npw,1))



dw=2.d0*wm/(npw-1)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Currents (Start)
!--------------------------------------------------------------------
mcurl=0.0d0
mcurr=0.0d0

ecurl=0.0d0
ecurr=0.0d0

el=0.0d0
er=0.0d0

mcondl=0.d0
mcondr=0.d0


lsr=zero
lsl=zero
lsg=zero

!left
do i=1, ny

	lsembr=zero
	lsembl=zero
	lsembg=zero


	call wembslfen(context,nps,npw,wm,vw,iXL,gg1,beta1,mu1,0.d0,'culo',V1,0.d0,-1,lsembl,lsembg,lsembr)

	lsr=lsr+lsembr
	lsl=lsl+lsembl
	lsg=lsg+lsembg


end do

lsembr=lsr
lsembl=lsl
lsembg=lsg

GammaL=-2.d0* aimag(lsembr)

call wmcurr(context,nps,npw,wm,lnegl,lnegr,lsembl,lsembr,desc,0,0,mcurl)
!call wecurrlds(context,nps,npw,wm,ham,lnegl,lnegr,lsl,lsr,lsembl,lsembr,desc,ecurl)
call wecurrlds(context,nps,npw,wm,vw,lnegl,lnegr,lsembl,lsembr,desc,0,0,ecurl)
call wmcond(context,nps,npw,wm,vw,beta1,mu1, V1, -1, GammaL,lnegr,desc,0,0,mcondl)



lsr=zero
lsl=zero
lsg=zero


!right
do i=1, ny

	lsembr=zero
	lsembl=zero
	lsembg=zero


	call wembslfen(context,nps,npw,wm,vw,iXR,gg2,beta2,mu2,0.d0,'culo',V2,0.d0,-1,lsembl,lsembg,lsembr)

	lsr=lsr+lsembr
	lsl=lsl+lsembl
	lsg=lsg+lsembg

end do

lsembr=lsr
lsembl=lsl
lsembg=lsg

GammaR=-2.d0* aimag(lsembr)


call wmcurr(context,nps,npw,wm,lnegl,lnegr,lsembl,lsembr,desc,0,0,mcurr)
call wecurrlds(context,nps,npw,wm,vw,lnegl,lnegr,lsembl,lsembr,desc,0,0,ecurr)
call wmcond(context,nps,npw,wm,vw,beta2,mu2, V2, -1, GammaR,lnegr,desc,0,0,mcondr)


if(myrow==0.and.mycol==0) then
	print*, "curr", confg
	print*, 'Il=', mcurl, 'Ir=', mcurr
	print*, 'Jl=', ecurl, 'Jr=', ecurr
	print*, 'Gl=', mcondl, 'Gr=', mcondr
end if



!--------------------------------------------------------------------
!			Currents (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Transmission Function (Start)
!--------------------------------------------------------------------

!non interactiong transmission function
Gammamb=-2.d0*aimag(lsmbr)

ltmp=0.d0
Tfun=0.d0
TfuncL=0.d0
TfuncR=0.d0

do lw=1, lldwc
	liw=(lw-1)*nps+1
	om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)

	ltmp(1:nps,1:nps) = matmul(GammaL(1:nps,liw:liw+nps-1),matmul(lnegr(1:nps,liw:liw+nps-1), matmul(GammaR(1:nps,liw:liw+nps-1),transpose(conjg(lnegr(1:nps,liw:liw+nps-1))))))
	do i=1, nps
		Tfun(vw(lw),1)=Tfun(vw(lw),1)+ltmp(i,i)
	end do

	ltmp(1:nps,1:nps) = matmul(GammaL(1:nps,liw:liw+nps-1),matmul(lnegr(1:nps,liw:liw+nps-1), matmul(Gammamb(1:nps,liw:liw+nps-1),transpose(conjg(lnegr(1:nps,liw:liw+nps-1))))))
	do i=1, nps
		TfuncL(vw(lw),1)=TfuncL(vw(lw),1)+ltmp(i,i)
	end do

	ltmp(1:nps,1:nps) = matmul(GammaR(1:nps,liw:liw+nps-1),matmul(lnegr(1:nps,liw:liw+nps-1), matmul(Gammamb(1:nps,liw:liw+nps-1),transpose(conjg(lnegr(1:nps,liw:liw+nps-1))))))
	do i=1, nps
		TfuncR(vw(lw),1)=TfuncR(vw(lw),1)+ltmp(i,i)
	end do

end do

call dgsum2d(context, 'R', 'i-ring', npw, 1, Tfun, npw, 0, 0)
call dgsum2d(context, 'R', 'i-ring', npw, 1, TfuncL, npw, 0, 0)
call dgsum2d(context, 'R', 'i-ring', npw, 1, TfuncR, npw, 0, 0)



!--------------------------------------------------------------------
!			Transmission Function (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
mcondtl=0.d0
mcondtr=0.d0
Icurr=0.d0
do iw=1,npw
	om = -wm+2.d0*wm*(iw-1)/(npw-1)

	dfdmu = beta1/((exp(beta1*(om-mu1-V1))-1.d0*stat)*(1-1.d0*stat*exp(-beta1*(om-mu1-V1))))
	mcondtl = mcondtl + dfdmu*(Tfun(iw,1) + TfuncL(iw,1))

	dfdmu = beta2/((exp(beta2*(om-mu2-V2))-1.d0*stat)*(1-1.d0*stat*exp(-beta2*(om-mu2-V2))))
	mcondtr = mcondtr + dfdmu*(Tfun(iw,1) + TfuncL(iw,1))

	dfdmu = 1/(exp(beta1*(om-mu1-V1))-1.d0*stat) - 1/(exp(beta2*(om-mu2-V2))-1.d0*stat)
	Icurr = Icurr + dfdmu*Tfun(iw,1)
end do

mcondtl=mcondtl*dw/(2.0d0*pi)
mcondtr=mcondtr*dw/(2.0d0*pi)

!_______________mediana_Tfun__________________________________________________________
sum=0.d0
do iw=1,npw
	sum=sum+Tfun(iw,1)
end do
iw=1
hsum=0.d0
do while((hsum<=0.5d0*sum).or.iw<=npw)
	hsum=hsum+Tfun(iw,1)
	median=iw
	iw=iw+1
end do

!______________________________________________________________________________!
!						Stampa
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!



if (myrow.eq.0 .and. mycol.eq.0) then
	open(unit=21,file=trim(folder)//'currents'//trim(confg)//'.dat',status='unknown',access='append')
	write(21,'(E14.6E3,5x)') mcurl, mcurr, ecurl, ecurr, mcondl, mcondr, mcondtl, mcondtr, median, -wm+2.d0*wm*(median-1)/(npw-1)  !,Icurr
	close(21)
end if


if (myrow==0.and.mycol==0) then
	open(unit=21,file='pesante'//trim(folder)//'Itfun'//trim(confg)//'.dat',status='unknown',access='append')
	open(unit=22,file='pesante'//trim(folder)//'TtfuncL'//trim(confg)//'.dat',status='unknown',access='append')
	open(unit=23,file='pesante'//trim(folder)//'TtfuncR'//trim(confg)//'.dat',status='unknown',access='append')
	do iw=1, npw
					write(21,*) Tfun(iw,1)+TfuncL(iw,1)+TfuncR(iw,1)
					write(22,*) TfuncL(iw,1)
					write(23,*) TfuncR(iw,1)
	end do
	close(21)
	close(22)
	close(23)
end if


deallocate(lsembl,lsembg,lsembr)
deallocate(lsl,lsg,lsr)
deallocate(GammaR, GammaL, Gammamb)
deallocate(Tfun, TfuncL, TfuncR)
deallocate(ltmp)

end subroutine CPcurrents
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Compute & Print Currents (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
