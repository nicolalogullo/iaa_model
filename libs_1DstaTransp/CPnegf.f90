!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Compute & Print phys quant from Negf (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine CPnegf(context,npx,npy,npw,wm,vw,d,lnegl,lnegr, desc, confg, folder)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: npx,npy,npw, d
	double precision :: wm
	integer, dimension (:) :: desc
	integer, dimension(:) :: vw
	double complex, dimension(:,:) :: lnegl,lnegr
	character(*) :: confg, folder

	!Varibles for grid query
		integer :: context, nprow, npcol, myrow, mycol
!Dummy variables
	integer :: n, nps, mb, nb,i,j,lw,liw,numroc, iw
	double precision :: median,sum,hsum, occ
	double complex, dimension(:,:), allocatable :: intg
	double precision, dimension(:,:), allocatable :: rho, specfun
	double precision, dimension(:), allocatable :: pnum, dos


	nps=npx*npy
	allocate(pnum(nps))
	allocate(intg(nps,nps))
	allocate(rho(nps,nps))
	allocate(dos(npw))
	! allocate(specfun(npw,nps))



call blacs_gridinfo(context,nprow,npcol,myrow,mycol)



!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Scrittura NEGFs (Start)
!--------------------------------------------------------------------
pnum=0.d0
dos=0.d0
specfun=0.d0
rho=0.d0
call woccnum(context,nps,npw,wm,1,lnegl,desc,0,0,pnum)
call wdos(context,nps,npw,lnegr,dos)
!call wspecfun(context,d,npx,npy,npw,lnegr,specfun)

intg=0.d0
call wint(context,nps,npw,wm,lnegl,desc,0,0,intg)
rho=-1.d0*Aimag(intg)
if (myrow.eq.0 .and. mycol.eq.0) then
	print*, "dimmi qualcosa"
end if

!
! !******************************************************************************
! !_______________mediana_DOS__________________________________________________________
! sum=0.d0
! do iw=1,npw
! 	sum=sum+dos(iw)
! end do
! iw=1
! hsum=0.d0
! do while((hsum<=0.5d0*sum).and.iw<=npw)
! 	hsum=hsum+dos(iw)
! 	median=iw
! 	iw=iw+1
! end do
!
! occ=0.d0
! do i=1,nps
! 	occ=occ+pnum(i)
! end do
!
! if (myrow.eq.0 .and. mycol.eq.0) then
! 	open(unit=21,file=trim(folder)//'DosCenter'//trim(confg)//'.dat',status='unknown',access='append')
! 	write(21,*) median
! 	write(21,*) -wm+2.d0*wm*(median-1)/(npw-1)
! 	write(21,*) sum
! 	write(21,*) occ
! 	close(21)
! end if


!______________________________________________________________________________

if(myrow==0.and.mycol==0) then
	open(unit=21,file=trim(folder)//'pnum'//trim(confg)//'.dat',position='append',status='unknown')
	open(unit=22,file=trim(folder)//'dos'//trim(confg)//'.dat',position='append',status='unknown')
	! open(unit=23,file='pesante'trim(folder)//'specfun'//trim(confg)//'.dat',position='append',status='unknown')
	! open(unit=24,file='pesante'trim(folder)//'rho'//trim(confg)//'.dat',position='append',status='unknown')

	do i=1,nps
		write(21,*) pnum(i)
	end do
	do i=1,npw
		write(22,*) dos(i)
	end do
	! do i=1,npw
	! do j=1,nps
	! 	write(23,*) specfun(i,j)
	! end do
	! end do
	! do i=1,nps
	! 	do j=1,nps
	! 		write(24,*) rho(i,j)
	! 	end do
	! end do


	close(21)
	close(22)
	! close(23)
	! close(24)

end if



!--------------------------------------------------------------------
!			Scrittura NEGFs (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


deallocate(pnum)
deallocate(intg)
deallocate(rho)
 deallocate(dos)
! deallocate(specfun)


end subroutine CPnegf
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Compute & Print phys quant from Negf (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
