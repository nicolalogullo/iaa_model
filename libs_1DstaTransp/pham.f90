!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!  Hamiltonian for spinless particles (Start)
!  distributed over process grid
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine pham(npx,bc,lham,descham,onsite,nnhop)
	use mpi
	implicit none
!Input variables
	integer :: npx, bc
	integer, dimension (:) :: descham
	double precision, dimension(:) :: onsite
	double precision, dimension(:) :: nnhop
!Input/output variables
	double precision, dimension(:,:) :: lham
!Dummy variables
	double precision :: dalpha
	integer :: i

lham=0.0d0

!Diagonal elements
do i=1, npx
	dalpha = onsite(i)
	call pdelset(lham,i,i,descham,dalpha)
end do
!Nearest Neighbour	
do i=1, npx-1
	dalpha = nnhop(i)
	call pdelset(lham,i,i+1,descham,dalpha)
	call pdelset(lham,i+1,i,descham,dalpha)
end do

if (bc==1) then
!Periodic boundary conditions
	dalpha = nnhop(i)
	call pdelset(lham,1,npx,descham,dalpha)
	call pdelset(lham,npx,1,descham,dalpha)
end if


end subroutine pham

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!  Hamiltonian for spinless particles (End)
!  distributed over process grid
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
