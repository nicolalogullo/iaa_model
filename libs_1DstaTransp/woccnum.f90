!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Occupation number (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine woccnum(context,nps,npw,wm,stat,lgr,desc,outrow,outcol,occn)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw, stat, outrow,outcol
	double precision :: wm
	integer, dimension (:) :: desc
	double complex, dimension(:,:) :: lgr
	double precision, dimension(:) :: occn
	double complex, dimension(nps,nps) :: intg
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: i,iw,lw,lwp,liw,liwp
	integer :: nb,mb,pr,pc,lldr,lldc,lldwr,lldwc,numroc
	double precision :: dalpha
	integer, dimension (9) :: descw

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

!mb = desc(5)
!nb = desc(6)
!n=nps*npw


!call descinit(descw,1,npw,1,1,0,0,context,lldwr,info)

intg=zero
call wint(context,nps,npw,wm,lgr,desc,outrow,outcol,intg)

	do i=1,nps
		occn(i)= stat*Aimag(intg(i,i))
	end do
end subroutine woccnum

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Occupation number (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
