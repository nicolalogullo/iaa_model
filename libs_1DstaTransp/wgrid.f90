!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Prints w (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wgrid(context,npw,wm,vw,prt,om)
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
!Input variables
	integer :: npw,prt
	double precision :: wm
!Input/Output variables
	integer, dimension(:) :: vw
	double precision, dimension (1:npw) :: om
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,pc,pr
!Dummy variables
	integer :: iw,liw,lldwc,numroc
	double precision :: deps

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

lldwc=numroc(npw,1,mycol,0,nprow*nprow)


liw=0
do iw=1, npw
pc=Mod(Floor(Real(iw-1)),nprocs)
if(mycol==pc) then
liw=liw+1
vw(liw)=iw
end if
end do

if(prt==1.and.myrow==0.and.mycol==0) then

do iw=1, npw
	om(iw)=om(iw) -wm+2.d0*wm*(iw-1)/(npw-1)
end do

end if



!if(prt==1.and.myrow==0.and.mycol==0) then
!open(unit=21,file=filenameout//'.dat',access = 'sequential',status='unknown')
!do iw=1, npw
!	write(21,*) -wm+2.d0*wm*(iw-1)/(npw-1)
!end do
!close(21)
!end if

end subroutine wgrid
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Prints w (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
