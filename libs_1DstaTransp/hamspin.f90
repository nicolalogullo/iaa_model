!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!  Hamiltonian for spin one-half particles (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine hamspin(npx,bc,onsite,nnhop,ham)
	use mpi
	implicit none
!Input variables
	integer :: npx, bc
	double precision, dimension(:) :: onsite
	double precision, dimension(:) :: nnhop
!Input/output variables
	double precision, dimension(:,:) :: ham
!Dummy variables
	integer :: i

ham=0.0d0

!Diagonal elements
do i=1, npx
!up
	ham(2*i-1,2*i-1) = onsite(i)
!down		
	ham(2*i,2*i) = onsite(i)
end do

!Nearest Neighbour
do i=1, npx-1
!up
	ham(2*i-1,2*i+1) = nnhop(i)
	ham(2*i+1,2*i-1) = nnhop(i)
!down
	ham(2*i,2*i+2) = nnhop(i)
	ham(2*i+2,2*i) = nnhop(i)
end do


!Periodic boundary conditions

if(bc==1) then
!up
	ham(2*npx-1,2*1+1) = nnhop(1)
	ham(2*1+1,2*npx-1) = nnhop(1)
!down
	ham(2*npx,2*1+2) = nnhop(1)
	ham(2*1+2,2*npx) = nnhop(1)
end if

end subroutine hamspin

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!  Hamiltonian for spin one-half particles (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
