
!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Energy of the leads (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wenrgl(context,nps,npw,wm,vw,stat,lgr,desc,outrow,outcol,enrg)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw, stat, outrow,outcol
	double precision :: wm
	integer, dimension(:) :: vw
	integer, dimension (:) :: desc
	double complex, dimension(:,:) :: lgr
!Output variables
	double precision, dimension(:) :: enrg
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: n,i,iw,lw,lwp,liw,liwp
	integer :: nb,mb,pr,pc,lldr,lldc,lldwr,lldwc,numroc
	double precision :: dalpha,dw,w
	integer, dimension (9) :: descw
	double complex, dimension(nps,nps) :: intg
	double complex, dimension(:,:), allocatable :: ltmp

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)

lldwr=numroc(npw,1,myrow,0,1)
lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(ltmp(lldr,lldc))

dw=2*wm/(npw-1)

do lw=1, lldwc
	liw=(lw-1)*nps+1
        w=-wm+(vw(lw)-1)*dw
	ltmp(1:nps,liw:liw+nps-1) = w*lgr(1:nps,liw:liw+nps-1)
end do

call descinit(descw,1,npw,1,1,0,0,context,lldwr,info)

intg=zero
call wint(context,nps,npw,wm,ltmp,desc,outrow,outcol,intg)

	do i=1,nps
		enrg(i)= stat*Aimag(intg(i,i))
	end do
end subroutine wenrgl

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Energy of the leads (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
