!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Free Green's (Start)
!	g0l(x,xp)=ci<b^{dag}_{ip}(tp) b_{i}(t)>_0
!	g0g(x,xp)=-ci<b_{i}(t) b^{dag}_{ip}(tp)>_0
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wfreeg(context,nps,npw,wm,vw,w,s,beta,mu,stat,lg0r,lg0l,lg0g,desc)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary Unit
	double complex, parameter :: zero = (0.0d0,0.0d0)			! Complex zero
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
!Input variables
	integer :: nps,npw,stat
	double precision :: wm,beta,mu
	double precision, dimension(:,:) :: s
	integer, dimension(:) :: vw
	double precision, dimension(:) :: w
!Input/Output variables
	integer, dimension(:) :: desc
	double complex, dimension(:,:) :: lg0r	
	double complex, dimension(:,:) :: lg0l
	double complex, dimension(:,:) :: lg0g
!Variables for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: n,i,ip,j,liw,lw
	integer :: nb,mb,pr,pc,lldr,lldc,lldwc,numroc
	double precision :: dalpha,om, t, tm, dt
	double complex :: zalpha
	double complex, dimension(:,:), allocatable :: lg0tmp

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)

lldwc=numroc(npw,1,mycol,0,nprocs)


allocate(lg0tmp(lldr,lldc))

lg0tmp = lg0r
lg0r=zero

!!!!!!!!!!!!!!!!!!!!!!!!!!!!Retarded Green's function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do lw=1, lldwc
	liw = (lw-1)*nps+1
	lg0r(1:nps,liw:liw+nps-1) = matmul(s,matmul(lg0tmp(1:nps,liw:liw+nps-1),transpose(s)))
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!Lesser Green's function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do lw=1, lldwc
	liw = (lw-1)*nps+1
	om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)
	if(stat==1.and.om.le.0.d0) then
                dalpha=0.d0
        else
                dalpha=stat*(1.0d0/(exp(beta*(om-mu))-1.0d0*stat))
	end if
        lg0l(1:nps,liw:liw+nps-1)=dalpha*(lg0r(1:nps,liw:liw+nps-1)-transpose(conjg(lg0r(1:nps,liw:liw+nps-1))))
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Greater Green's function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do lw=1, lldwc
	liw = (lw-1)*nps+1
	om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)
	if (stat==1.and.om.le.0.d0) then
                dalpha=0.d0
        else 
                dalpha=(1.d0+stat/(exp(beta*(om-mu))-1.0d0*stat))
	end if
        lg0g(1:nps,liw:liw+nps-1)=dalpha*(lg0r(1:nps,liw:liw+nps-1)-transpose(conjg(lg0r(1:nps,liw:liw+nps-1))))
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!lg0tmp=zero
!tm=pi*(npw-1)/(2.d0*wm)
!dt=pi/wm

!do lw=1, lldwc
!	liw=(lw-1)*nps+1
!	t=-tm+(vw(lw)-1)*dt
!		do i=1,nps
!		lg0tmp(i,liw+i-1)=-ci*stat*(1.0d0/(exp(beta*(w(i)-mu))-1.0d0*stat))*exp(-ci*(w(i)-ci*5.d-2*t/abs(t))*t)
!		end do
!end do
!lg0l=zero
!call fourierwt(context,nps,npw,wm,-1,lg0tmp,desc,lg0l)

!lg0tmp=zero
!do lw=1, lldwc
!	liw=(lw-1)*nps+1
!	t=-tm+(vw(lw)-1)*dt
!		do i=1,nps
!		lg0tmp(i,liw+i-1)=-ci*(1.d0-1.0d0/(exp(beta*(w(i)-mu))-1.0d0*stat))*exp(-ci*(w(i)-ci*5.d-2*t/abs(t))*t)
!		end do
!end do
!lg0g=zero
!call fourierwt(context,nps,npw,wm,-1,lg0tmp,desc,lg0g)


!lg0tmp=zero
!do lw=1, lldwc
!	liw=(lw-1)*nps+1
!	t=-tm+(vw(lw)-1)*dt
!		do i=1,nps
!			if (t.ge.0.d0.and.t<dt) then
!				lg0tmp(i,liw:liw+i-1)=-ci*0.5d0
!			else if (t>0.d0) then
!				lg0tmp(i,liw+i-1)=-ci*exp(-ci*(w(i)-ci*5.d-2)*t)
!			end if
!		end do
!end do
!lg0r=zero
!call fourierwt(context,nps,npw,wm,-1,lg0tmp,desc,lg0r)



end subroutine wfreeg
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Free Green's (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
