

!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		mass Current (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wmcurr(context,nps,npw,wm,lnegl,lnegr,lsembl,lsembr,desc,outrow,outcol,mcur)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw, outrow, outcol
	double precision :: wm
	integer, dimension (:) :: desc
	double complex, dimension(:,:) :: lnegl,lnegr,lsembl,lsembr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: n,i,j,lw,liw,mb,nb,lldc,lldr,lldwc,numroc
	double precision :: mcur
	double complex, dimension(nps,nps) :: intg
	double complex, dimension(:,:), allocatable :: ltmp

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldwc = size(lnegr,2)/nps
lldc = lldwc*lldr
! lldc=numroc(n,nb,mycol,0,nprocs)
! lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(ltmp(lldr,lldc))

ltmp=zero


do lw=1, lldwc
	liw=(lw-1)*nps+1
	ltmp(1:nps,liw:liw+nps-1) = matmul(lnegl(1:nps,liw:liw+nps-1), transpose(conjg(lsembr(1:nps,liw:liw+nps-1))))&
		& + matmul(lnegr(1:nps,liw:liw+nps-1), lsembl(1:nps,liw:liw+nps-1))
end do



call wint(context,nps,npw,wm,ltmp,desc,outrow,outcol,intg)

	mcur=0.0d0
	do i=1,nps
		 mcur = mcur + 2*Real(intg(i,i))
	end do


end subroutine wmcurr
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		mass current (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
