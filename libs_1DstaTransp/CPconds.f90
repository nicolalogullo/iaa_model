!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Compute & Print Currents (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine CPconds(context,nps,ny,npw,wm,vw,stat, iXL,iXR,iXD,ggL,ggR,ggD, betas,mus, lnegl,lnegr,lsmbr, desc, confg,folder)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,ny,npw, stat
	double precision :: wm
	double precision, dimension(:) :: mus, betas

	double precision, dimension(:) :: ggL,ggR,ggD
	integer, dimension (:) :: desc
	integer, dimension(:) :: vw, ixL,ixR, ixD
	double complex, dimension(:,:) :: lnegl,lnegr,lsmbr
	character(*) :: confg, folder

!Varibles for grid query
	integer :: context, nprocs, lldr, lldc, lldwc, nprow, npcol, myrow, mycol
!Dummy variables
double precision :: betaL, betaR,  muL, muR, VL, VR, VD
double precision, dimension(:), allocatable :: muD,betaD


	integer :: i, n,mb,nb,j,lw,liw,iw,numroc, median, ib, imu
	double complex, dimension(nps,nps) :: intg
	double precision :: om, dfdmu,dw, tsum,hsum
	double precision :: mcurl, mcurr, mcurd, ecurl, ecurr, ecurd, mconl, mconr, mcond
 	double precision, dimension(:), allocatable :: mflxd, eflxd
	double complex, dimension(:,:), allocatable :: ltmp, lsembl,lsembg,lsembr, lsr, lsl, lsg, lsrt, lslt, lsgt
	double precision, dimension(:,:), allocatable :: GammaR, GammaL, GammaD, Gammamb
	double precision, dimension(:,:), allocatable :: Tfun, TfuncL, TfuncR, TfunLD, TfunRD, TfuncD


call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol


mb = desc(5)
nb = desc(6)
n=nps*npw


lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)
lldwc=numroc(npw,1,mycol,0,nprocs)


allocate(lsembl(lldr,lldc),lsembg(lldr,lldc),lsembr(lldr,lldc))
allocate(GammaR(lldr,lldc), GammaL(lldr,lldc), GammaD(lldr,lldc), Gammamb(lldr,lldc))
allocate(lsl(lldr,lldc),lsg(lldr,lldc),lsr(lldr,lldc))
allocate(lslt(lldr,lldc),lsgt(lldr,lldc),lsrt(lldr,lldc))
allocate(ltmp(lldr,lldc))
allocate(Tfun(npw,1))
allocate( TfuncL(npw,1), TfuncR(npw,1))
allocate(TfunLD(npw,1), TfunRD(npw,1), TfuncD(npw,1))
allocate(mflxd(nps), eflxd(nps))
allocate(muD(nps), betaD(nps))


muD=-wm
mul=mus(1)
mur=mus(1)
betaD=betas(1)
betaL=betas(1)
betaR=betas(1)

VL=0.d0
VR=0.d0
VD=0.d0

dw=2.d0*wm/(npw-1)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!			Currents (Start)
!--------------------------------------------------------------------
mcurl=0.0d0
mcurr=0.0d0
mcurd=0.0d0
mflxd=0.d0

ecurl=0.0d0
ecurr=0.0d0
ecurd=0.0d0
eflxd=0.d0

lsrt=zero
lslt=zero
lsgt=zero

!left_______________________________________
lsr=zero
lsl=zero
lsg=zero

do i=1, ny
	call wembslfen(context,nps,npw,wm,vw,(/iXL(i)/),ggL,betaL,muL,0.d0,'culo',VL,0.d0,-1,lsembl,lsembg,lsembr)
	lsr=lsr+lsembr
	lsl=lsl+lsembl
	lsg=lsg+lsembg
end do

lsembr=lsr
lsembl=lsl
lsembg=lsg

GammaL=-2.d0* aimag(lsembr)


lsrt=lsrt+lsembr
lslt=lslt+lsembl
lsgt=lsgt+lsembg
!right___________________________________________
lsr=zero
lsl=zero
lsg=zero

do i=1, ny
	call wembslfen(context,nps,npw,wm,vw,(/iXR(i)/),ggR,betaR,muR,0.d0,'culo',VR,0.d0,-1,lsembl,lsembg,lsembr)
	lsr=lsr+lsembr
	lsl=lsl+lsembl
	lsg=lsg+lsembg
end do

lsembr=lsr
lsembl=lsl
lsembg=lsg


GammaR=-2.d0* aimag(lsembr)

lsrt=lsrt+lsembr
lslt=lslt+lsembl
lsgt=lsgt+lsembg
!Dissipation_________________________________________

call wembslfenDiss(context,nps,npw,wm,vw,iXD,ggD,betaD,muD,0.d0,'culo',VD,0.d0,-1,lsembl,lsembg,lsembr)


GammaD=-2.d0* aimag(lsembr)


lsrt=lsrt+lsembr
lslt=lslt+lsembl
lsgt=lsgt+lsembg




!--------------------------------------------------------------------
!			Currents (End)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


if (myrow==0 .and. mycol ==0) then
open(unit=21,file=trim(folder)//'condL_'//trim(confg)//'.dat',status='unknown',access='append')
open(unit=22,file=trim(folder)//'condR_'//trim(confg)//'.dat',status='unknown',access='append')
open(unit=23,file=trim(folder)//'condD_'//trim(confg)//'.dat',status='unknown',access='append')
open(unit=24,file=trim(folder)//'Betas_'//trim(confg)//'.dat',status='unknown',access='append')
open(unit=25,file=trim(folder)//'mus_'//trim(confg)//'.dat',status='unknown',access='append')

end if

do ib=1, size(Betas)
	betaR=Betas(ib)
	betaL=Betas(ib)
	betaD=Betas(ib)
	do imu=1, size(mus)
	mul=mus(imu)
	mur=mus(imu)
	muD=mus(imu)


	!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	!			Transmission Function (Start)
	!--------------------------------------------------------------------
		!non interactiong transmission function
		Gammamb=-2.d0*aimag(lsmbr)

		ltmp=0.d0
		Tfun=0.d0
		TfuncL=0.d0
		TfuncR=0.d0
		TfunLD=0.d0
		TfunRD=0.d0

		do lw=1, lldwc
			liw=(lw-1)*nps+1
			om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)
			!Landauer-Buttiker---------------
			ltmp(1:nps,1:nps) = matmul(GammaL(1:nps,liw:liw+nps-1),matmul(lnegr(1:nps,liw:liw+nps-1), matmul(GammaR(1:nps,liw:liw+nps-1),transpose(conjg(lnegr(1:nps,liw:liw+nps-1))))))
			do i=1, nps
				Tfun(vw(lw),1)=Tfun(vw(lw),1)+ltmp(i,i)
			end do

			ltmp(1:nps,1:nps) = matmul(GammaD(1:nps,liw:liw+nps-1),matmul(lnegr(1:nps,liw:liw+nps-1), matmul(GammaL(1:nps,liw:liw+nps-1),transpose(conjg(lnegr(1:nps,liw:liw+nps-1))))))
			do i=1, nps
				TfunLD(vw(lw),1)=TfunLD(vw(lw),1)+ltmp(i,i)
			end do

			ltmp(1:nps,1:nps) = matmul(GammaD(1:nps,liw:liw+nps-1),matmul(lnegr(1:nps,liw:liw+nps-1), matmul(GammaL(1:nps,liw:liw+nps-1),transpose(conjg(lnegr(1:nps,liw:liw+nps-1))))))
			do i=1, nps
				TfunRD(vw(lw),1)=TfunRD(vw(lw),1)+ltmp(i,i)
			end do

		! 	!Many-Body---------------------
		! 	ltmp(1:nps,1:nps) = matmul(GammaL(1:nps,liw:liw+nps-1),matmul(lnegr(1:nps,liw:liw+nps-1), matmul(Gammamb(1:nps,liw:liw+nps-1),transpose(conjg(lnegr(1:nps,liw:liw+nps-1))))))
		! 	do i=1, nps
		! 		TfuncL(vw(lw),1)=TfuncL(vw(lw),1)+ltmp(i,i)
		! 	end do
		!
		! 	ltmp(1:nps,1:nps) = matmul(GammaR(1:nps,liw:liw+nps-1),matmul(lnegr(1:nps,liw:liw+nps-1), matmul(Gammamb(1:nps,liw:liw+nps-1),transpose(conjg(lnegr(1:nps,liw:liw+nps-1))))))
		! 	do i=1, nps
		! 		TfuncR(vw(lw),1)=TfuncR(vw(lw),1)+ltmp(i,i)
		! 	end do
		!
		! 	ltmp(1:nps,1:nps) = matmul(GammaD(1:nps,liw:liw+nps-1),matmul(lnegr(1:nps,liw:liw+nps-1), matmul(Gammamb(1:nps,liw:liw+nps-1),transpose(conjg(lnegr(1:nps,liw:liw+nps-1))))))
		! 	do i=1, nps
		! 		TfuncD(vw(lw),1)=TfuncD(vw(lw),1)+ltmp(i,i)
		! 	end do

		end do

		call dgsum2d(context, 'R', 'i-ring', npw, 1, Tfun, npw, 0, 0)
		call dgsum2d(context, 'R', 'i-ring', npw, 1, TfunLD, npw, 0, 0)
		call dgsum2d(context, 'R', 'i-ring', npw, 1, TfunRD, npw, 0, 0)

		! call dgsum2d(context, 'R', 'i-ring', npw, 1, TfuncL, npw, 0, 0)
		! call dgsum2d(context, 'R', 'i-ring', npw, 1, TfuncR, npw, 0, 0)
		! call dgsum2d(context, 'R', 'i-ring', npw, 1, TfuncD, npw, 0, 0)


		!--------------------------------------------------------------------
		!			Transmission Function (End)
		!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		mconl=0.d0
		mconr=0.d0
		mcond=0.d0

		do iw=1,npw
			om = -wm+2.d0*wm*(iw-1)/(npw-1)

			dfdmu = betaL/((exp(betaL*(om-muL-VL))-1.d0*stat)*(1-1.d0*stat*exp(-betaL*(om-muL-VL))))
			mconl = mconl + dw*dfdmu*(Tfun(iw,1) )! + TfunLD(iw,1))! + TfuncL(iw,1))

			dfdmu = betaR/((exp(betaR*(om-muR-VR))-1.d0*stat)*(1-1.d0*stat*exp(-betaR*(om-muR-VR))))
			mconr = mconr + dw*dfdmu*(Tfun(iw,1) )!+ TfunRD(iw,1))! + TfuncR(iw,1))

			dfdmu = betaD(1)/((exp(betaD(1)*(om-muD(1)-VD))-1.d0*stat)*(1-1.d0*stat*exp(-betaD(5)*(om-muD(5)-VD))))
			mcond = mcond + dw*dfdmu*(TfunLD(iw,1) + TfunRD(iw,1))! + TfuncD(iw,1))

		end do

		mconl=mconl/(2.0d0*pi)
		mconr=mconr/(2.0d0*pi)
		mcond=mcond/(2.0d0*pi)

		!______________________________________________________________________________!
		!						Stampa
		!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!


		! if(myrow==0.and.mycol==0) then
		! 	print*, " "
		! 	print*, 'Beta=', betas(ib), 'mu=', mus(imu), "Gd=", mcond
		! 	print*, 'Gl=', mconl, 'Gr=', mconr
		! end if



		if(myrow==0.and.mycol==0) then
			write(21,*)  mconl
			write(22,*)  mconr
			write(23,*)  mcond
			write(24,*)  betas(ib)
			write(25,*)  mus(imu)
		end if

	end do
end do

if(myrow==0.and.mycol==0) then
 close(21)
 close(22)
 close(23)
 close(24)
 close(25)
end if

deallocate(lsembl,lsembg,lsembr)
deallocate(lsl,lsg,lsr)
deallocate(GammaR, GammaL, GammaD, Gammamb)
deallocate(Tfun)
!deallocate( TfuncL, TfuncR)
deallocate(TfunLD, TfunRD, TfuncD)
deallocate(ltmp)

end subroutine CPconds
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Compute & Print Currents (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
