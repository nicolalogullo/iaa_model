!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	free phonon green's (start) 
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine freeph(context,nps,npw,wm,vw,bt,om0,omC,sg0,g,ld0l,ld0g,ld0r,desc)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
!Input variables
	integer :: nps,npw
	double precision :: wm,g,bt,om0,omC,sg0,sgn
	double complex :: cf
	integer, dimension (:) :: vw, desc
!Input/Output variables
	double complex, dimension(:,:) :: ld0l
	double complex, dimension(:,:) :: ld0g
	double complex, dimension(:,:) :: ld0r
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: n,i,j,iw,lr,lc,lir,lic
	integer :: mb,nb,pr,pc,lldr,lldc,lldwr,lldwc,lldtr,lldtc,numroc
	integer, dimension (:), allocatable :: vr,vc
	double complex, dimension (:), allocatable :: phdos, Nbe
	integer, dimension(9) :: descphdos, descNbe 
	double complex, dimension (:,:), allocatable :: lexp, ltmp
	integer, dimension(9) :: descexp, desctmp	
	double complex, dimension(:,:), allocatable :: sfctp, sfct
	integer, dimension (9) :: descsfct, descsfctp
	double precision :: om,tm,dt,t,dw,tp,dalpha

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(n,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)

lldtr=numroc(1,1,myrow,0,1)
lldtc=numroc(npw,1,mycol,0,nprocs)

lldwr=numroc(npw,1,myrow,0,1)
lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(phdos(lldwc))
allocate(Nbe(lldwc))

allocate(ltmp(lldtr,lldtc),sfctp(lldtr,lldtc),sfct(lldtr,lldtc))
call descinit(desctmp,1,npw,1,1,0,0,context,lldtr,info)


dw=2.d0*wm/(npw-1)
tm=pi*(npw-1)/(2.d0*wm)
dt=pi/wm

allocate(vr(lldwr),vc(lldwc))

vr=0
lr=0
do iw=1, npw
pr=Mod(Floor(Real(iw-1)),1)
if(myrow==pr) then
lr=lr+1
vr(lr)=iw
end if
end do

vc=0
lc=0
do iw=1, npw
pc=Mod(Floor(Real(iw-1)),nprocs)
if(mycol==pc) then
lc=lc+1
vc(lc)=iw
end if
end do


phdos=zero
Nbe=zero
!create the spectral density and number distribution for a phonon bath
do lc = 1, lldwc
	om = -wm+(vc(lc)-1)*dw
        if (om > 0.0d0) then        
		phdos(lc) = (1/bt)*(sg0/pi)/(((om-om0)**2 + sg0**2 ))
                !phdos(lc) = ((om/omC)**2)*exp(2*(1-om/omC))
		Nbe(lc) = 1.0d0!/(exp(bt*om)-1.0d0) !  sg0/((om-om0)**2 + sg0**2)  ! 1.0d0/(exp(bt*om)-1.0d0) !		 
        end if
end do

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~STRUCTURE FACTOR~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!                    int dom/2pi F(om) n(om) exp[-i*om*(t-t')]
ltmp=zero
do lc = 1, lldwc
	om = -wm+(vc(lc)-1)*dw
	if (om.ne.0.d0) then
		ltmp(1,lc) = phdos(lc)*Nbe(lc)
	end if
end do

!Questa qua sotto non può funzionare è solo per riferimento
!call pzgemm ('N','N',nps,n,n,dw*uno/(2.0d0*pi),ltmp,1,1,desctmp,lexp,1,1,descexp,zero,sfct,1,1,descsfct)

call fourierwt(context,1,npw,wm,1,ltmp,desctmp,sfct)

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~STRUCTURE FACTOR~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!                    int dom /2pi F(om) [1+n(om)] exp[-i*om*(t-t')]

ltmp=zero
do lc = 1, lldwc	
	om = -wm+(vc(lc)-1)*dw
	if (om.ne.0.d0) then
		ltmp(1,lc) = phdos(lc)*(1.0d0+ Nbe(lc))
	end if
end do

!call pzgemm ('N','N',nps,n,n,dw*uno/(2.0d0*pi),ltmp,1,1,desctmp,lexp,1,1,descexp,zero,sfctp,1,1,descsfctp)
call fourierwt(context,1,npw,wm,1,ltmp,desctmp,sfctp)


!############################## LESSER #########################################


do lc = 1, lldwc
	lic=(lc-1)*nps+1
	do i=1,nps
	do j=1,nps	
		ld0l(i,lic+j-1) =-ci*sfct(1,lc)!*((-1)**i)*((-1)**j)
	end do 
	end do
end do


!############################## GREATER ######################################## 

do lc = 1, lldwc
	lic=(lc-1)*nps+1
	do i=1,nps
	do j=1,nps	
		ld0g(i,lic+j-1) = -ci*sfctp(1,lc)!*((-1)**i)*((-1)**j)
	end do 
	end do
end do

!############################## RETARDED ######################################## 


do lc = 1, lldwc
	lic=(lc-1)*nps+1
	t = -tm+(vc(lc)-1)*dt
	if (t.ge.0.d0.and.t<dt) then
		ld0r(:,lic:lic+nps-1) = 0.5d0*(ld0g(:,lic:lic+nps-1)-ld0l(:,lic:lic+nps-1))
	else if (t>0.d0) then
		ld0r(:,lic:lic+nps-1) = ld0g(:,lic:lic+nps-1)-ld0l(:,lic:lic+nps-1)
	end if
end do


!frequency domain
ltmp=ld0r
ld0r=zero
call fourierwt(context,1,npw,wm,-1,ltmp,desc,ld0r)

ltmp=ld0l
ld0l=zero
call fourierwt(context,1,npw,wm,-1,ltmp,desc,ld0l)

ltmp=ld0g
ld0g=zero
call fourierwt(context,1,npw,wm,-1,ltmp,desc,ld0g)


!phdos=zero
!!create the spectral density for a phonon bath
!)*((-1)**j)do lc = 1, lldwc
!	om = -wm+(vc(lc)-1)*dw
!!if(om>0.d0) then
!!	phdos(lc) = sg0/(pi*((om-om0)**2 + sg0**2 ))
!!end if

!!phdos(lc) = 1.0d0/(exp(-0.5d0*om0*om) +1.0d0)*1.0d0/(exp(0.5d0*om0*(om -2*om0)) + 1.0d0)&
!!		&*sin(om*pi/(2*om0))*sin(om*pi/(2*om0))/om0	

!phdos(lc) = 1.0d0/(exp(-sg0*om) +1.0d0)*om*exp(-om/om0)
!end do

!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~STRUCTURE FACTOR~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!!                    int dom/2pi F(om) n(om) exp[-i*om*(t-t')]

!lexp=zero
!dalpha=1.d0
!do lr = 1, lldwr
!do lc = 1, lldwc
!	lir=(lr-1)*nps+1
!	lic=(lc-1)*nps+1
!	om = -wm+(vr(lr)-1)*dw
!	t = -tm+(vc(lc)-1)*dt
!!Multiply by om to compute the energy current

!	do i=1,nps
!		lexp(lir+i-1,lic+i-1) = exp(-ci*om*t)
!	end do
!end do
!end do

!ltmp=zero
!do lc = 1, lldwc
!	lic=(lc-1)*nps+1	
!	om = -wm+(vc(lc)-1)*dw
!	if(sgn == 0) then
!		dalpha=om
!	end if
!	if (om.ne.0.d0) then
!	ltmp(:,lic:lic+nps-1) = dalpha*phdos(lc)*(1/(exp(bt*om)-1.0d0))
!	end if
!end do

!call pzgemm ('N','N',nps,n,n,dw*uno/(2.0d0*pi),ltmp,1,1,desctmp,lexp,1,1,descexp,zero,sfct,1,1,descsfct)



!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~STRUCTURE FACTOR~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!!                    int dom /2pi F(om) [1+n(om)] exp[i*om*(t-t')]
!lexp=zero
!dalpha=1.d0
!do lr = 1, lldwr
!do lc = 1, lldwc
!	lir=(lr-1)*nps+1
!	lic=(lc-1)*nps+1
!	om = -wm+(vr(lr)-1)*dw
!	t = -tm+(vc(lc)-1)*dt
!!Multiply by om to compute the energy current

!	do i=1,nps
!		lexp(lir+i-1,lic+i-1) = exp(ci*om*t)
!	end do
!end do
!end do

!ltmp=zero
!do lc = 1, lldwc
!	lic=(lc-1)*nps+1	
!	om = -wm+(vc(lc)-1)*dw
!	if(sgn == 0) then
!		dalpha=om
!	end if
!	if (om.ne.0.d0) then
!	ltmp(:,lic:lic+nps-1) = dalpha*phdos(lc)*(1.0d0+1.0d0/(exp(bt*om)-1.0d0))
!	end if
!end do

!call pzgemm ('N','N',nps,n,n,dw*uno/(2.0d0*pi),ltmp,1,1,desctmp,lexp,1,1,descexp,zero,sfctp,1,1,descsfctp)


!!############################## LESSER #########################################


!do lc = 1, lldc
!	lic=(lc-1)*nps+1	
!	ld0l(:,lic:lic+nps-1) = ld0l(:,lic:lic+nps-1)-ci*cf*( sfct(:,lic:lic+nps-1) + sgn*sfctp(:,lic:lic+nps-1) )*(g**2)
!end do


!!############################## GREATER ######################################## 

!do lc = 1, lldc
!	lic=(lc-1)*nps+1	
!	ld0g(:,lic:lic+nps-1) = ld0g(:,lic:lic+nps-1)-ci*cf*(sgn*conjg(sfct(:,lic:lic+nps-1))+conjg(sfctp(:,lic:lic+nps-1)))*(g**2)
!end do

!!############################## RETARDED ######################################## 


!do lc = 1, lldc
!	lic=(lc-1)*nps+1
!	t = -tm+(vc(lc)-1)*dt
!	if (t.ge.0.d0.and.t<dt) then
!		ld0r(:,lic:lic+nps-1) = 0.5d0*(ld0g(:,lic:lic+nps-1)-ld0l(:,lic:lic+nps-1))
!	else if (t>0.d0) then
!		ld0r(:,lic:lic+nps-1) = ld0g(:,lic:lic+nps-1)-ld0l(:,lic:lic+nps-1)
!	end if
!end do


end subroutine freeph
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	free phonon green's (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
