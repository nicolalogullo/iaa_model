!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Density of States (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wdos(context,nps,npw,lgr,dos)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
!	character(*) :: filenameout
	integer :: nps,npw
	double complex, dimension(:,:) :: lgr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: i,iw,lw,lwp,liw,liwp
	integer :: nb,mb,pr,pc,lldr,lldc,lldwr,lldwc,numroc
	double precision :: dalpha
	integer, dimension (9) :: descw

	double precision, dimension(1:npw) :: dos

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

!mb = desc(5)
!nb = desc(6)
!n=nps*npw

!lldr=numroc(nps,mb,myrow,0,1)
!lldc=numroc(n,nb,mycol,0,nprocs)

lldwr=numroc(npw,1,myrow,0,1)
lldwc=numroc(npw,1,mycol,0,nprocs)

call descinit(descw,1,npw,1,1,0,0,context,lldwr,info)

!if(myrow==0.and.mycol==0) then
!		open(unit=21,file=filenameout//'.dat',position='append',status='unknown')
!end if


dos=0.0d0
do iw=1, npw
	call infog2l(1,iw,descw,1,nprocs,myrow,mycol,lw,lwp,rsrc,csrc)

	if (myrow==rsrc.and.mycol==csrc) then
		liwp=(lwp-1)*nps+1
		dalpha=0.d0
		do i=1, nps
			dalpha = dalpha-Aimag(lgr(i,liwp+i-1))/pi
		end do
		call dgebs2d(context,'A','i-ring',1, 1,dalpha,1)
	else
		call dgebr2d(context,'A','i-ring',1,1,dalpha,1,rsrc,csrc)

	end if

	if(myrow==0.and.mycol==0) then
		dos(iw)=dos(iw)+dalpha
	end if

end do

!if(myrow==0.and.mycol==0) then
!close(21)
!end if

end subroutine wdos

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Density of States (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
