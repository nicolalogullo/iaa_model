
!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Dyson (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wdysonlg(context,nps,npw,ls,lnegr,lneg)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npt
	double complex, dimension(:,:) :: lnegr
!Input/Output variables
	double complex, dimension(:,:) :: lneg
	double complex, dimension(:,:) :: ls
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: n,npw,iw,lw,liw,lldwc,numroc
	double precision :: dalpha,om
	double complex :: zalpha
	double complex, dimension(:,:), allocatable :: lnegtmp

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

allocate(lnegtmp(nps,nps))

lnegtmp=zero

lldwc=numroc(npw,1,mycol,0,nprocs)

do lw=1, lldwc
	liw=(lw-1)*nps+1
	lnegtmp(1:nps,1:nps) = conjg(transpose(lnegr(1:nps,liw:liw+nps-1)))
	lneg(1:nps,liw:liw+nps-1) = matmul(lnegr(1:nps,liw:liw+nps-1),matmul(ls(1:nps,liw:liw+nps-1),lnegtmp(1:nps,1:nps)))
end do

call blacs_barrier(context,'A')

end subroutine wdysonlg

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Dyson (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

