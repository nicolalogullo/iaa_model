!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	Fourier transform w-t (End)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine fourierDFwt( context , Desc_DM , SIZE_ldft, NX_IN , NX_OUT , vw_ilc , vt_ilc, ns , la , dir , lat )
	use mpi
	use MKL_CDFT
	implicit none

!Useful numerical parameters
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	!Varibles for grid query
		integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
		integer :: colour_LC , LC_comm_world , error_LC, colour_FW
		type(DFTI_DESCRIPTOR_DM), POINTER :: Desc_DM

!Input variables
	integer :: ns, dir
	double complex, dimension (:,:) :: la
	integer, dimension(:) :: vw_ilc , vt_ilc
!Output variables
	double complex, dimension (:,:) :: lat

!Dummy variables
	integer :: i,j, iw, liw, lldwc, lldtc, lc_iw
	double complex, dimension (:), allocatable :: work
	double complex, dimension (:,:), allocatable :: work_w, work_t
	integer :: STATUS, SIZE_ldft, NX_IN, START_X, NX_OUT, START_X_OUT
	double precision :: startTime, endTIme


lldwc=size(vw_ilc)
lldtc=size(vt_ilc)

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol


!_____________________________________________!

!if ( colour_FW == 1) then

! STATUS = DftiGetValueDM( Desc_DM , CDFT_LOCAL_SIZE , SIZE_ldft)
! STATUS = DftiGetValueDM( Desc_DM , CDFT_LOCAL_NX , NX_IN)
! STATUS = DftiGetValueDM( Desc_DM , CDFT_LOCAL_OUT_NX , NX_OUT)
! STATUS = DftiGetValueDM( Desc_DM , CDFT_LOCAL_X_START , START_X)
! STATUS = DftiGetValueDM( Desc_DM , CDFT_LOCAL_OUT_X_START , START_X_OUT)

! call MPI_BCAST ( NX_IN , 1, MPI_INT , 0 , LC_comm_world , error_LC )
! call MPI_BCAST ( NX_OUT , 1, MPI_INT , 0 , LC_comm_world , error_LC )
! call MPI_BCAST ( SIZE_ldft , 1, MPI_INT , 0 , LC_comm_world , error_LC )

allocate( work_w(1,NX_IN) )
allocate( work_t(1,NX_OUT) )
allocate( work(SIZE_ldft) )

!la=1.d0
if ( dir == 1 ) then !FORWARD

	do i = 1, ns
	do j = 1, ns

work_w=0.d0
work_t=0.d0

		do iw= 1, lldwc
			liw=(iw-1)*ns
			work_w( 1, vw_ilc(iw) ) = la(i, liw+j)
		end do

		call zgsum2d(context, 'R', 'i-ring', 1, NX_IN, work_w, 1, myrow, 0)
		work(1:NX_IN)= work_w(1,1:NX_IN)



		if ( mycol == 0) then
			Status = DftiComputeForwardDM( Desc_DM, work )

			work_t(1:NX_OUT,1)= work(1:NX_OUT)
			call zgebs2d( context, "R", "i-ring", 1, NX_OUT, work_t, 1 )
		else
			call zgebr2d( context, "R", "i-ring", 1, NX_OUT, work_t, 1 , myrow, 0)
		end if


		do iw= 1, lldtc
			liw=(iw-1)*ns
 			lat(i, liw+j)=work_t( 1, vt_ilc(iw) )
		end do



	end do
	end do



else	if ( dir == -1 ) then !BACKWARD

		do i = 1, ns
		do j = 1, ns

			work_w=0.d0
			work_t=0.d0

			do iw= 1, lldtc
				liw=(iw-1)*ns
				work_t( 1, vt_ilc(iw) ) = lat(i, liw+j)
			end do


			call zgsum2d(context, 'R', 'i-ring', 1, NX_OUT, work_t, 1, myrow, 0)
			work(1:NX_OUT)= work_t(1,1:NX_OUT)



			if ( mycol == 0) then

				Status = DftiComputeBackwardDM( Desc_DM, work )

				work_w(1:NX_IN,1)= work(1:NX_IN)
				call zgebs2d( context, "R", "i-ring", 1, NX_IN, work_w, 1 )
			else
				call zgebr2d( context, "R", "i-ring", 1, NX_IN, work_w, 1 , myrow, 0)
			end if


			do iw= 1, lldwc
				liw=(iw-1)*ns
	 			la(i, liw+j)=work_w( 1, vw_ilc(iw) )
			end do


		end do
		end do

end if


!
! if ( dir == 1 ) then !FORWARD
!
! 	do i = 1, ns
! 	do j = 1, ns
!
! 		do iw= 1, lldwc
! 			liw=(iw-1)*ns
! 			work_w( vw_ilc(iw), 1 ) = la(i, liw+j)
! 		end do
!
! 		call zgsum2d(context, 'R', 'i-ring', NX_IN, 1, work_w, NX_IN, myrow, 0)
! 		work(1:NX_IN)= work_w(1:NX_IN,1)
!
! 		if ( mycol == 0) then
! 			Status = DftiComputeForwardDM( Desc_DM, work )
! 			work_t(1:NX_OUT,1)= work(1:NX_IN)
! 			call zgebs2d( context, "R", "i-ring", NX_OUT, 1, work_t, NX_OUT )
! 		else
! 			call zgebr2d( context, "R", "i-ring", NX_OUT, 1, work_t, NX_OUT , myrow, 0)
! 		end if
!
! 		do iw= 1, lldtc
! 			liw=(iw-1)*ns
!  			lat(i, liw+j)=work_t( vt_ilc(iw) , 1 )
! 		end do
!
! 	end do
! 	end do
!
! end if


deallocate( work , work_w , work_t )


end subroutine fourierDFwt
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Fourier transform w-t (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
