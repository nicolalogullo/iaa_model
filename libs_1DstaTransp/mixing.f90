!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Pulay mixing (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine mixing(context,nps,npw,wm,mem, gin,gout, desc, cc)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw, stat, mem
	double precision :: wm
	integer, dimension (:) :: desc
	double complex, dimension(mem+1,mem+1) :: P
	double complex, dimension(:,:,:) :: gin
	double complex, dimension(:,:,:) :: gout
	double precision, dimension(:) :: cc
	double complex, dimension(nps,nps) :: intg
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: n,i,j,k,iw,lw,lwp,liw,liwp
	integer :: nb,mb,pr,pc,lldr,lldc,lldwr,lldwc,numroc
	double complex :: zalpha
	integer, dimension (9) :: descw
	double complex, dimension(:,:), allocatable :: ltmp

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)

lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(ltmp(lldr,lldc))

ltmp=zero
P=zero



do i=1,mem
	do j=1,mem

		intg=zero
		do iw=1, lldwc
			liw=(iw-1)*nps+1
			ltmp(1:nps,liw:liw+nps-1)=Aimag(conjg(gin(i,1:nps,liw:liw+nps-1)&
				&-gout(i,1:nps,liw:liw+nps-1)))&
				&*Aimag(gin(j,1:nps,liw:liw+nps-1)&
				&-gout(j,1:nps,liw:liw+nps-1))
		end do

		call wint(context,nps,npw,wm,ltmp,desc,-1,-1,intg)	

		zalpha=zero
		do k=1,nps
			zalpha =zalpha + intg(k,k)
		end do

		P(i,j)= zalpha

	end do
end do
do i=1,mem
	P(mem+1,i)=-1
	P(i,mem+1)=-1
end do


!if(myrow==0.and.mycol==0) then
!do i=1, mem+1
!print*, Real(P(i,:))
!end do
!print*, 'FINITO'
!end if

call locinv(mem+1,P)

!if(myrow==0.and.mycol==0) then
!do i=1, mem+1
!print*, Real(P(i,:))
!end do
!print*, 'FINITO'
!end if

 cc=0.d0
do i=1,mem+1
	cc(i)=-real(P(i,mem+1))
end do

end subroutine mixing

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Pulay mixing (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
