!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!                State (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wstate(context,nps,npw,wm,stat,lgr,desc,rho)
        use mpi
        implicit none
!Useful numerical parameters
        double precision, parameter :: pi = dacos(-1.0d0)
! Pi greco
        double complex, parameter :: ci = (0.0d0,1.0d0)
! Imaginary unit
        double complex, parameter :: uno = (1.0d0,0.0d0)
! Complex one
        double complex, parameter :: zero = (0.0d0,0.0d0)
! Complex zero
!Input variables
        integer :: nps,npw, stat
        double precision :: wm
        integer, dimension (:) :: desc
        double complex, dimension(:,:) :: lgr
        double complex, dimension(nps,nps) :: intg, rho
!Varibles for grid query
        integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
        integer :: i,iw,lw,lwp,liw,liwp
        integer :: nb,mb,pr,pc,lldr,lldc,lldwr,lldwc,numroc
        double precision :: dalpha
        integer, dimension (9) :: descw

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

!mb = desc(5)
!nb = desc(6)
!n=nps*npw

!lldr=numroc(nps,mb,myrow,0,1)
!lldc=numroc(n,nb,mycol,0,nprocs)

lldwr=numroc(npw,1,myrow,0,1)
lldwc=numroc(npw,1,mycol,0,nprocs)

call descinit(descw,1,npw,1,1,0,0,context,lldwr,info)

intg=zero
call wint(context,nps,npw,wm,lgr,desc,-1,-1,intg)

rho=zero
rho = stat*intg

end subroutine wstate

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!               State (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
