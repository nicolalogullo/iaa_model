!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Spectral Function (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wspecfun(context,d,npx,npy,npw,lgr,specfun)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
!	character(*) :: filenameout
	integer :: d,npx,npy,npw
	double complex, dimension(:,:) :: lgr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: i,ix,iy,k,kkx,kky,iw,lw,lwp,liw,liwp,nps
	integer :: nb,mb,pr,pc,lldr,lldc,lldwr,lldwc,numroc
	double precision :: x,y,kx,ky
	integer, dimension (9) :: descw

	double precision, dimension(:,:) :: specfun
	double precision, dimension(:,:), allocatable :: sf
	double complex, dimension(:,:), allocatable :: expk,lgrk

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

!mb = desc(5)
!nb = desc(6)
!n=nps*npw

!lldr=numroc(nps,mb,myrow,0,1)
!lldc=numroc(n,nb,mycol,0,nprocs)

lldwr=numroc(npw,1,myrow,0,1)
lldwc=numroc(npw,1,mycol,0,nprocs)

call descinit(descw,1,npw,1,1,0,0,context,lldwr,info)

!if(myrow==0.and.mycol==0) then
!		open(unit=21,file=filenameout//'.dat',position='append',status='unknown')
!end if

if (d==1) then
	nps=npx
else if (d==2) then
	nps=npx*npy
end if

allocate(sf(nps,nps),expk(nps,nps),lgrk(nps,lldwc*nps))
specfun=0.0d0
expk=zero
!Fourier transform in momentum
if (d==1) then
	do k=1,npx
	do i=1,npx
		kx=-pi+2*pi*(k-1)/npx
		expk(k,i)=exp(ci*kx*i)/sqrt(1.d0*nps)
	end do
	end do
else if (d==2) then
	k=1
	do kky=1,npy
	do kkx=1,npx
	i=1
	kx=-pi+2*pi*(kkx-1)/npx
	ky=-pi+2*pi*(kky-1)/(npy*cos(pi/6.d0))

	do iy=1,npy
	do ix=1,npx

	if (mod(iy,2)==0) then	
		x=Floor(ix*5.d-1)*(1+2*sin(pi/6.d0))+(Ceiling(ix*5.d-1)-1)-sin(pi/6.d0)
	else if (mod(iy,2).ne.0) then
		x=Floor(ix*5.d-1)+(Ceiling(ix*5.d-1)-1)*(1+2*sin(pi/6.d0))
	end if
	y=(iy-1)*cos(pi/6.d0)

	expk(k,i)=exp(ci*(kx*x+ky*y))/sqrt(1.d0*nps)
	
	i=i+1

	end do
	end do

	k=k+1

	end do
	end do

end if


do lwp=1,lldwc
	liwp=(lwp-1)*nps+1
	lgrk(:,liwp:liwp+nps-1)=matmul(expk,matmul(lgr(:,liwp:liwp+nps-1),conjg(transpose(expk))))
end do


!Frequency part
do iw=1,npw
	call infog2l(1,iw,descw,1,nprocs,myrow,mycol,lw,lwp,rsrc,csrc)

	if (myrow==rsrc.and.mycol==csrc) then
		liwp=(lwp-1)*nps+1
		sf=0.d0
		do i=1, nps
			sf(i,i) = -Aimag(lgrk(i,liwp+i-1))/pi
		end do
		call dgebs2d(context,'A','i-ring',nps,nps,sf,nps)
	else
		call dgebr2d(context,'A','i-ring',nps,nps,sf,nps,rsrc,csrc)

	end if

	if(myrow==0.and.mycol==0) then
		do i=1,nps
			specfun(iw,i)=sf(i,i)
		end do
	end if

end do

!if(myrow==0.and.mycol==0) then
!close(21)
!end if

end subroutine wspecfun

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Spectral Function (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
