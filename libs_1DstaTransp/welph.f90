!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	electron-phonon self-energy (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine welph(context,nps,npw,wm,vw,v,lnegl,lnegg,desc,lnedl,lnedg,descld,lsl,lsg,lsr)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw
	double precision :: wm,g
	integer, dimension (:) :: vw,desc,descld
	double precision, dimension (:,:) :: v
	double complex, dimension(:,:) :: lnegl
	double complex, dimension(:,:) :: lnegg
	double complex, dimension(:,:) :: lnedl
	double complex, dimension(:,:) :: lnedg


!Input/Output variables
	double complex, dimension(:,:) :: lsl
	double complex, dimension(:,:) :: lsg
	double complex, dimension(:,:) :: lsr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: n,i,j,lw,liw,mb,nb,lldc,lldr,lldwc,lldwr,numroc
	double precision :: om,tm,dt,t,dw,tp
	double complex :: zalpha
	double complex, dimension(nps,nps) :: intg, tmp,pol
	double complex, dimension(1,1) :: intd
	double complex, dimension(:,:), allocatable :: ltgl,ltgg,ltdl,ltdg,ltmp


call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)

lldwr=numroc(1,1,myrow,0,1)
lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(ltgl(lldr,lldc),ltgg(lldr,lldc),ltdl(lldwr,lldwc),ltdg(lldwr,lldwc),ltmp(lldr,lldc))

call fourierwt(context,nps,npw,wm,1,lnegl,desc,ltgl)
call fourierwt(context,nps,npw,wm,1,lnegg,desc,ltgg)

call fourierwt(context,1,npw,wm,1,lnedl,descld,ltdl)
call fourierwt(context,1,npw,wm,1,lnedg,descld,ltdg)

dw=2.d0*wm/(npw-1)
tm=pi*(npw-1)/(2.d0*wm)
dt=pi/wm

!FOCK
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!LESSER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
tmp=zero
do lw=1, lldwc
	liw=(lw-1)*nps+1
	tmp=matmul(v,matmul(ltgl(:,liw:liw+nps-1),v))
	do i=1,nps
	do j=1,nps
		lsl(i,liw+j-1)=ci*tmp(i,j)*(ltdl(1,lw)-conjg(ltdg(1,lw)))
	end do 
	end do
end do

!do lw=1, lldwc
!	liw=(lw-1)*nps+1
!	do i=1,nps
!	do j=1,nps
!		lsl(i,liw+j-1)= ci*ltgl(i,liw+j-1)*(ltdl(i,liw+j-1)-conjg(ltdg(i,liw+j-1)))*(g**2)
!	end do
!	end do
!end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!GREATER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
tmp=zero
do lw=1, lldwc
	liw=(lw-1)*nps+1
	tmp=matmul(v,matmul(ltgg(:,liw:liw+nps-1),v))
	do i=1,nps
	do j=1,nps
		lsg(i,liw+j-1)=ci*tmp(i,j)*(ltdg(1,lw)-conjg(ltdl(1,lw)))
	end do 
	end do
end do

!do lw=1, lldwc
!	liw=(lw-1)*nps+1
!	do i=1,nps
!	do j=1,nps
!		lsg(i,liw+j-1)= ci*ltgg(i,liw+j-1)*(ltdg(i,liw+j-1)-conjg(ltdl(i,liw+j-1)))*(g**2)
!	end do
!	end do
!end do


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!RETARDED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


ltmp=zero
do lw=1, lldwc
	liw=(lw-1)*nps+1
	t=-tm+(vw(lw)-1)*dt
!Self energy
	if (t.ge.0.d0.and.t<dt) then
		ltmp(:,liw:liw+nps-1)=0.5d0*(lsg(:,liw:liw+nps-1)-lsl(:,liw:liw+nps-1))
	else if (t>0.d0) then
		ltmp(:,liw:liw+nps-1)=1.0d0*(lsg(:,liw:liw+nps-1)-lsl(:,liw:liw+nps-1))
	end if
end do


!HARTREE (La HF É solo a tempi uguali e quindi solo se tau=0)
!!!!!!!!!!!!!!!!!!!!FORSE QUI CI VA IL 1/dt!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!FORSE QUI CI VA IL 1/dt!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!FORSE QUI CI VA IL 1/dt!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!FORSE QUI CI VA IL 1/dt!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!FORSE QUI CI VA IL 1/dt!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!call wint(context,nps,npw,wm,lnegl,desc,intg)
!call wint(context,1,npw,wm,ld0r,descd,intd)

!do lw=1,lldwc
!	liw=(lw-1)*nps+1
!	t=-tm+(vw(lw)-1)*dt
!	if (t.ge.0.d0.and.t<dt) then
!		do i=1,size(lds)
!		do j=1, size(lds)
!			ltmp(lds(i),liw+lds(i)-1)=ltmp(lds(i),liw+lds(i)-1)&
!				&+intd(1,1)*Aimag(intg(lds(j),lds(j)))*(2*pi)**2
!		end do
!		end do
!	end if
!end do

!((-1)**(lds(i)+lds(j)))*

!  self energy in frequency domain 
lsr=zero
call fourierwt(context,nps,npw,wm,-1,ltmp,desc,lsr)
!lsr = ltgl

ltmp=lsl
lsl=zero
call fourierwt(context,nps,npw,wm,-1,ltmp,desc,lsl)

ltmp=lsg
lsg=zero
call fourierwt(context,nps,npw,wm,-1,ltmp,desc,lsg)

end subroutine welph
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	electron-phonon self-energy (Start)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
