!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	HF self-energy (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine whf(context,nps,npw,wm,v,lnegl,desc,lsr)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw
	double precision :: wm
	integer, dimension (:) :: desc
	double precision, dimension (:,:) :: v
	double complex, dimension(:,:) :: lnegl
!Input/Output variables
	double complex, dimension(:,:) :: lsr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: i,j,lw,liw,lldwc,numroc
	double precision :: dalpha
	double complex, dimension(nps,nps) :: intg

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

lldwc=numroc(npw,1,mycol,0,nprocs)

call wint(context,nps,npw,wm,lnegl,desc,-1,-1,intg)

do lw=1,lldwc
	liw=(lw-1)*nps+1
	do i=1,nps
	do j=1,nps
		lsr(i,liw+i-1)=lsr(i,liw+i-1)+v(i,j)*Aimag(intg(j,j))
	end do
	end do
end do


end subroutine whf
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	HF self-energy (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
