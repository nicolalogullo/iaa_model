!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Chemical potential (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine ff(n,x,fvec,fbs,iflag)
        use parameters
        implicit none
        integer :: n
        double precision, dimension(n) :: x
        double precision, dimension(n) :: fvec,fbs
        double precision, dimension(nw) :: om
        double precision :: dist,mu,nf,enf
        !double precision :: beta
        double precision :: num,en
        integer :: i,iflag
        !double precision :: pi=acos(-1.0)
        double precision :: dw

        if (iflag==1) then
                dw=2*wm/(nw-1)

                do i=1, size(om)
                        om(i)=-wm+(i-1)*dw
                end do

                num = 0.d0
                en = 0.d0
                do i=1, size(om)
                        dist=1/(exp(x(2)*(om(i)-x(1)))+1)
                        num=num+dw*dist/(2.d0*pi)
                        en=en+dw*om(i)*dist/(2.d0*pi)
                end do
                fvec(1)=num-fbs(1)
                fvec(2)=en-fbs(2)

        end if

        if (abs(fvec(1))<1.d-25.and.abs(fvec(2))<1.d-25) then
                iflag=-1
        end if
end subroutine ff

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Chemical potential (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
