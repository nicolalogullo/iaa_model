
!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	Second Born self-energy (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wsndbDFT(context, desc, Desc_DM_WT,ns,nw, NX_IN, NX_OUT, &
	 									& wm,vw, vw_ilc, vt_ilc, v,&
										& lnegl,lnegg,lsl,lsg,lsr)
	use mpi
	use MKL_CDFT
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: ns,nw, NX_IN, NX_OUT
	type(DFTI_DESCRIPTOR_DM), POINTER :: Desc_DM_WT
	double precision :: wm
	integer, dimension (:) :: vw, vw_ilc, vt_ilc, desc
	double precision, dimension (:,:) :: v
	double complex, dimension(:,:) :: lnegl
	double complex, dimension(:,:) :: lnegg
!Input/Output variables
	double complex, dimension(:,:) :: lsl
	double complex, dimension(:,:) :: lsg
	double complex, dimension(:,:) :: lsr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: n,i,j,lw,liw
	integer :: mb,nb,numroc, lldc,lldr,lldwc, lldtc, lldc_t
	double precision :: t,dt,tm
	double complex :: zalpha
	double complex, dimension(ns,ns) :: intg, tmp,pol
	double complex, dimension(:,:), allocatable :: ltmpl, ltmpg, ltmp
	double complex, dimension(:,:), allocatable :: lsl_t, lsg_t, lsr_t

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=ns*nw


lldr=numroc(ns,mb,myrow,0,1)

lldwc=numroc(NX_IN,1,mycol,0,npcol)
lldc = lldr*lldwc

lldtc=numroc(NX_OUT,1,mycol,0,npcol)
lldc_t = lldr*lldtc

! allocate(ltmpl(lldr,lldc),ltmpg(lldr,lldc),ltmp(lldr,lldc))
allocate(ltmp(lldr,lldc))
allocate(ltmpl(lldr,lldc_t),ltmpg(lldr,lldc_t) )
allocate(lsl_t(lldr,lldc_t),lsg_t(lldr,lldc_t),lsr_t(lldr,lldc_t))


ltmpl=zero
ltmpg=zero
ltmp=zero

call fourierDFwt( context , Desc_DM_WT , nw, NX_IN, NX_OUT , vw_ilc , vt_ilc, ns , lnegl , 1 , ltmpl )
call fourierDFwt( context , Desc_DM_WT , nw, NX_IN, NX_OUT , vw_ilc , vt_ilc, ns , lnegg , 1 , ltmpg )

! call fourierwt(context,ns,nw,wm,1,lnegl,desc,ltmpl)
! call fourierwt(context,ns,nw,wm,1,lnegg,desc,ltmpg)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!LESSER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

do lw=1, lldwc
	liw=(lw-1)*ns+1
!Polarization diagram
	pol(:,:)=ltmpl(:,liw:liw+ns-1)*conjg(ltmpg(:,liw:liw+ns-1))
	tmp=matmul(v,matmul(pol,v))
!Self energy
	lsl_t(:,liw:liw+ns-1)=lsl_t(:,liw:liw+ns-1)-ltmpl(:,liw:liw+ns-1)*tmp
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!GREATER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

do lw=1, lldwc
	liw=(lw-1)*ns+1
!Polarization diagram
	pol(:,:)=ltmpg(:,liw:liw+ns-1)*conjg(ltmpl(:,liw:liw+ns-1))
	tmp=matmul(v,matmul(pol,v))
!Self energy
	lsg_t(:,liw:liw+ns-1)=lsg_t(:,liw:liw+ns-1)-ltmpg(:,liw:liw+ns-1)*tmp
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!RETARDED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
ltmp=zero

tm=pi*(nw-1)/(2.d0*wm)
dt=pi/wm

do lw=1, lldwc
	liw=(lw-1)*ns+1
	t=-tm+(vw(lw)-1)*dt
!Self energy
	if (t.ge.0.d0.and.t<dt) then
		lsr_t(:,liw:liw+ns-1)=0.5d0*(lsg(:,liw:liw+ns-1)-lsl(:,liw:liw+ns-1))
	else if (t>0.d0) then
		lsr_t(:,liw:liw+ns-1)=(lsg(:,liw:liw+ns-1)-lsl(:,liw:liw+ns-1))
	end if
end do



ltmp=zero
call fourierDFwt( context , Desc_DM_WT , nw , NX_IN, NX_OUT , vw_ilc , vt_ilc, ns , ltmp , -1 , lsr_t )
lsr = ltmp +lsr

ltmp=zero
call fourierDFwt( context , Desc_DM_WT , nw , NX_IN, NX_OUT , vw_ilc , vt_ilc, ns , ltmp , -1 , lsl_t )
lsl = ltmp+lsl

ltmp=zero
call fourierDFwt( context , Desc_DM_WT , nw , NX_IN, NX_OUT , vw_ilc , vt_ilc, ns , ltmp , -1 , lsg_t )
lsg = ltmp+lsg


end subroutine wsndbDFT
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Second Born self-energy (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
