!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Global 2 local (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wg2l(context,npw,iw,liw,rsrc,csrc)
	implicit none
!Input variables
	integer :: npw,iw
!Output variables
	integer :: liw,rsrc,csrc
!Varibles for grid query
	integer :: context,nprow,npcol,nprocs,myrow,mycol

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

liw=Floor((iw-1.d0)/nprocs)+1
csrc=Floor((iw-(liw-1)*nprocs-1.d0)/nprow)
rsrc=iw-(liw-1)*nprocs-csrc*nprow-1

end subroutine wg2l
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Global 2 local (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
