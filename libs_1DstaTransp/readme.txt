-)“ff.f90”, “ff1.f90” use MinPack
“fourierDFwt.f90”, “fourierDFwtBIG.f90”, “wgrid2D_lc.f90”, “wsndbDFT.f90”, “wtgids.f90” use MKL_CDFT
if you don’t need/use these libraries don’t include them

-) Modifications/additions:
[(.)) 
CP*.f90 -> compute and print various physical quantities form environment parameters and green functions. 
The environment self energies are recomputed from scratch, in a optimized code I don’t recommend the use of this routines

CPconds.f90		->2 terminal + small losses configuration. Input a set of environment temperatures and chemical potential, gives non-interacting linear conductances
CPcurrents.f90		->currents in a 2 terminal set-up
CPcurrents3.f90		->currents in a 2 terminal + small losses set-up
CPnegf.f90		->density of states and spectral function if needed


[(.))
integration and gathering of data is now done with dgsum when possible, most notably in “wint.f90”. To avoid confusion  “fourierwt.f90” and “fourierwt(good).f90” still work as in their previous version, while “fourierwtSUM.f90“ uses directly dgsum and was faster in all my tests.

[(.))
The “eta-regularization” is now included physically as small losses in the system. It’s effects can be computed with the usual routines, but to create the self energies and to compute the currents I strongly suggest to use “*Diss.f90” routines which are much faster and use way less memory.
“_name_Diss.f90” routines are the correspectives of “_name_.f90” routines, but work only if the embedding self energy of interest is diagonal.

[(.))
MKL cluster DFT is implemented in some codes. Since it poses some restrictions on parallelization it was first implemented in a Scalpack 2D grid it thus requires different routines for the creation of frequency and temporal grid. 
Refer to the code and the routines involved (“fourierDFwt.f90”, “fourierDFwtBIG.f90”, “wgrid2D_lc.f90”, “wsndbDFT.f90”, “wtgids.f90”) for further reference.

“wgrid_lc.f90” should be ignored for the moment. 

