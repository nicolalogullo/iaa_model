!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Prints w (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wgrid_lc(my_comm_world, nprocs, my_rank, displ, lclength, vw_c, vw_lc, vw_ilc)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
!Input variables
	integer :: npw, my_comm_world
!Input/Output variables
integer, dimension(:) :: vw_c, vw_lc, vw_ilc
integer, dimension(:) :: displ, lclength

!Dummy variables
	integer :: iw,liw, liiw, pc, j_rank
	integer :: nprocs, my_rank, error
	!integer, dimension(:), allocatable :: displ, lclength

!call mpi_comm_size	( my_comm_world , nprocs , error)
!call mpi_comm_rank	( my_comm_world , my_rank, error)
npw=size(vw_c)

!allocate(displ(nprocs), lclength(nprocs))

liiw=0
do j_rank=1, nprocs
	liw=0
	do iw=1, npw
		pc=Mod(Floor(Real(iw-1)),nprocs)
		if(j_rank==pc+1) then
			liw=liw+1
		end if
	end do
	lclength(j_rank)=liw
	displ(j_rank)=liiw

	liiw=liiw+liw
end do


!displ=0
liw=0
do iw=1, npw
	pc=Mod(Floor(Real(iw-1)),nprocs)
	if(my_rank==pc) then
		liw=liw+1
		! vw_lc(liw)=vw_c(iw)
		vw_ilc(liw)=iw
	end if
end do


call MPI_Scatterv(vw_c,lclength,displ,MPI_INT, vw_lc, npw, MPI_INT, 0, my_comm_world, error)
! !call MPI_Scatter(vw_c, liw, MPI_INT, vw_lc, liw, MPI_INT, 0, my_comm_world, error)

!
! if (my_rank==0) then
! 	print*, "lclength", lclength
! 	print*, "displ", displ
! end if
!





end subroutine wgrid_lc
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Prints w (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
