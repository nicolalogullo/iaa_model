!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		INBEDDING self-energy (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine winbse(context,nps,npw,wm,vw,g,lnegl,lnegg,lnegr,desc,lsl,lsg,lsr,descld)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw
	double precision :: wm 
	integer, dimension (:) :: vw,desc,descld
	double precision, dimension (:) :: g
	double complex, dimension(:,:) :: lnegl
	double complex, dimension(:,:) :: lnegg
	double complex, dimension(:,:) :: lnegr
!Input/Output variables
	double complex, dimension(:,:) :: lsl
	double complex, dimension(:,:) :: lsg
	double complex, dimension(:,:) :: lsr

!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: n,i,j,lw,liw,mb,nb,lldc,lldr,lldwc,lldwr, numroc
	double precision :: t,dt,tm
	double complex :: zalpha
	double complex, dimension(nps,nps) :: intg, tmpa, cv
	double complex, dimension(1,1) :: tmp
	double complex, dimension(:,:), allocatable :: ltmpr, ltmpl, ltmpg, ltmp,lpa, lpr, lpl, lpg, lwr, lwl, lwg 

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)

lldwr=numroc(1,1,myrow,0,1)
lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(ltmpr(lldr,lldc),ltmpl(lldr,lldc),ltmpg(lldr,lldc),ltmp(lldwr,lldwc))
allocate(lwr(lldr,lldc),lwl(lldr,lldc),lwg(lldr,lldc))




!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!RETARDED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

lsr=zero
tmp=zero
do lw=1, lldwc
	liw=(lw-1)*nps+1
	tmp=0.5d0*matmul(reshape(g,(/1,size(g)/)), matmul(lnegr(1:nps,liw:liw+nps-1),reshape(g,(/size(g),1/)) ) )
	lsr(1,lw) = tmp(1,1)
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!LESSER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

lsl=zero
tmp=zero
do lw=1, lldwc
	liw=(lw-1)*nps+1
	tmp=matmul(reshape(g,(/1,size(g)/)), matmul(lnegl(1:nps,liw:liw+nps-1),reshape(g,(/size(g),1/)) ) )
	lsl(1,lw) = tmp(1,1)
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!GREATER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

lsg=zero
tmp=zero
do lw=1, lldwc
	liw=(lw-1)*nps+1
	tmp=matmul(reshape(g,(/1,size(g)/)), matmul(lnegg(1:nps,liw:liw+nps-1), reshape(g,(/size(g),1/))) )
	lsg(1,lw) = tmp(1,1)
end do



end subroutine winbse
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		INBEDDING self-energy (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

