

!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		mass Current (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wmcond(context,nps,npw,wm,vw,bt,mu, V, stat, gamm,lnegr,desc,outrow,outcol,mcond)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw, stat, outrow,outcol
	double precision :: wm, bt, mu, V
	integer, dimension (:) :: desc
	double complex, dimension(:,:) :: lnegr
	double precision, dimension(:,:) :: gamm
	integer, dimension(:) :: vw

!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: n,i,j,lw,liw,mb,nb,lldc,lldr,lldwc,numroc
	double precision :: om,mcond
	double complex, dimension(nps,nps) :: intg
	double complex, dimension(:,:), allocatable :: ltmp

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldwc = size(vw)
lldc = lldwc*lldr
! lldc=numroc(n,nb,mycol,0,nprocs)
! lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(ltmp(lldr,lldc))

ltmp=zero


do lw=1, lldwc
	liw=(lw-1)*nps+1
	om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)
	ltmp(1:nps,liw:liw+nps-1) = ( bt/((exp(bt*(om-mu-V))-1.d0*stat)*(1-1.d0*stat*exp(-bt*(om-mu-V)))) ) *ci* matmul(gamm(1:nps,liw:liw+nps-1),lnegr(1:nps,liw:liw+nps-1)-transpose(conjg(lnegr(1:nps,liw:liw+nps-1))))

end do




call wint(context,nps,npw,wm,ltmp,desc,outrow,outcol,intg)

	mcond=0.0d0
	do i=1,nps
		 mcond = mcond + 2*Real(intg(i,i))
	end do


end subroutine wmcond
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		mass current (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
