!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	Energy Current in the leads (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wecurrldsDiss(context,nps,npw,wm,vw,lnegl,lnegr,lsembl,lsembr,desc,outrow,outcol,ecur)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw, outrow, outcol
  double precision :: wm
	integer, dimension (:) :: vw
	integer, dimension (:) :: desc
	double complex, dimension(:,:) :: lnegl,lnegr,lsembl,lsembr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: n,i,j,lw,liw,mb,nb,lldc,lldr,lldwc,numroc
	double precision :: w,dw
	double precision, dimension(nps) :: ecur
	double complex, dimension(nps,nps) :: intg
	double complex, dimension(:,:), allocatable :: ltmp

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldwc=size(vw)
lldc=lldwc*lldr
! lldc=numroc(n,nb,mycol,0,nprocs)
! lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(ltmp(lldr,lldc))

ltmp=zero

dw=2*wm/(npw-1)

do lw=1, lldwc
	liw=(lw-1)*nps+1
        w=-wm+(vw(lw)-1)*dw
	ltmp(1:nps,liw:liw+nps-1) = w*(matmul(lnegl(1:nps,liw:liw+nps-1), transpose(conjg(lsembr(1:nps,liw:liw+nps-1))))&
		& + matmul(lnegr(1:nps,liw:liw+nps-1), lsembl(1:nps,liw:liw+nps-1)))
end do

call wint(context,nps,npw,wm,ltmp,desc,outrow,outcol,intg)

	ecur=0.0d0
	do i=1,nps
		 ecur(i) = 2*Real(intg(i,i))
	end do


end subroutine wecurrldsDiss
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Energy Current in the leads (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
