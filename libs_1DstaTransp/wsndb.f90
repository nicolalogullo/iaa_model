
!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	Second Born self-energy (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wsndb(context,nps,npw,wm,vw,v,lnegl,lnegg,desc,lsl,lsg,lsr)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw
	double precision :: wm
	integer, dimension (:) :: vw,desc
	double precision, dimension (:,:) :: v
	double complex, dimension(:,:) :: lnegl
	double complex, dimension(:,:) :: lnegg
!Input/Output variables
	double complex, dimension(:,:) :: lsl
	double complex, dimension(:,:) :: lsg
	double complex, dimension(:,:) :: lsr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: n,i,j,lw,liw,mb,nb,lldc,lldr,lldwc,numroc
	double precision :: t,dt,tm
	double complex :: zalpha
	double complex, dimension(nps,nps) :: intg, tmp,pol
	double complex, dimension(:,:), allocatable :: ltmpl, ltmpg, ltmp

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)

lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(ltmpl(lldr,lldc),ltmpg(lldr,lldc),ltmp(lldr,lldc))
ltmpl=zero
ltmpg=zero
ltmp=zero
call fourierwt(context,nps,npw,wm,1,lnegl,desc,ltmpl)
call fourierwt(context,nps,npw,wm,1,lnegg,desc,ltmpg)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!LESSER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

do lw=1, lldwc
	liw=(lw-1)*nps+1
!Polarization diagram
	pol(:,:)=ltmpl(:,liw:liw+nps-1)*conjg(ltmpg(:,liw:liw+nps-1))
	tmp=matmul(v,matmul(pol,v))
!Self energy
	lsl(:,liw:liw+nps-1)=lsl(:,liw:liw+nps-1)-ltmpl(:,liw:liw+nps-1)*tmp
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!GREATER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

do lw=1, lldwc
	liw=(lw-1)*nps+1
!Polarization diagram
	pol(:,:)=ltmpg(:,liw:liw+nps-1)*conjg(ltmpl(:,liw:liw+nps-1))
	tmp=matmul(v,matmul(pol,v))
!Self energy
	lsg(:,liw:liw+nps-1)=lsg(:,liw:liw+nps-1)-ltmpg(:,liw:liw+nps-1)*tmp
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!RETARDED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
ltmp=zero

tm=pi*(npw-1)/(2.d0*wm)
dt=pi/wm

do lw=1, lldwc
	liw=(lw-1)*nps+1
	t=-tm+(vw(lw)-1)*dt
!Self energy
	if (t.ge.0.d0.and.t<dt) then
		ltmp(:,liw:liw+nps-1)=0.5d0*(lsg(:,liw:liw+nps-1)-lsl(:,liw:liw+nps-1))
	else if (t>0.d0) then
		ltmp(:,liw:liw+nps-1)=(lsg(:,liw:liw+nps-1)-lsl(:,liw:liw+nps-1))
	end if
end do

ltmpl=zero
call fourierwt(context,nps,npw,wm,-1,ltmp,desc,ltmpl)
lsr = lsr + ltmpl

ltmp=lsl
lsl=zero
call fourierwt(context,nps,npw,wm,-1,ltmp,desc,lsl)

ltmp=lsg
lsg=zero
call fourierwt(context,nps,npw,wm,-1,ltmp,desc,lsg)

end subroutine wsndb
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Second Born self-energy (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
