!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!        Diagonalization (Start)
!Info: Diagonalizes a symmetric matrix distributed
!	in a grid of processes with the subroutine
!			PDSYEV
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine diagsy(nps,a,w,s,prnt,suffix)
	use mpi
	implicit none
!Input variables
	character(*) :: suffix
	integer :: nps,prnt
	double precision, dimension(:,:) :: a
!Input/Output variables
	double precision, dimension(:) :: w
	double precision, dimension(:,:) :: s
!Dummy variables
	double precision :: dalpha
	integer :: i,j
	integer :: info1,info2,dwork,diwork
	double precision, dimension(:), allocatable :: work
	integer, dimension(:), allocatable :: iwork


s=a
w=0.d0

allocate (work(1),iwork(1))
work=0.d0
iwork=0

call dsyevd('V','U',nps,s,nps,w,work,-1,iwork,-1,info1)

dwork=max(1,int(work(1)))
diwork=max(1,int(iwork(1)))

s=a
deallocate(work,iwork)



if (info1==0) then
	w=0.0d0
	allocate(work(dwork),iwork(diwork))
	call dsyevd('V','U',nps,s,nps,w,work,dwork,iwork,diwork,info2)
	deallocate(work)
else if (info1/=0) then
	write(*,*) 'ERROR 1, info1=  ', info1
        stop
end if

if (info2/=0) then
	write(*,*) 'ERROR 2, info2= ', info2
	stop
end if

!Print eigenvalues
if (prnt==1) then 
	open (unit=22,file='eval'//trim(suffix)//'.dat', status='unknown')
	do i=1, nps
		write(22,*) w(i)
	end do
        close(22)

!Print eigenvectors
open(unit=23,file='evec'//trim(suffix)//'.dat',status='unknown')
	do i=1,nps
	do j=1,nps
		write(23,*) s(i,j)
	end do
	end do
close(23)
end if


end subroutine diagsy

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!        Diagonalization (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
