!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Free Green's (Start)
!	g0l(x,xp)=ci<b^{dag}_{ip}(tp) b_{i}(t)>_0
!	g0g(x,xp)=-ci<b_{i}(t) b^{dag}_{ip}(tp)>_0
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wfreeg3(context,nps,npw,wm,vw,w,s,beta,stat,lg0r,lg0l,lg0g,lsr,lsl,lsg,ham,neta,desc)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary Unit
	double complex, parameter :: zero = (0.0d0,0.0d0)			! Complex zero
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
!Input variables
	integer :: nps,npw,stat
	double precision :: wm,beta, neta
	double precision, dimension(:,:) :: s, ham
	integer, dimension(:) :: vw
	double precision, dimension(:) :: w
!Input/Output variables
	integer, dimension(:) :: desc
	double complex, dimension(:,:) :: lg0r
	double complex, dimension(:,:) :: lg0l
	double complex, dimension(:,:) :: lg0g
	double complex, dimension(:,:) :: lsr,lsl,lsg
!Variables for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: n,i,ip,j,liw,lw
	integer :: nb,mb,pr,pc,lldr,lldc,lldwc,numroc
	double precision :: dalpha,om, t, tm, dt
	double complex :: zalpha
	double complex, dimension(:,:), allocatable :: lg0tmp, Id

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)

lldwc=size(vw)
lldc=lldr*lldwc
! lldc=numroc(n,nb,mycol,0,nprocs)
! lldwc=numroc(npw,1,mycol,0,nprocs)


allocate(lg0tmp(lldr,lldc))
allocate(Id(nps,nps))

Id=0.d0
do i=1,nps
	Id(i,i)=1
end do



!lg0r=zero


!!!!!!!!!!!!!!!!!!!!!!!!!!!!Retarded Green's function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do lw=1, lldwc
!+++++++++++++++++++++++++++R Green's+++++++++++++++++++++++++++
	liw = (lw-1)*nps+1
	om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)
	! !lg0r(1:nps,liw:liw+nps-1) = matmul(s,matmul(lg0tmp(1:nps,liw:liw+nps-1),transpose(s)))
	! lg0r(1:nps,liw:liw+nps-1)= om*Id(1:nps,1:nps) - ham(1:nps,1:nps) -lsr(1:nps,liw:liw+nps-1)
	! call locinv(nps,lg0r(1:nps,liw:liw+nps-1))

	!+++++++++++++++++++++++++++L/G Green's+++++++++++++++++++++++++++
	lg0tmp(1:nps,1:nps) = conjg(transpose(lg0r(1:nps,liw:liw+nps-1)))

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!Lesser Green's function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	lg0l(1:nps,liw:liw+nps-1) = 1.0d0*matmul(lg0r(1:nps,liw:liw+nps-1),matmul(lsl(1:nps,liw:liw+nps-1),lg0tmp(1:nps,1:nps)))

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!Greater Green's function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	lg0g(1:nps,liw:liw+nps-1) = 1.0d0*matmul(lg0r(1:nps,liw:liw+nps-1),matmul(lsg(1:nps,liw:liw+nps-1),lg0tmp(1:nps,1:nps)))
	!--------------------------------------------------------------------

end do

end subroutine wfreeg3
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Free Green's (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
