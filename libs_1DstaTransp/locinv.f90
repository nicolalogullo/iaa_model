!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	Local Inversion (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine locinv(n,a)
	implicit none
!Useful numerical parameters
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
!Input variables
	integer :: n
	double complex :: delta
	double complex, dimension(:), allocatable :: work
	integer, dimension(:), allocatable :: ipiv
!Input/Output variables
	double complex, dimension(:,:) :: a
!Dummy variables
	integer :: i,j,tt,ttp,lit,litp
	integer :: info1,info2,dwork
	double precision :: dalpha


allocate(ipiv(n))
allocate (work(1))

call zgetri(n,a,n,ipiv,work,-1,info1)

dwork=max(1,int(work(1)))


deallocate(work)

if (info1==0) then
	allocate(work(dwork))

	call zgetrf(n,n,a,n,ipiv,info2)

	call zgetri(n,a,n,ipiv,work,dwork,info2)
	
	deallocate(work)

else if (info1/=0.0d0) then
		write(*,*) 'ERROR in loc inversion', info1
		stop
end if
		
if (info2/=0) then
	write(*,*) 'ERROR 2 in loc inversion', info2
	stop
end if



end subroutine locinv
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Local Inversion (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
