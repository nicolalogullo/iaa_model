!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Inbedding (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine winb(context,nps,npw,wm,lg0r,lg0l,lg0g,lsr,lsl,lsg, desc,lnegr,lnegl,lnegg)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw
	double precision :: wm
	integer, dimension (:) :: desc
	double complex, dimension(:,:) :: lg0r,lg0l,lg0g,lsr,lsl,lsg
!Output variables
	double complex, dimension(:,:) ::lnegr, lnegl,lnegg
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: n,i,j,lw,liw,mb,nb,lldc,lldr,lldwc,numroc
	double precision :: ecur
	double complex, dimension(nps,nps) :: intg
	double complex, dimension(:,:), allocatable :: ltmp,ltmp1,ltmp2

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)

lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(ltmp(lldr,lldc),ltmp1(lldr,lldc),ltmp2(lldr,lldc))

ltmp=zero
ltmp1=zero

do lw=1, lldwc
	liw=(lw-1)*nps+1
	lnegr(1:nps,liw:liw+nps-1)= lg0r(1:nps,liw:liw+nps-1)&
		&+matmul(lg0r(1:nps,liw:liw+nps-1),matmul(lsr(1:nps,liw:liw+nps-1),lg0r(1:nps,liw:liw+nps-1)))

end do

do lw=1, lldwc
	liw=(lw-1)*nps+1
	ltmp(1:nps,liw:liw+nps-1) = matmul(lsl(1:nps,liw:liw+nps-1), transpose(conjg(lg0r(1:nps,liw:liw+nps-1))))&
		& + matmul(lsr(1:nps,liw:liw+nps-1), lg0l(1:nps,liw:liw+nps-1))

	ltmp1(1:nps,liw:liw+nps-1) = matmul(transpose(conjg(lsr(1:nps,liw:liw+nps-1))),&
		&transpose(conjg(lg0r(1:nps,liw:liw+nps-1))))

	lnegl(1:nps,liw:liw+nps-1) = lg0l(1:nps,liw:liw+nps-1)&
		&+matmul(lg0r(1:nps,liw:liw+nps-1),ltmp(1:nps,liw:liw+nps-1))&
		&+matmul(lg0l(1:nps,liw:liw+nps-1),ltmp1(1:nps,liw:liw+nps-1))
end do

ltmp=zero
ltmp1=zero

do lw=1, lldwc
	liw=(lw-1)*nps+1
	ltmp(1:nps,liw:liw+nps-1) = matmul(lsg(1:nps,liw:liw+nps-1), transpose(conjg(lg0r(1:nps,liw:liw+nps-1))))&
		& + matmul(lsr(1:nps,liw:liw+nps-1), lg0g(1:nps,liw:liw+nps-1))

	ltmp1(1:nps,liw:liw+nps-1) = matmul(transpose(conjg(lsr(1:nps,liw:liw+nps-1))),&
		&transpose(conjg(lg0r(1:nps,liw:liw+nps-1))))

	lnegg(1:nps,liw:liw+nps-1) = lg0g(1:nps,liw:liw+nps-1)&
		&+matmul(lg0r(1:nps,liw:liw+nps-1),ltmp(1:nps,liw:liw+nps-1))&
		&+matmul(lg0g(1:nps,liw:liw+nps-1),ltmp1(1:nps,liw:liw+nps-1))
end do


!allocate(ltmp(lldr,lldc),ltmp1(lldr,lldc),ltmp2(lldr,lldc))

!ltmp=zero
!ltmp1=zero
!ltmp2=zero


!do lw=1, lldwc
!	liw=(lw-1)*nps+1
!	lnegr(1:nps,liw:liw+nps-1)= lg0r(1:nps,liw:liw+nps-1)&
!		&+matmul(lg0r(1:nps,liw:liw+nps-1),matmul(lsr(1:nps,liw:liw+nps-1),lg0r(1:nps,liw:liw+nps-1)))

!end do

!do lw=1, lldwc
!	liw=(lw-1)*nps+1
!	ltmp(1:nps,liw:liw+nps-1) = matmul(lsl(1:nps,liw:liw+nps-1), transpose(conjg(lg0r(1:nps,liw:liw+nps-1))))
!			
!	ltmp1(1:nps,liw:liw+nps-1)= matmul( lsr(1:nps,liw:liw+nps-1), lg0l(1:nps,liw:liw+nps-1))

!	ltmp2(1:nps,liw:liw+nps-1) = matmul(transpose(conjg(lsr(1:nps,liw:liw+nps-1))),&
!		&transpose(conjg(lg0r(1:nps,liw:liw+nps-1))))

!	lnegl(1:nps,liw:liw+nps-1) = lg0l(1:nps,liw:liw+nps-1)&
!		&+matmul(lg0r(1:nps,liw:liw+nps-1),ltmp(1:nps,liw:liw+nps-1))&
!		&+matmul(lg0r(1:nps,liw:liw+nps-1),ltmp1(1:nps,liw:liw+nps-1))&
!		&+matmul(lg0l(1:nps,liw:liw+nps-1),ltmp2(1:nps,liw:liw+nps-1))
!end do


!ltmp=zero
!ltmp1=zero
!ltmp2=zero

!do lw=1, lldwc
!	liw=(lw-1)*nps+1
!	ltmp(1:nps,liw:liw+nps-1) = matmul(lsg(1:nps,liw:liw+nps-1), transpose(conjg(lg0r(1:nps,liw:liw+nps-1))))
!			
!	ltmp1(1:nps,liw:liw+nps-1)= matmul( lsr(1:nps,liw:liw+nps-1), lg0g(1:nps,liw:liw+nps-1))

!	ltmp2(1:nps,liw:liw+nps-1) = matmul(transpose(conjg(lsr(1:nps,liw:liw+nps-1))),&
!		&transpose(conjg(lg0r(1:nps,liw:liw+nps-1))))

!	lnegg(1:nps,liw:liw+nps-1) = lg0g(1:nps,liw:liw+nps-1)&
!		&+matmul(lg0r(1:nps,liw:liw+nps-1),ltmp(1:nps,liw:liw+nps-1))&
!		&+matmul(lg0r(1:nps,liw:liw+nps-1),ltmp1(1:nps,liw:liw+nps-1))&
!		&+matmul(lg0g(1:nps,liw:liw+nps-1),ltmp2(1:nps,liw:liw+nps-1))
!end do

end subroutine winb
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Inbedding (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
