!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Prints w (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wgrid2d_lc(nprocs, my_rank, vw_c, vw_lc, vw_ilc)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
!Input variables
	integer :: npw!, my_comm_world
!Input/Output variables
integer, dimension(:) :: vw_c, vw_lc, vw_ilc

!Dummy variables
	integer :: iw,liw, liiw, pc, j_row
	integer :: nprocs, my_rank, error

!call mpi_comm_size	( my_comm_world , nprocs , error)
!call mpi_comm_rank	( my_comm_world , my_rank, error)
npw=size(vw_c)

!allocate(displ(nprocs), lclength(nprocs))


!displ=0
liw=0
do iw=1, npw
	pc=Mod(Floor(Real(iw-1)),nprocs)
	if(my_rank==pc) then
		liw=liw+1
		vw_lc(liw)=vw_c(iw)
		vw_ilc(liw)=iw
	end if
end do

end subroutine wgrid2d_lc
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Prints w (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
