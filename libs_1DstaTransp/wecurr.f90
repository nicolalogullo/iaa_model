!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Energy Current (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wecurr(context,nps,npw,wm,h,lnegl,lnegr,lsl,lsr,lsembl,lsembr,desc,outrow,outcol,ecur)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw, outrow,outcol
	double precision :: wm
	integer, dimension (:) :: desc
	double precision, dimension(:,:) :: h
	double complex, dimension(:,:) :: lnegl,lnegr,lsl,lsr,lsembl,lsembr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: n,i,j,lw,liw,mb,nb,lldc,lldr,lldwc,numroc
	double precision :: ecur
	double complex, dimension(nps,nps) :: intg
	double complex, dimension(:,:), allocatable :: ltmp,ltmp1,ltmp2

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldwc=size(lnegr,2)/nps
lldc=lldwc*lldr
! lldc=numroc(n,nb,mycol,0,nprocs)
! lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(ltmp(lldr,lldc),ltmp1(lldr,lldc),ltmp2(lldr,lldc))

ltmp=zero
ltmp1=zero
ltmp2=zero

do lw=1, lldwc
	liw=(lw-1)*nps+1
	ltmp(1:nps,liw:liw+nps-1) = matmul(lnegl(1:nps,liw:liw+nps-1), transpose(conjg(lsembr(1:nps,liw:liw+nps-1))))&
		& + matmul(lnegr(1:nps,liw:liw+nps-1), lsembl(1:nps,liw:liw+nps-1))
end do
do lw=1, lldwc
	liw=(lw-1)*nps+1
	ltmp1(1:nps,liw:liw+nps-1) = matmul(transpose(conjg(lnegr(1:nps,liw:liw+nps-1))),&
		&transpose(conjg(lsembr(1:nps,liw:liw+nps-1))))
end do
do lw=1, lldwc
	liw=(lw-1)*nps+1
	ltmp2(1:nps,liw:liw+nps-1) =matmul(h, ltmp(1:nps,liw:liw+nps-1))&
		&+matmul(lsr(1:nps,liw:liw+nps-1),ltmp(1:nps,liw:liw+nps-1))&
		&+matmul(lsl(1:nps,liw:liw+nps-1),ltmp1(1:nps,liw:liw+nps-1))
end do


call wint(context,nps,npw,wm,ltmp2,desc,outrow,outcol,intg)

	ecur=0.0d0
	do i=1,nps
		 ecur = ecur + 2*Real(intg(i,i))
	end do


end subroutine wecurr
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Energy current (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
