
!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		GW self-energy (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wgw(context,nps,npw,wm,vw,v,lnegl,lnegg,desc,lsl,lsg,lsr)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npw
	double precision :: wm
	integer, dimension (:) :: vw,desc
	double precision, dimension (:,:) :: v
	double complex, dimension(:,:) :: lnegl
	double complex, dimension(:,:) :: lnegg
!Input/Output variables
	double complex, dimension(:,:) :: lsl
	double complex, dimension(:,:) :: lsg
	double complex, dimension(:,:) :: lsr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol,rsrc,csrc
!Dummy variables
	integer :: n,i,j,lw,liw,mb,nb,lldc,lldr,lldwc,numroc
	double precision :: t,dt,tm
	double complex :: zalpha
	double complex, dimension(nps,nps) :: intg, tmp ,tmpr ,tmpa, cv
	double complex, dimension(:,:), allocatable :: ltmpr, ltmpl, ltmpg, ltmp,lpa, lpr, lpl, lpg, lwr, lwl, lwg 

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

mb = desc(5)
nb = desc(6)
n=nps*npw

lldr=numroc(nps,mb,myrow,0,1)
lldc=numroc(n,nb,mycol,0,nprocs)

lldwc=numroc(npw,1,mycol,0,nprocs)

allocate(ltmpr(lldr,lldc),ltmpl(lldr,lldc),ltmpg(lldr,lldc),ltmp(lldr,lldc))
allocate(lpr(lldr,lldc),lpa(lldr,lldc),lpl(lldr,lldc),lpg(lldr,lldc))
allocate(lwr(lldr,lldc),lwl(lldr,lldc),lwg(lldr,lldc))


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!POLARIZATION DIAGRAM (time) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

call fourierwt(context,nps,npw,wm,1,lnegl,desc,ltmpl)
call fourierwt(context,nps,npw,wm,1,lnegg,desc,ltmpg)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!LESSER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

do lw=1, lldwc
	liw=(lw-1)*nps+1
	lpl(:,liw:liw+nps-1)=ci*ltmpl(:,liw:liw+nps-1)*conjg(ltmpg(:,liw:liw+nps-1))
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!GREATER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

do lw=1, lldwc
	liw=(lw-1)*nps+1
	lpg(:,liw:liw+nps-1)=ci*ltmpg(:,liw:liw+nps-1)*conjg(ltmpl(:,liw:liw+nps-1))
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!RETARDED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ltmp=zero
tm=pi*(npw-1)/(2.d0*wm)
dt=pi/wm

do lw=1, lldwc
	liw=(lw-1)*nps+1
	t=-tm+(vw(lw)-1)*dt

	if (t.ge.0.d0.and.t<dt) then
		ltmp(:,liw:liw+nps-1)=0.5d0*(lpg(:,liw:liw+nps-1)-lpl(:,liw:liw+nps-1))
	else if (t>0.d0) then
		ltmp(:,liw:liw+nps-1)=(lpg(:,liw:liw+nps-1)-lpl(:,liw:liw+nps-1))
	end if
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!SCREENED INTERACTION (frequency) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
lpr=zero
call fourierwt(context,nps,npw,wm,-1,ltmp,desc,lpr)



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ADVANCED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ltmp=zero
tm=pi*(npw-1)/(2.d0*wm)
dt=pi/wm

do lw=1, lldwc
	liw=(lw-1)*nps+1
	t=-tm+(vw(lw)-1)*dt

	if (t.ge.0.d0.and.t<dt) then
		ltmp(:,liw:liw+nps-1)=-0.5d0*(lpg(:,liw:liw+nps-1)-lpl(:,liw:liw+nps-1))
	else if (t<0.d0) then
		ltmp(:,liw:liw+nps-1)=-(lpg(:,liw:liw+nps-1)-lpl(:,liw:liw+nps-1))
	end if
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!SCREENED INTERACTION (frequency) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
lpa=zero
call fourierwt(context,nps,npw,wm,-1,ltmp,desc,lpa)


lwl=lpl
lpl=zero
call fourierwt(context,nps,npw,wm,-1,lwl,desc,lpl)

lwg=lpg
lpg=zero
call fourierwt(context,nps,npw,wm,-1,lwg,desc,lpg)

cv=uno*v
lwl=zero
lwg=zero
do lw=1, lldwc
	liw=(lw-1)*nps+1

	![I-vP^r]^-1
	call wsolver(nps,cv,lpr(:,liw:liw+nps-1),tmp)
	tmpr=matmul(tmp,cv)!-v


	call wsolver(nps,cv,lpa(:,liw:liw+nps-1),tmp)
	tmpa=matmul(tmp,cv)!-v

	!lesser
	lwl(:,liw:liw+nps-1)=matmul((tmpr),matmul(lpl(:,liw:liw+nps-1),(tmpa)))

	!greater
	lwg(:,liw:liw+nps-1)=matmul((tmpr),matmul(lpg(:,liw:liw+nps-1),(tmpa)))

end do


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!SELF ENERGY (time) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
lpl=lwl
lwl=zero
call fourierwt(context,nps,npw,wm,1,lpl,desc,lwl)
lpg=lwg
lwg=zero
call fourierwt(context,nps,npw,wm,1,lpg,desc,lwg)



do lw=1, lldwc
	liw=(lw-1)*nps+1
	!lesser
	lsl(:,liw:liw+nps-1)=ci*ltmpl(:,liw:liw+nps-1)*lwl(:,liw:liw+nps-1)
	!greater
	lsg(:,liw:liw+nps-1)=ci*ltmpg(:,liw:liw+nps-1)*lwg(:,liw:liw+nps-1)
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!RETARDED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


ltmp=zero
tm=pi*(npw-1)/(2.d0*wm)
dt=pi/wm

do lw=1, lldwc
	liw=(lw-1)*nps+1
	t=-tm+(vw(lw)-1)*dt
!Self energy
	if (t.ge.0.d0.and.t<dt) then
		ltmp(:,liw:liw+nps-1)=0.5d0*(lsg(:,liw:liw+nps-1)-lsl(:,liw:liw+nps-1))
	else if (t>0.d0) then
		ltmp(:,liw:liw+nps-1)=(lsg(:,liw:liw+nps-1)-lsl(:,liw:liw+nps-1))
	end if
end do

ltmpl=zero
call fourierwt(context,nps,npw,wm,-1,ltmp,desc,ltmpl)
lsr = lsr + ltmpl

ltmp=lsl
lsl=zero
call fourierwt(context,nps,npw,wm,-1,ltmp,desc,lsl)

ltmp=lsg
lsg=zero
call fourierwt(context,nps,npw,wm,-1,ltmp,desc,lsg)

end subroutine wgw
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		GW self-energy (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

