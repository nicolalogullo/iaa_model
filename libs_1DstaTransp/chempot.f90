!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Chemical potential (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine chempot(context,n,w,bt,numpart,stat,mu)
	use mpi
	implicit none
!Input varibles
	integer :: n,stat
	integer :: context
	double precision :: bt
	double precision, dimension(:) :: w
!Input/output variables
	double precision :: numpart,mu
!Variable for grid query
	integer :: info,nprow,npcol,myrow,mycol
!Dummy varibales
	integer :: i,j,nsteps
	double precision :: tnpart, dmu

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)

mu=0.d0

!Only process (0,0) process finds the chemical potential
if(myrow==0.and.mycol==0) then
	tnpart=1.0d10
!Sets the initial chemical potential to be above the highest energy allowed
	if(w(n)>0) then
		mu=2*w(n)
	else if (w(n).le.0) then
		mu=2.d0
	end if

	dmu=1.d0
	nsteps=1

	do while ((Abs(tnpart-numpart)>=1.0E-14.and.nsteps<500000))

		tnpart=0.0d0
		do i=1,n
			tnpart=tnpart+1.0d0/(exp(bt*(w(i)-mu))-1.d0*stat)
		end do

		if (tnpart>numpart) then
			mu=mu-dmu
		else 
			mu=mu+dmu
			dmu=dmu*0.5d0
		end if
		
		if (tnpart<0) then
			mu=0.5d0*(w(int(numpart)+1)+w(int(numpart)))
			dmu=dmu*0.5d0
			print*, 'Particle number is negative npart= ', tnpart, 'nsteps = ', nsteps
		end if
			nsteps=nsteps+1
		tnpart=0.0d0
		do i=1,n
			tnpart=tnpart+1.0d0/(exp(bt*(w(i)-mu))-1.d0*stat)
		end do
	end do
		tnpart=0.0d0
		do i=1,n
			tnpart=tnpart+1.0d0/(exp(bt*(w(i)-mu))-1.d0*stat)
		end do

end if

!Process (0,0) sends the calculated chemical potential to all other processes
if(myrow==0.and.mycol==0) then
	call dgebs2d(context,'All','i-ring',1, 1,mu,1)
else
	call dgebr2d(context,'All','i-ring',1, 1,mu,1,0,0)
end if
call blacs_barrier(context,'A')

!Computes the total number of particles with mu
numpart=0.d0
do i=1,n
	numpart=numpart+1.0d0/(exp(bt*(w(i)-mu))-1.d0*stat)
end do

end subroutine chempot

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Chemical potential (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
