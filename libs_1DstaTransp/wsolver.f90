!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Solver (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wsolver(nps,lg0,ls,lg0s)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps
	double complex, dimension(:,:) :: lg0
	double complex, dimension(:,:):: ls
!Input/Output variables
	double complex, dimension(:,:) :: lg0s
!Dummy variables
	integer :: i

!+++++++++++++++++++++++++++++++++Kernel+++++++++++++++++++++++++++++++++++++++

!g0*s*delta

lg0s=matmul(lg0,ls)

lg0s = -lg0s

!!!!!!!!!!!!!!!!!!!!!!!!!!!Identity-Ker!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do i=1, nps
	lg0s(i,i) = uno + lg0s(i,i)
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!------------------------------------------------------------------------------

call locinv(nps,lg0s)

end subroutine wsolver
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Solver (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
