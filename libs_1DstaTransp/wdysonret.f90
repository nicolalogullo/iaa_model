
!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Dyson (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wdysonret(context,nps,npw,lg0r,lsr,lnegr)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	integer :: nps,npt
	double complex, dimension(:,:) :: lg0r
!Input/Output variables
	double complex, dimension(:,:) :: lnegr
	double complex, dimension(:,:), allocatable :: lgrsr
	double complex, dimension(:,:) :: lsr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: n,npw,iw,lw,liw,lldwc,numroc
	double precision :: dalpha,om
	double complex :: zalpha

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

allocate(lgrsr(nps,nps))

lgrsr=zero

lldwc=numroc(npw,1,mycol,0,nprocs)

do lw=1, lldwc
	liw=(lw-1)*nps+1
	call wsolver(nps,lg0r(:,liw:liw+nps-1),lsr(:,liw:liw+nps-1),lgrsr)

	!negr = matmul(grsr,g0r)
	lnegr(1:nps,liw:liw+nps-1) = matmul(lgrsr(1:nps,1:nps),lg0r(1:nps,liw:liw+nps-1))
end do
!--------------------------------------------------------------------

end subroutine wdysonret

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Dyson (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
