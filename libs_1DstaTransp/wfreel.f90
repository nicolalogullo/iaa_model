!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Free Green's Leads (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine wfreel(context,nps,npw,wm,vw,mu,ea,T,beta,V,shp,stat,lg0r,lg0l,lg0g,desc)
	use mpi
	implicit none
!Useful numerical parameters
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: zero = (0.0d0,0.0d0)			! Complex zero
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
!Input variables
	character(*) :: shp
	integer :: nps,npw,lld, stat
	double precision :: wm, mu, ea, T,beta, V
	integer, dimension(:) :: vw
!Input/Output variables
	integer, dimension (:) :: desc
	double complex, dimension(:,:) :: lg0r, lg0l,lg0g
!Variables for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: i,j,ww,wwp,liw,lw, nsteps, lldwc,numroc
	double precision :: dw,eta,deta,norm,om, omp
	double precision :: dalpha
	double complex :: zalpha,zbeta
	double complex, dimension(nps,nps) :: intg0r

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

lldwc=numroc(npw,1,mycol,0,nprocs)


lg0r=zero
!eta=1.d0
!deta=5.d-1
!norm=1.d3
!nsteps = 1


!do while ((Abs(norm-2*npw)>=1.0E-14.and.nsteps<2000))
!lg0r=zero

!!Retarded Green in the eigenbasis
!	do ww = 1, size(vw)
!		om=-wm+2*wm*(vw(ww)-1)/(npw-1)
!		liw = (ww-1)*nps+1
!		do wwp=1, npw
!			omp=-wm+2*wm*(wwp-1)/(npw-1)
!			do i=1,nps
!			lg0r(i,liw+i-1) =lg0r(i,liw+i-1) + 1.d0/(om-omp+ci*eta)
!			end do
!		end do
!	end do

!!Calcolo integrale A(k,w)

!	call wint(context,nps,npw,wm,lg0r,desc,intg0r)

!	norm=0.0d0
!	do i=1,2
!		norm=norm-2.d0*Aimag(intg0r(i,i))
!	end do
!	if (norm>2*npw) then
!		eta=eta+deta
!	else 
!		eta=eta-deta
!		deta=deta*0.5d0
!	end if

!	nsteps=nsteps+1

!end do

!!Retarded Green in the eigenbasis
!	do ww = 1, size(vw)
!		om=-wm+2*wm*(vw(ww)-1)/(npw-1)
!		liw = (ww-1)*nps+1
!		do wwp=1, npw
!			omp=-wm+2*wm*(wwp-1)/(npw-1)
!			do i=1,nps
!			lg0r(i,liw+i-1) =lg0r(i,liw+i-1) + 1.d0/(om-omp+ci*eta)
!			end do
!		end do
!	end do

!print*, eta,norm

do lw=1, lldwc
	liw = (lw-1)*nps+1	
	do i=1,nps
		lg0r(i,liw+i-1) =-ci*0.5d0
	end do
end do


!Tight-Binding Semi-Infinite Leads
!if (shp == 'tb') then

!do lw=1,lldwc
!liw=(lw-1)*nps+1
!om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)	
!do i=1,nps

!	if((om-ea+mu-V).gt.2*Abs(T)) then
!		lg0r(i,liw+i-1)=0.5d0*(om-ea+mu-V-sqrt((om-ea+mu-V)**2.d0-4*T**2.d0))/T**2.d0

!	elseif(Abs(om-ea+mu-V).le.2*Abs(T)) then
!		lg0r(i,liw+i-1)=0.5d0*(om-ea+mu-V-ci*sqrt(4*T**2.d0-(om-ea+mu-V)**2.d0))/T**2.d0

!	else
!		lg0r(i,liw+i-1)=0.5d0*(om-ea+mu-V+sqrt((om-ea+mu-V)**2.d0-4*T**2.d0))/T**2.d0

!	end if
!end do
!end do

!end if


!!!!!!!!!!!!!!!!!!!!!!!!!!!!Lesser Green's function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do lw=1, lldwc
	liw = (lw-1)*nps+1
	om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)
	dalpha=stat*(1.0d0/(exp(beta*(om-mu-V))-1.0d0*stat))
	lg0l(1:nps,liw:liw+nps-1)=dalpha*(lg0r(1:nps,liw:liw+nps-1)-transpose(conjg(lg0r(1:nps,liw:liw+nps-1))))
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Greater Green's function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do lw=1, lldwc
	liw = (lw-1)*nps+1
	om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)
	dalpha=(1.d0-1.0d0/(exp(beta*(om-mu-V))-1.0d0*stat))
	lg0g(1:nps,liw:liw+nps-1)=dalpha*(lg0r(1:nps,liw:liw+nps-1)-transpose(conjg(lg0r(1:nps,liw:liw+nps-1))))
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

end subroutine wfreel
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Free Green's Leads (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
