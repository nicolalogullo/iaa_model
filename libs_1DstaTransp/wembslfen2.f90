!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!	Embedding self-energies (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wembslfen2(context,nps,npw,wm,vw,lds,g,bt,mu,ea,shp,V,T,stat,lsl,lsg,lsr, desc,myrow,mycol)
	use mpi
	implicit none
!Useful numerical parameters
	double precision, parameter :: pi = dacos(-1.0d0)		! Pi greco
	double complex, parameter :: ci = (0.0d0,1.0d0)			! Imaginary unit
	double complex, parameter :: uno = (1.0d0,0.0d0)		! Complex one
	double complex, parameter :: zero = (0.0d0,0.0d0)		! Complex zero
!Input variables
	character(*) :: shp
	integer :: nps,npw,stat
	double precision :: wm,bt,ea,mu,V,T
	double precision, dimension(:) :: g
	integer, dimension (:) :: lds, desc
	integer, dimension (:) :: vw
!Input/Output variables
	double complex, dimension(:,:) :: lsl
	double complex, dimension(:,:) :: lsg
	double complex, dimension(:,:) :: lsr
!Varibles for grid query
	integer :: context,info,nprow,npcol,nprocs,myrow,mycol
!Dummy variables
	integer :: i,j,iw,lw,liw,lldwc,numroc
	double precision :: tm,dt,dalpha,om, tmp

!create the DOS for the leads
!eps0=2.0d0
!sgm0=-1.5d0

!call edos(context,shp,tf,npt,npw,eps0,sgm0,V,dos,descdos)

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol

!lldwc=numroc(npw,1,mycol,0,nprocs)
lldwc=size(vw)

lsl=zero
lsr=zero
tm=pi*(npw-1)/(2.d0*wm)
dt=pi/wm

!!!!!!!!!!!!!!!!!!!!!!!!!!!!Retarded Self-energy!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! lsl=zero
! do lw=1, lldwc
! 	liw=(lw-1)*nps+1
!  	om=-wm+2.d0*wm*(vw(lw)-1)/(npw-1)
! 	 tmp=-tm+(vw(lw)-1)*dt
! 		do i=1,size(lds)
! 			do j=1,size(lds)
! 				if (t.ge.0.d0.and.t<dt) then
! 					lsl(lds(i),liw+lds(j)-1)=-ci*0.5d0*(g(i)*g(j))
! 				else if (t>0.d0) then
! 					lsl(lds(i),liw+lds(j)-1)=-ci*exp(-ci*om*t)*(g(i)*g(j))
! 				end if
! 			end do
! 		end do
! end do
! lsr=zero
!
! call fourierwtSUM(context,nps,npw,wm,-1,lsl,desc,lsr)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!Retarded Self-energy!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do lw=1,lldwc
	liw=(lw-1)*nps+1
	do i=1,size(lds)
                do j=1,size(lds)
		        lsr(lds(i),liw+lds(j)-1)=-ci*0.5d0*(g(i)*g(j))
                end do
	end do
end do

call fourierwtSUM(context,nps,npw,wm,1,lsr,desc,lsl)

do lw=1, lldwc
	liw=(lw-1)*nps+1
	 tmp=-tm+(vw(lw)-1)*dt
		do i=1,size(lds)
			do j=1,size(lds)
				if (t.ge.0.d0.and.t<dt) then
					lsl(1:nps,liw:liw+nps-1) = 0.5d0*lsl(1:nps,liw:liw+nps-1)
				else if (t<0.d0) then
					lsl(1:nps,liw:liw+nps-1)=zero
				end if
			end do
		end do
end do

call fourierwtSUM(context,nps,npw,wm,-1,lsl,desc,lsr)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!Lesser Self-energy!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
lsl=zero
do lw=1, lldwc
	liw=(lw-1)*nps+1
	om=-wm+2*wm*(vw(lw)-1)/(npw-1)
        if(shp=='bos'.and.om.le.0) then
                dalpha=0.d0
        else
                dalpha=stat*(1.0d0/(exp(bt*(om-mu-V))-1.0d0*stat))
        end if
	lsl(1:nps,liw:liw+nps-1)=dalpha*(lsr(1:nps,liw:liw+nps-1)-transpose(conjg(lsr(1:nps,liw:liw+nps-1))))
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Greater Self-energy!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do lw=1, lldwc
	liw=(lw-1)*nps+1
	om=-wm+2*wm*(vw(lw)-1)/(npw-1)
	if(shp=='bos'.and.om.le.0) then
                dalpha=0.d0
        else
                dalpha=(1.d0+stat*1.0d0/(exp(bt*(om-mu-V))-1.0d0*stat))
        end if
	lsg(1:nps,liw:liw+nps-1)=dalpha*(lsr(1:nps,liw:liw+nps-1)-transpose(conjg(lsr(1:nps,liw:liw+nps-1))))
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


end subroutine wembslfen2
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Embedding self-energies (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvjvvvvvvvvvvvvvvvv
