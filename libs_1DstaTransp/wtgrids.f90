!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!		Prints w (Start)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine wtgrids( context, Desc_FWT, v_in, v_out )
	use MKL_CDFT
	implicit none
!Input/Output variables
	integer, dimension(:) :: v_in, v_out
!Varibles for grid query
	integer :: context, nprow, npcol, myrow, mycol, nprocs
	type(DFTI_DESCRIPTOR_DM), POINTER :: Desc_FWT
!Dummy variables
	integer :: iw,liw,lldwc,numroc, STATUS
	integer :: SIZE, NX_IN, NX_OUT, START_X, START_X_OUT
	double precision :: deps

call blacs_gridinfo(context,nprow,npcol,myrow,mycol)
nprocs=nprow*npcol


STATUS = DftiGetValueDM( Desc_FWT , CDFT_LOCAL_SIZE , SIZE )
STATUS = DftiGetValueDM( Desc_FWT , CDFT_LOCAL_NX , NX_IN )
STATUS = DftiGetValueDM( Desc_FWT , CDFT_LOCAL_OUT_NX , NX_OUT )
STATUS = DftiGetValueDM( Desc_FWT , CDFT_LOCAL_X_START , START_X )
STATUS = DftiGetValueDM( Desc_FWT , CDFT_LOCAL_OUT_X_START , START_X_OUT )

lldwc = SIZE

v_in=0
v_out=0

do iw=1, NX_IN
	v_in(iw)	=	START_X +iw -1
end do
do iw=1, NX_OUT
	v_out(iw)	=	START_X_OUT +iw -1
end do

end subroutine wtgrids
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!		Prints w (End)
!vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
